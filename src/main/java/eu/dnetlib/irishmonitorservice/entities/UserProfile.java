package eu.dnetlib.irishmonitorservice.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

public class UserProfile {
    @Id
    @JsonProperty("_id")
    private String id;

    @Indexed(unique = true)
    private String aaiId;
    private Boolean consent = false;

    public UserProfile() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAaiId() {
        return aaiId;
    }

    public void setAaiId(String aaiId) {
        this.aaiId = aaiId;
    }

    public Boolean getConsent() {
        return consent;
    }

    public void setConsent(Boolean consent) {
        this.consent = consent;
    }
}
