package eu.dnetlib.irishmonitorservice.entities;

public enum SortBy {
    OPEN_ACCESS,
    PUBLICATIONS
}
