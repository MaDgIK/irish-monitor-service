package eu.dnetlib.irishmonitorservice.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Data {
    List<List<List<String>>> data;

    public List<List<String>> getData() {
        if(data.isEmpty()) {
            return new ArrayList<>();
        }
        return data.get(0);
    }
}
