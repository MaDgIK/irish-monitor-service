package eu.dnetlib.irishmonitorservice.entities;

import eu.dnetlib.uoamonitorservice.entities.Stakeholder;

public class StakeholderExtended extends Stakeholder {
    private final Integer publications;
    private final Float publicationPR;
    private final Float publicationPROA;

    public StakeholderExtended(Stakeholder stakeholder, String publications, String publicationPR, String publicationPROA) {
        super(stakeholder);
        this.publications = publications != null ? Integer.parseInt(publications) : null;
        this.publicationPR = publicationPR != null ? Float.parseFloat(publicationPR) : null;
        this.publicationPROA = publicationPROA != null ? Float.parseFloat(publicationPROA) : null;
    }

    public Integer getPublications() {
        return publications;
    }

    public Number getOpenAccess() {
        if( publicationPR != null && publicationPROA != null && publicationPR > 0) {
            return (publicationPROA / publicationPR) * 100;
        }
        return 0;
    }
}
