package eu.dnetlib.irishmonitorservice;

import eu.dnetlib.authentication.configuration.AuthenticationConfiguration;
import eu.dnetlib.irishmonitorservice.configuration.GlobalVars;
import eu.dnetlib.irishmonitorservice.configuration.properties.APIProperties;
import eu.dnetlib.irishmonitorservice.configuration.properties.StatsToolProperties;
import eu.dnetlib.uoamonitorservice.UoaMonitorServiceConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = {"eu.dnetlib.irishmonitorservice"})
@PropertySources({
        @PropertySource("classpath:admintoolslibrary.properties"),
        @PropertySource("classpath:authentication.properties"),
        @PropertySource("classpath:monitorservice.properties"),
        @PropertySource("classpath:irishmonitorservice.properties"),
        @PropertySource(value = "classpath:dnet-override.properties", ignoreResourceNotFound = true)
})
@EnableConfigurationProperties({GlobalVars.class, APIProperties.class, StatsToolProperties.class})
@EnableCaching
@EnableScheduling
@Import({UoaMonitorServiceConfiguration.class, AuthenticationConfiguration.class})
public class IrishMonitorServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(IrishMonitorServiceApplication.class, args);
    }
}
