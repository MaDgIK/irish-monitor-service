package eu.dnetlib.irishmonitorservice.security;

import com.google.gson.JsonArray;
import com.nimbusds.jwt.JWT;
import eu.dnetlib.authentication.utils.AuthoritiesMapper;
import eu.dnetlib.uoaauthorizationlibrary.security.AuthorizationService;
import org.mitre.openid.connect.client.OIDCAuthoritiesMapper;
import org.mitre.openid.connect.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
@ConditionalOnProperty(
        value = "authentication.authorities-mapper",
        havingValue = "irish.eduperson_entitlement")
public class IrishAuthoritiesMapper implements OIDCAuthoritiesMapper {
    private final String domain = "IRISH_";
    private final AuthorizationService authorizationService;

    @Autowired
    public IrishAuthoritiesMapper(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @Override
    public Collection<? extends GrantedAuthority> mapAuthorities(JWT jwtToken, UserInfo userInfo) {
        JsonArray entitlements = userInfo.getSource().getAsJsonArray("eduperson_entitlement");
        return AuthoritiesMapper.map(entitlements).stream()
                .filter(this::filter)
                .map(this::map).collect(Collectors.toSet());
    }


    public boolean filter(GrantedAuthority authority) {
        return authority.getAuthority().equals(this.authorizationService.PORTAL_ADMIN) ||
                authority.getAuthority().equals(this.authorizationService.REGISTERED_USER) ||
                authority.getAuthority().contains(domain);
    }

    public GrantedAuthority map(GrantedAuthority authority) {
        return new SimpleGrantedAuthority(authority.getAuthority().replaceFirst(domain, ""));
    }
}
