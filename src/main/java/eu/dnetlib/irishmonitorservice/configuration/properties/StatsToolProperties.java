package eu.dnetlib.irishmonitorservice.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("stats-tool")
public class StatsToolProperties {
    private String host;
    private Indicators rpo;
    private Indicators rfo;
    private Indicators repository;

    public StatsToolProperties() {
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Indicators getRpo() {
        return rpo;
    }

    public void setRpo(Indicators rpo) {
        this.rpo = rpo;
    }

    public Indicators getRfo() {
        return rfo;
    }

    public void setRfo(Indicators rfo) {
        this.rfo = rfo;
    }

    public Indicators getRepository() {
        return repository;
    }

    public void setRepository(Indicators repository) {
        this.repository = repository;
    }
}
