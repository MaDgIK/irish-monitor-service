package eu.dnetlib.irishmonitorservice.configuration.properties;

public class Indicators {
    String publications;
    String publicationsPR;
    String publicationsPROA;

    public String getPublications() {
        return publications;
    }

    public void setPublications(String publications) {
        this.publications = publications;
    }

    public String getPublicationsPR() {
        return publicationsPR;
    }

    public void setPublicationsPR(String publicationsPR) {
        this.publicationsPR = publicationsPR;
    }

    public String getPublicationsPROA() {
        return publicationsPROA;
    }

    public void setPublicationsPROA(String publicationsPROA) {
        this.publicationsPROA = publicationsPROA;
    }
}
