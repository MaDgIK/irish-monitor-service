package eu.dnetlib.irishmonitorservice.configuration.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import eu.dnetlib.uoamonitorservice.configuration.properties.MongoConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Collections;

@Configuration
@EnableMongoRepositories(basePackages = {"eu.dnetlib.irishmonitorservice.dao"}, mongoTemplateRef = "mongoIrishTemplate")
public class IrishMongoConnection {

    @Autowired
    private MongoConfig mongoConfig;

    @Bean
    public MongoDbFactory mongoDbFactory() {
        return new SimpleMongoDbFactory(getMongoClient(), mongoConfig.getDatabase());
    }

    @Bean(name = "mongoIrishTemplate")
    public MongoTemplate getMongoTemplate() {
        return new MongoTemplate(mongoDbFactory());
    }

    private MongoClient getMongoClient() {
        if(mongoConfig.getUsername() != null && mongoConfig.getPassword() != null){
            return new MongoClient(Collections.singletonList(
                    new ServerAddress(mongoConfig.getHost(), mongoConfig.getPort())),
                    Collections.singletonList(MongoCredential.createCredential(mongoConfig.getUsername(), mongoConfig.getDatabase(), mongoConfig.getPassword().toCharArray())));
        } else {
            return new MongoClient(Collections.singletonList(new ServerAddress(mongoConfig.getHost(), mongoConfig.getPort())));
        }
    }


}
