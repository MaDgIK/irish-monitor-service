package eu.dnetlib.irishmonitorservice.services;

import eu.dnetlib.irishmonitorservice.entities.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class CacheService {
    private final Logger logger = LogManager.getLogger(CacheService.class);

    private final StatsToolService statsToolService;
    private final CacheManager cacheManager;

    @Autowired
    public CacheService(StatsToolService statsToolService, CacheManager cacheManager) throws UnsupportedEncodingException {
        this.statsToolService = statsToolService;
        this.cacheManager = cacheManager;
        this.clearCache();
    }

    public List<List<List<String>>> getResponse(String type) throws UnsupportedEncodingException {
        switch (type) {
            case "funder":
                return this.statsToolService.getFunders();
            case "organization":
                return this.statsToolService.getOrganizations();
            case "datasource":
                return this.statsToolService.getDataSources();
            default:
                return null;
        }
    }

    @Scheduled(cron = "0 0 0 * * *") // Reset cache every day at 00:00
    public void clearCache() throws UnsupportedEncodingException {
        this.cacheManager.getCache("funder");
        this.cacheManager.getCache("organization");
        this.cacheManager.getCache("datasource");
        this.cacheManager.getCacheNames().stream()
                .map(this.cacheManager::getCache)
                .filter(Objects::nonNull)
                .forEach(Cache::clear);
        logger.info("Cache cleared at " + LocalDateTime.now());
        this.statsToolService.getFunders();
        this.statsToolService.getOrganizations();
        this.statsToolService.getDataSources();
    }
}
