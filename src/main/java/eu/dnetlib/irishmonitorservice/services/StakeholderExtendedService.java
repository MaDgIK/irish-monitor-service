package eu.dnetlib.irishmonitorservice.services;

import eu.dnetlib.irishmonitorservice.entities.SortBy;
import eu.dnetlib.irishmonitorservice.entities.StakeholderExtended;
import eu.dnetlib.uoamonitorservice.entities.Stakeholder;
import eu.dnetlib.uoamonitorservice.service.StakeholderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StakeholderExtendedService {
    private final StakeholderService service;
    private final CacheService cacheService;

    @Autowired
    public StakeholderExtendedService(StakeholderService service, CacheService cacheService) {
        this.service = service;
        this.cacheService = cacheService;
    }

    public StakeholderExtended getStakeholderExtended(String stakeholderId) throws UnsupportedEncodingException {
        Stakeholder stakeholder = this.service.findByPath(stakeholderId);
        List<List<List<String>>> results = this.cacheService.getResponse(stakeholder.getType());
        if(results != null) {
            return new StakeholderExtended(stakeholder,
                            getNumber(results, 0, stakeholder.getIndex_id()),
                            getNumber(results, 1, stakeholder.getIndex_id()),
                            getNumber(results, 2, stakeholder.getIndex_id()));
        }
        else {
            return new StakeholderExtended(stakeholder, "0", "0", "0");
        }
    }

    public List<StakeholderExtended> getVisibleStakeholdersExtended(String type) throws UnsupportedEncodingException {
        List<Stakeholder> stakeholders = this.service.getVisibleStakeholders(type, null);
        List<List<List<String>>> results = this.cacheService.getResponse(type);
        if(results != null) {
            return stakeholders.stream().map(stakeholder ->
                    new StakeholderExtended(stakeholder,
                            getNumber(results, 0, stakeholder.getIndex_id()),
                            getNumber(results, 1, stakeholder.getIndex_id()),
                            getNumber(results, 2, stakeholder.getIndex_id()))).collect(Collectors.toList());
        }
        else {
            return stakeholders.stream().map(stakeholder -> new StakeholderExtended(stakeholder, "0", "0", "0")).collect(Collectors.toList());
        }
    }

    public String getNumber(List<List<List<String>>> results, int index, String id) {
        if(results != null && index < results.size()) {
            return results.get(index).stream().filter(list -> list.get(1).equals(id)).findFirst().orElse(Collections.singletonList("0")).get(0);
        }
        return "0";
    }

    public List<StakeholderExtended> sortBy(List<StakeholderExtended> stakeholders, SortBy sort) {
        stakeholders.sort(Comparator.comparingInt(StakeholderExtended::getPublications).reversed());
        if(sort == SortBy.OPEN_ACCESS) {
            stakeholders.sort(Comparator.comparingDouble((StakeholderExtended a) -> a.getOpenAccess().doubleValue()).reversed());
        }
        return stakeholders;
    }
}
