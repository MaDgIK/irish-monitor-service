package eu.dnetlib.irishmonitorservice.services;

import eu.dnetlib.irishmonitorservice.configuration.properties.Indicators;
import eu.dnetlib.irishmonitorservice.configuration.properties.StatsToolProperties;
import eu.dnetlib.irishmonitorservice.entities.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

@Service
public class StatsToolService {
    private final Logger logger = LogManager.getLogger(StatsToolService.class);

    @Autowired
    private StatsToolProperties properties;

    @Autowired
    private RestTemplate restTemplate;

    @Cacheable(value = "funder")
    public List<List<List<String>>> getFunders() throws UnsupportedEncodingException {
        return this.getData(this.properties.getRfo());
    }

    @Cacheable(value = "organization")
    public List<List<List<String>>> getOrganizations() throws UnsupportedEncodingException {
        return this.getData(this.properties.getRpo());
    }

    @Cacheable(value = "datasource")
    public List<List<List<String>>> getDataSources() throws UnsupportedEncodingException {
        return this.getData(this.properties.getRepository());
    }

    public List<List<List<String>>> getData(Indicators indicators) throws UnsupportedEncodingException {
        List<List<List<String>>> data = new ArrayList<>();
        if(indicators != null) {
            data.add(this.getData(properties.getHost(), indicators.getPublications()));
            data.add(this.getData(properties.getHost(), indicators.getPublicationsPR()));
            data.add(this.getData(properties.getHost(), indicators.getPublicationsPROA()));
        }
        return data;
    }

    private List<List<String>> getData(String service, String json) throws UnsupportedEncodingException {
        if(service == null || json == null) {
            return new ArrayList<>();
        } else {
            URI uri = UriComponentsBuilder.fromUriString(service + URLDecoder.decode(json, "UTF-8")).build().encode().toUri();
            try {
                ResponseEntity<Data> response = restTemplate.getForEntity(uri, Data.class);
                if(response.getStatusCode() == HttpStatus.OK) {
                    return response.getBody().getData();
                }
                return new ArrayList<>();
            } catch (RestClientException e) {
                return new ArrayList<>();
            }
        }
    }
}
