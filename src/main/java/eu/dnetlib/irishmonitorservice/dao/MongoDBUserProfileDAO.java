package eu.dnetlib.irishmonitorservice.dao;

import eu.dnetlib.irishmonitorservice.entities.UserProfile;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoDBUserProfileDAO extends UserProfileDAO, MongoRepository<UserProfile, String> {
    List<UserProfile> findAll();

    UserProfile findById(String Id);

    UserProfile findByAaiId(String aaiId);

    UserProfile save(UserProfile user);

    void deleteAll();

    void delete(String id);
}
