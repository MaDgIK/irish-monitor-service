package eu.dnetlib.irishmonitorservice.dao;

import eu.dnetlib.irishmonitorservice.entities.UserProfile;

import java.util.List;

public interface UserProfileDAO {
    List<UserProfile> findAll();

    UserProfile findById(String Id);

    UserProfile findByAaiId(String aaiId);

    UserProfile save(UserProfile user);

    void deleteAll();

    void delete(String id);
}
