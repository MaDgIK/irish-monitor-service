package eu.dnetlib.irishmonitorservice.controllers;

import eu.dnetlib.irishmonitorservice.dao.UserProfileDAO;
import eu.dnetlib.irishmonitorservice.entities.UserProfile;
import eu.dnetlib.uoaadmintoolslibrary.handlers.ContentNotFoundException;
import eu.dnetlib.uoaadmintoolslibrary.handlers.ForbiddenException;
import eu.dnetlib.uoaadmintoolslibrary.handlers.utils.RolesUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/user")
@PreAuthorize("isAuthenticated()")
public class UserProfileController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private RolesUtils rolesUtils;

    @Autowired
    private UserProfileDAO userDAO;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public UserProfile saveUserProfile(@RequestBody UserProfile userProfile) {
        String aaiId = rolesUtils.getAaiId();
        UserProfile existingUserProfile =  userDAO.findByAaiId(aaiId);
        if(existingUserProfile != null) {
            userProfile.setId(existingUserProfile.getId());
        }
        userProfile.setAaiId(aaiId); // users can only save/ update their own profiles
        return userDAO.save(userProfile);
    }

//    @RequestMapping(value = "/delete/{aaiId}", method = RequestMethod.DELETE)
//    public Boolean deleteUserProfileByAaiId(@PathVariable("aaiId") String aaiId) {
//        // maybe portal admin could delete any user profile?
//
//        String currentAaiId = rolesUtils.getAaiId();
//        if(aaiId != null && currentAaiId.equals(aaiId)) {
//            throw new ForbiddenException("User with id: "+currentAaiId + " has not access to update user with id: "+aaiId);
//        }
//        UserProfile user =  userDAO.findByAaiId(aaiId);
//        if(user == null) {
//            // EXCEPTION - Entity Not Found
//            throw new ContentNotFoundException("Delete user profile: No user profile found for : " + aaiId);
//        } else {
//            userDAO.delete(user.getId());
//        }
//        return true;
//    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public UserProfile getUserProfile() {
        String aaiId = rolesUtils.getAaiId();
        UserProfile user =  userDAO.findByAaiId(aaiId);
        if(user == null) {
            // EXCEPTION - Entity Not Found
            throw new ContentNotFoundException("No user profile found for: " + aaiId);
        }
        return user;
    }
}
