package eu.dnetlib.irishmonitorservice.controllers;

import eu.dnetlib.irishmonitorservice.configuration.GlobalVars;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class IrishServiceCheckDeployController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private GlobalVars globalVars;

    @RequestMapping(value = {"", "/health_check"}, method = RequestMethod.GET)
    public String hello() {
        log.debug("Hello from irish-monitor-service!");
        return "Hello from irish-monitor-service!";
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/health_check/advanced", method = RequestMethod.GET)
    public Map<String, String> checkEverything() {
        Map<String, String> response = new HashMap<>();

        if(globalVars.date != null) {
            response.put("Date of deploy", globalVars.date.toString());
        }
        if(globalVars.getBuildDate() != null) {
            response.put("Date of build", globalVars.getBuildDate());
        }
        if(globalVars.getVersion() != null) {
            response.put("Version", globalVars.getVersion());
        }

        return response;
    }
}
