package eu.dnetlib.irishmonitorservice.controllers;

import eu.dnetlib.irishmonitorservice.entities.SortBy;
import eu.dnetlib.irishmonitorservice.entities.StakeholderExtended;
import eu.dnetlib.irishmonitorservice.services.StakeholderExtendedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("extended")
public class StakeholderExtendedController {
    private final StakeholderExtendedService service;

    @Autowired
    public StakeholderExtendedController(StakeholderExtendedService service) {
        this.service = service;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<StakeholderExtended> getStakeholders(@RequestParam("type") String type, @RequestParam(value = "sort", required = false) SortBy sort) throws UnsupportedEncodingException {
        return this.service.sortBy(this.service.getVisibleStakeholdersExtended(type), sort);
    }

    @RequestMapping(value = "/{stakeholderId}", method = RequestMethod.GET)
    public StakeholderExtended getStakeholder(@PathVariable String stakeholderId) throws UnsupportedEncodingException {
        return this.service.getStakeholderExtended(stakeholderId);
    }
}
