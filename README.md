# Irish Monitor Service

Irish Monitor Service is a service that integrates both OpenAIRE Login API and 
OpenAIRE Monitor API and provide all functionalities for National Open Access Monitor, Ireland.

## Maven

Java Version: 1.8, Sprint boot Version: 1.5.8.RELEASE

This service has dependencies downloaded from [D4Science Nexus repository](https://maven.research-infrastructures.eu/nexus/content/repositories/dnet45-releases),
so there is a definition of this repository inside [pom.xml](pom.xml).

### Install

    mvn clean install -DskipTests -Dmaven.javadoc.skip=true;

#### Standalone (Recommended)

1. Create a new system service and enable in order to start up at boot.

       cd /etc/systemd/system
       sudo vim service-name.service

2. Add the following content by replacing () accordingly.

       [Unit]
       Description=service-name
       After=syslog.target
        
       [Service]
       User=root
       ExecStart=(/home/user/spring-boot/)irish-monitor-service.war (--spring.profiles.active=swagger) --server.port=(port) (--server.context-path=/irish-monitor-service) --spring.config.location=file:///(home/user/spring-boot/config/application.properties)
       SuccessExitStatus=143
        
       [Install]
       WantedBy=multi-user.target

3. Configure the service.

       sudo systemctl daemon-reload
       sudo systemctl enable service-name

4. Handle service.

       sudo systemctl start service-name
       sudo systemctl stop service-name
       sudo systemctl restart service-name
       sudo systemctl status service-name

#### Container (eg Tomcat)

1. Add the war in the _webapps_ folder.
2. Create application.properties file in _lib_ folder.

## Configuration

In order to configure this service you have to set the following properties:

    stats-tool.rfo=<StatsToolURL>?json=<JsonQuery>
    stats-tool.rpo=<StatsToolURL>?json=<JsonQuery>
    stats-tool.repository=<StatsToolURL>?json=<JsonQuery>


## Login core

This dependency provides the Authentication and Authorization functionality. 
In order to configure it you have to set the following properties:

    authentication.domain=<Domain>
    authentication.oidc.home=<API_URL>/openid_connect_login
    authentication.oidc.scope=openid,profile,email,eduperson_entitlement,orcid
    authentication.oidc.id=<ProviderClientId>
    authentication.oidc.secret=<ProviderClientSecret>
    authentication.oidc.issuer=https://aai.openaire.eu/auth/realms/openaire
    authentication.oidc.redirect=<API_URL>/redirect
    authentication.session=irishSession
    authentication.redirect=<DefaultRedirectURL>
    authentication.redis.host=<RedisHost>
    authentication.redis.port=<RedisPort>
    authentication.redis.passwork=<RedisPassword>
    authentication.authorities-mapper=irish.eduperson_entitlement // DO NOT CHANGE
    authentication.keycloak=true

## Monitor Service

This dependency provides the main service functionality 
in order to create/edit/delete monitor profiles and their content.
In order to configure it you have to set the following properties:

    monitorservice.mongodb.host=<MongoHost>
    monitorservice.mongodb.port=<MongoPort>
    monitorservice.mongodb.database=<DatabaseName>
    monitorservice.mongodb.username=<MongoUser>
    monitorservice.mongodb.password=<MongoPassword>

## Admin Tools Library (integrated in Monitor Service)

This dependency provides utilities to store dynamic HTML content, 
send Email and verify Google recaptcha. In order to configure it 
you have to set the following properties:

    admintoolslibrary.mail.from = <Email>
    admintoolslibrary.mail.username = <Email>
    admintoolslibrary.mail.password = <EmailPassword>
    admintoolslibrary.mail.host = <EmailHost>
    admintoolslibrary.mail.port = <EmailPort>
    admintoolslibrary.mail.auth = true
    admintoolslibrary.mail.sslProtocols = TLSv1.2
    admintoolslibrary.mail.defaultEncoding=UTF-8
    admintoolslibrary.mail.protocol=<EmailProtocol>
    admintoolslibrary.mail.testConnection=false
    admintoolslibrary.google.secret = <GoogleSecret>
