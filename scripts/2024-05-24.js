load('library/initialize.js');

function keepRPOsWithRole(aliasWithRole) {
    var stakeholders = db.stakeholder.find({ alias: { $in: aliasWithRole } });
    rpos = removeDuplicateIds(rpos, 'id');
    var canContinue = true;
    stakeholders.forEach(stakeholder => {
        if(rpos.find(rpo => !!rpo.id && rpo.id === stakeholder.index_id) == null) {
            print('Stakeholder with alias: ' + stakeholder.alias + ' has invalid id. Checking by name...');
            var newRPO = rpos.find(rpo => !!rpo.name && rpo.name === stakeholder.index_name);
            if(newRPO != null) {
                print('Name is found. Updating the id from: ' + stakeholder.index_id + ' to: ' + newRPO.id);
                stakeholder.index_id = newRPO.id;
                db.stakeholder.save(stakeholder);
            } else {
                newRPO = rpos.find(rpo => !!rpo.shortname && rpo.shortname === stakeholder.index_shortname);
                if (newRPO != null) {
                    print('Shortname is found. Updating the id from: ' + stakeholder.index_id + ' to: ' + newRPO.id);
                    stakeholder.index_id = newRPO.id;
                    db.stakeholder.save(stakeholder);
                } else {
                    print('Resolve stakeholder with alias: ' + stakeholder.alias + ' to continue.');
                    canContinue = false;
                }
            }
        }
    });
    return canContinue;
}

if(keepRPOsWithRole(aliasWithRole)) {
    print('Deleting old profiles');
    db.stakeholder.deleteMany({ alias: { $nin: aliasWithRole }, defaultId: { $ne: null }, type: "organization"});
    initializeRPOs(rpos);
}
