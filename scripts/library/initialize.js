load('library/organization.js')
load('library/funder.js')
load('library/datasource.js')

function generateAlias(name) {
    // Step 1: Convert to lower case
    let alias = name.toLowerCase();

    // Step 2: Replace all invalid characters with a hyphen
    alias = alias.replace(/[^a-z0-9-]/g, '-');

    // Step 3: Replace multiple consecutive hyphens with a single hyphen
    alias = alias.replace(/-+/g, '-');

    // Step 4: Remove leading or trailing hyphens
    alias = alias.replace(/^-+|-+$/g, '');

    /* Find duplicates */
    let duplicate = alias.toString();
    var i = 1;
    while(db.stakeholder.findOne({"alias": duplicate}) != null) {
        duplicate = alias + i.toString();
        i++;
    }

    return duplicate;
}

function removeDuplicateIds(array, fieldId) {
    let seenIds = {}; // Object to store encountered ids
    let uniqueArray = [];

    array.forEach(obj => {
        if (!seenIds[obj[fieldId]]) { // If id is not encountered yet
            seenIds[obj[fieldId]] = true; // Mark id as encountered
            uniqueArray.push(obj); // Add object to unique array
        }
    });

    return uniqueArray;
}

function initializeRPOs(rpos) {
    var defaultRPO = db.stakeholder.findOne({"alias": "default-rpo"});
    var count = 0;
    var found = 0;
    rpos.forEach(rpo => {
        if(db.stakeholder.findOne({"index_id": rpo.id}) == null) {
            let alias = generateAlias(!!rpo.shortname && rpo.shortname != 'NULL'?rpo.shortname:rpo.name);
            var stakeholder = {
                "_class": "eu.dnetlib.uoamonitorservice.entities.Stakeholder",
                "type": "organization",
                "index_id": rpo.id,
                "index_name": rpo.name,
                "index_shortName": rpo.shortname,
                "statsProfile": "ie_monitor",
                "isUpload": false,
                "name": rpo.name,
                "alias": alias,
                "locale": "EN",
                "visibility": "PUBLIC",
                "topics": [],
                "copy": false,
                "standalone": true,
                "defaultId": defaultRPO._id.valueOf()
            }
            db.stakeholder.save(stakeholder);
            count++;
        } else {
            found++;
        }
    });
    print(count  + ' RPOs has been created and ' + found + ' already exist');
}

function initializeRFOs(rfos) {
    var defaultRFO = db.stakeholder.findOne({"alias": "default-rfo"});
    rfos.forEach(rfo => {
        let alias = generateAlias(rfo.funder_shortname != ''?rfo.funder_shortname:rfo.funder_name);
        var stakeholder = {
            "_class": "eu.dnetlib.uoamonitorservice.entities.Stakeholder",
            "type": "funder",
            "index_id": rfo.funder_id,
            "index_name": rfo.funder_name,
            "index_shortName": rfo.funder_shortname,
            "statsProfile": "ie_monitor",
            "isUpload": false,
            "name": rfo.funder_name,
            "alias": alias,
            "locale": "EN",
            "visibility": "PUBLIC",
            "topics": [],
            "copy": false,
            "standalone": true,
            "defaultId": defaultRFO._id.valueOf()
        }
        db.stakeholder.save(stakeholder);
    });
    print(rfos.length  + ' RFOs has been created');
}

function initializeDatasources(datasources) {
    var defaultRepo = db.stakeholder.findOne({"alias": "default-repo"});
    datasources.forEach(datasource => {
        let alias = generateAlias(datasource.official_name);
        var stakeholder = {
            "_class": "eu.dnetlib.uoamonitorservice.entities.Stakeholder",
            "type": "datasource",
            "index_id": datasource.openaire_id,
            "index_name": datasource.official_name,
            "index_shortName": datasource.official_name,
            "statsProfile": "ie_monitor",
            "isUpload": false,
            "name": datasource.official_name,
            "alias": alias,
            "locale": "EN",
            "visibility": "PUBLIC",
            "topics": [],
            "copy": false,
            "standalone": true,
            "defaultId": defaultRepo._id.valueOf()
        }
        db.stakeholder.save(stakeholder);
    });
    print(datasources.length  + ' repositories has been created');
}
