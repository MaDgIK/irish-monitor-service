var aliasWithRole = ["met-eireann","mary-immaculate-college","technological-university-dublin","sport-ireland","dcu","irel","nda","ucd","rcsi","sfi","health-service-executive","hib","atu","ul","deptecc","tusla---child-and-family-agency","tcd","galway","maynooth","heanet","i-form-advanced-manufacturing-research-centre","rotunda-hospital","ucc","department-of-children-and-youth-affairs","dublin","marine-institute","seai","cit","setu","esri1","department-of-agriculture-food-and-the-marine","iph","dbs","mmuh"];

var rpos = [
  {
    "id": "pending_org_::c581efe982a294affe08a1660b4649db",
    "name": "KPMG",
    "shortname": "KPMG"
  },
  {
    "id": "openorgs____::07d81d5fddb167d7984a254a5fff3721",
    "name": "AbbVie (Ireland)",
    "shortname": "AbbVie (Ireland)"
  },
  {
    "id": "openorgs____::a3613d3e7737d3356f1f7b7d5fd62371",
    "name": "Kerry Group (Ireland)",
    "shortname": "Kerry Group (Ireland)"
  },
  {
    "id": "pending_org_::0fb09e2648be9590e82ce48675983758",
    "name": "ARDEN ENERGY LIMITED",
    "shortname": "ENERGY SOLUTIONS"
  },
  {
    "id": "pending_org_::e048b9039d40de82553cf02f1af30e4e",
    "name": "Temple Street Children's Hospital School",
    "shortname": "Temple Street Children's Hospital School"
  },
  {
    "id": "openorgs____::18b6f7e3f4ace67d3f54ae7fba232adb",
    "name": "Free University of Ireland",
    "shortname": "Free University of Ireland"
  },
  {
    "id": "openorgs____::763ee55e0ab3f6e460aa0e2ad7507b03",
    "name": "Electricity Supply Board (Ireland)",
    "shortname": "ESB"
  },
  {
    "id": "pending_org_::a9c7b3bcf069cd606f6a59973f92cc49",
    "name": "IRISH RED CROSS SOCIETY",
    "shortname": "IRISH RED CROSS SOCIETY"
  },
  {
    "id": "openorgs____::ef72a53e16017cf890fd5e2d05fc2eb8",
    "name": "Barnardos Ireland",
    "shortname": "Barnardos Ireland"
  },
  {
    "id": "openorgs____::76ee7313190bfd900586d543bc672c54",
    "name": "Exergyn (Ireland)",
    "shortname": "Exergyn (Ireland)"
  },
  {
    "id": "openorgs____::bbc4a3340b7b257a20811e1b0ead5753",
    "name": "Mallinckrodt (Ireland)",
    "shortname": "Mallinckrodt (Ireland)"
  },
  {
    "id": "pending_org_::fccc1751fa239fd60c4f2b48e95d6e16",
    "name": "INSTITUTE OF DIGITAL INNOVATION AND RESEARCH (IDIR) LIMITED",
    "shortname": "Institute of Digital Innovation and Research"
  },
  {
    "id": "pending_org_::2895112ecf7fcd17f42ea6ee57d137bb",
    "name": "NYPRO LIMITED",
    "shortname": "NYPRO LIMITED"
  },
  {
    "id": "pending_org_::1c638e8bafd08517fb34ddb2d71a1836",
    "name": "DIONERGY LIMITED",
    "shortname": "DIONERGY LTD"
  },
  {
    "id": "pending_org_::60d95a4e3aabb799388b1ba5f64d9aae",
    "name": "OPENHYDRO GROUP LIMITED",
    "shortname": "OPENHYDRO GROUP LIMITED"
  },
  {
    "id": "openorgs____::e63ca2b8b1dce3d511e8c457fb846f8f",
    "name": "Tallaght Hospital",
    "shortname": "AMNCH"
  },
  {
    "id": "pending_org_::6ab7929dd1aa5f32f48552e33175fb25",
    "name": "MAGNOSTICS LIMITED",
    "shortname": "MAGNOSTICS LTD"
  },
  {
    "id": "pending_org_::692f6574187d51357949e5d613f6cb6a",
    "name": "Lara media Limited",
    "shortname": "Laramedia"
  },
  {
    "id": "openorgs____::894f2ef2de3b2aae22b2603b3e18ca63",
    "name": "Ballymun Job Centre",
    "shortname": "BJC"
  },
  {
    "id": "pending_org_::98fa7eba50974e163c457a4108e7b850",
    "name": "Care Alliance Ireland Ltd",
    "shortname": "Care Alliance Ireland Ltd"
  },
  {
    "id": "pending_org_::ca2d4b636766e722c0be446e3fca321b",
    "name": "KASTUS TECHNOLOGIES LIMITED",
    "shortname": "KASTUS TECHNOLOGIES LIMITED"
  },
  {
    "id": "pending_org_::9a4cd51e75caed9bafeeeb4f18e9cf9a",
    "name": "IASCACH INTIRE EIREANN",
    "shortname": "INLAND FISHERIES IRELAND"
  },
  {
    "id": "pending_org_::a973cf0e6771033166fe904b4b8dcd38",
    "name": "X DISPLAY COMPANY TECHNOLOGY LIMITED",
    "shortname": "X-CELEPRINT"
  },
  {
    "id": "openorgs____::f57c30f7c18ef30372400c051cd20da0",
    "name": "Suas Educational Development",
    "shortname": "Suas Educational Development"
  },
  {
    "id": "pending_org_::2ba8111ac592985ce73677e98b5028e4",
    "name": "St Josephs Special School",
    "shortname": "St Josephs Special School"
  },
  {
    "id": "pending_org_::d4b14f057a73454c5200fa6ee20299c2",
    "name": "BETAPOND LIMITED",
    "shortname": "BETAPOND LIMITED"
  },
  {
    "id": "pending_org_::0054227739ee90484696e27408bc0183",
    "name": "CELL STRESS DISCOVERIES LIMITED",
    "shortname": "CELL STRESS DISCOVERIES LIMITED"
  },
  {
    "id": "openorgs____::f87a387eee55a5ee9c95cb055eb0f812",
    "name": "Bridge Interpreting (Ireland)",
    "shortname": "Bridge Interpreting (Ireland)"
  },
  {
    "id": "openorgs____::fb8af71f5cc878d569224d011ba74a8f",
    "name": "Crossing the Line (Ireland)",
    "shortname": "Crossing the Line (Ireland)"
  },
  {
    "id": "pending_org_::2a49c9b401a5af5cd753545d9c11710a",
    "name": "SHANAHAN RESEARCH GROUP",
    "shortname": "SHANAHAN RESEARCH GROUP"
  },
  {
    "id": "openorgs____::bbb672711b1ecddf022c8176451216aa",
    "name": "Environmental Protection Agency",
    "shortname": "EPA"
  },
  {
    "id": "openorgs____::7446a56197733aa6ca6850a69eebf9bf",
    "name": "22q11 Ireland",
    "shortname": "22q11 Ireland"
  },
  {
    "id": "openorgs____::e76e42db9b8f7062cf6061b64be33ff4",
    "name": "Trinity Centre for Asian Studies Trinity College Dublin The University of Dublin",
    "shortname": "Trinity Centre for Asian Studies Trinity College Dublin The University of Dublin"
  },
  {
    "id": "pending_org_::5d934ea81f3c4ec30ff4ba63db92efd6",
    "name": "T.E. LABORATORIES LIMITED",
    "shortname": "T.E. LABORATORIES LIMITED"
  },
  {
    "id": "openorgs____::adcc06b0aa2ad21c6393bc8780edc855",
    "name": "Bon Secours Hospital Cork",
    "shortname": "Bon Secours Hospital Cork"
  },
  {
    "id": "openorgs____::2320e10a339baefdcf68c513661beb8d",
    "name": "Aut Even Hospital",
    "shortname": "Aut Even Hospital"
  },
  {
    "id": "pending_org_::81f39e3ffd3e3ebee054b4ad59850340",
    "name": "AIRFIELD ESTATE",
    "shortname": "AIRFIELD ESTATE"
  },
  {
    "id": "pending_org_::461d8c73aada0f78b34085c5c9bd5695",
    "name": "Cardinal Health (Ireland)",
    "shortname": "Cardinal Health (Ireland)"
  },
  {
    "id": "pending_org_::fd4f28a1276493ae986f188fc7e7ac36",
    "name": "TECHWORKS MARINE LIMITED",
    "shortname": "TWM"
  },
  {
    "id": "pending_org_::f628b05f23a1a7dfc748d2cc5015a02b",
    "name": "STATWOLF LIMITED",
    "shortname": "STATWOLF"
  },
  {
    "id": "openorgs____::59e62ab71025280b923809438044713c",
    "name": "Medfit Proactive Healthcare",
    "shortname": "Medfit Proactive Healthcare"
  },
  {
    "id": "pending_org_::4c5022cd338a8eb9763dc4ff578ca733",
    "name": "CRE Composting Association of Ireland Teoranta",
    "shortname": "CRE"
  },
  {
    "id": "pending_org_::e425965ad8829bba1f5a07a011eb490e",
    "name": "SANMINA IRELAND UNLIMITED COMPANY",
    "shortname": "SANMINA IRELAND UNLIMITED COMPANY"
  },
  {
    "id": "pending_org_::23ba4d2b85df46516feee18301ce6a69",
    "name": "SYNCROPHI SYSTEMS LTD",
    "shortname": "SYNCROPHI SYSTEMS LTD"
  },
  {
    "id": "openorgs____::365fe348f2b36d095732b574c85e84a0",
    "name": "SilverCloud (Ireland)",
    "shortname": "SilverCloud (Ireland)"
  },
  {
    "id": "openorgs____::0ba7db0665d321dfddac9cfde63097e7",
    "name": "Central Remedial Clinic",
    "shortname": "CRC"
  },
  {
    "id": "pending_org_::480d246505b40008f7d71ee2808e8953",
    "name": "Green Biofuels Ireland Limited",
    "shortname": "GBI"
  },
  {
    "id": "openorgs____::5ea01339978d46f35e31ec428becaa4e",
    "name": "Berand (Ireland)",
    "shortname": "Berand (Ireland)"
  },
  {
    "id": "pending_org_::5c47293fe7665b2ec6998191af1d10ad",
    "name": "DP Energy Ireland Limited",
    "shortname": "DPEI"
  },
  {
    "id": "pending_org_::ed23ef7ee13d0d981f6b11056279a015",
    "name": "National Microelectronics Research Centre",
    "shortname": "NMRC"
  },
  {
    "id": "pending_org_::1e8923d712421fb5e82318b3270a4254",
    "name": "MICROELECTRONICS INDUSTRY DESIGN ASSOCIATION (MIDAS)COMPANY LIMITED BY GUARANTEE",
    "shortname": "MIDAS IRELAND"
  },
  {
    "id": "pending_org_::b0f96a1ad8a84a2b38e7347b9bfbd307",
    "name": "ADAMA INNOVATIONS LIMITED",
    "shortname": "ADAMA INNOVATIONS LIMITED"
  },
  {
    "id": "pending_org_::d0a984a176dbd00554881a43ead4f60b",
    "name": "MeaningMine Ltd",
    "shortname": "MeaningMine Ltd"
  },
  {
    "id": "pending_org_::7585d0b6790f9d4d5564c8469ad9620f",
    "name": "ZOAN BIOMED LIMITED",
    "shortname": "Zoan BioMed Ltd"
  },
  {
    "id": "pending_org_::55a87d1fa03d72192c2c815f2d949fe1",
    "name": "National Roads Authority",
    "shortname": "NRA"
  },
  {
    "id": "pending_org_::6d47ea67617c6be10cab389d70d64c6c",
    "name": "TECHNOLOGY FROM IDEAS LIMITED",
    "shortname": "TECHNOLOGY FROM IDEAS"
  },
  {
    "id": "openorgs____::2dd04d6b745639fe3daa5f8c2c705743",
    "name": "Sacred Heart Hospital",
    "shortname": "Sacred Heart Hospital"
  },
  {
    "id": "pending_org_::f5a590e2e314c639d4b0f7fbb6aa3d6d",
    "name": "BRIVANT LIMITED",
    "shortname": "Integer"
  },
  {
    "id": "openorgs____::6437dd90a75aa7e793ac28a6f7fcc41c",
    "name": "Codema (Ireland)",
    "shortname": "Codema (Ireland)"
  },
  {
    "id": "pending_org_::0c5b443fc9c2d48e6a8ce5199e0e47b3",
    "name": "ISME LIMITED",
    "shortname": "ISME"
  },
  {
    "id": "pending_org_::4ef6457f4a101cb6145bea06b3923cad",
    "name": "NATIONAL SPACE CENTRE LIMITED",
    "shortname": "NSC"
  },
  {
    "id": "openorgs____::05ad2171113125cb6ac4c5c417bf3b2b",
    "name": "Food for Health Ireland",
    "shortname": "FHI"
  },
  {
    "id": "pending_org_::8afeed7c8db66856fd35518a376c4963",
    "name": "DIANIA TECHNOLOGIES LIMITED",
    "shortname": "DIANIA TECHNOLOGIES LTD"
  },
  {
    "id": "openorgs____::acf537583275a69d1b14ec0647f6ffe6",
    "name": "Department of Tourism, Culture, Arts, Gaeltacht, Sport and Media",
    "shortname": "DTCASM"
  },
  {
    "id": "pending_org_::8abbc1465453ad3db9a1be2bec467906",
    "name": "PERFUZE LIMITED",
    "shortname": "PERFUZE LIMITED"
  },
  {
    "id": "pending_org_::95df117358b9b70e47c79d011f4ee45c",
    "name": "Dairy Processing Technology Centre",
    "shortname": "Dairy Processing Technology Centre"
  },
  {
    "id": "openorgs____::34634e4f295ccdc1b2c7b283c9accf7e",
    "name": "Roscommon University Hospital",
    "shortname": "Roscommon University Hospital"
  },
  {
    "id": "openorgs____::7f828d9d1ca4469b2052a43b6dcc12d7",
    "name": "Sligo University Hospital",
    "shortname": "SUH"
  },
  {
    "id": "openorgs____::7fe2ca4613d207e89555ddb21561fcd9",
    "name": "School of Applied Language and Intercultural Studies (SALIS) Dublin City University",
    "shortname": "School of Applied Language and Intercultural Studies (SALIS) Dublin City University"
  },
  {
    "id": "pending_org_::360b4d27fbff74d60ee7508971049fa0",
    "name": "METABOLOMIC DIAGNOSTICS LIMITED",
    "shortname": "METABOL"
  },
  {
    "id": "pending_org_::460beaff88ad9c76fe2aa1fdd2437139",
    "name": "LAKE COMMUNICATIONS LTD",
    "shortname": "LAKE"
  },
  {
    "id": "openorgs____::0fec77221d8afde72f06351bc9382c51",
    "name": "ADAPT - Centre for Digital Content Technology",
    "shortname": "ADAPT - Centre for Digital Content Technology"
  },
  {
    "id": "pending_org_::62089ef8d4b5e199f7bc2e37890565f7",
    "name": "Aspect Medical Systems",
    "shortname": "Aspect Medical Systems"
  },
  {
    "id": "openorgs____::84a32a026c0203bb43c611fc73c08726",
    "name": "National Archives",
    "shortname": "National Archives"
  },
  {
    "id": "openorgs____::fe18f98c74ac2f59ba2d0929bfeac785",
    "name": "IDA Ireland",
    "shortname": "IDA Ireland"
  },
  {
    "id": "openorgs____::942d30b0bb40f8247d596e0ec1aed8c6",
    "name": "Fota Wildlife Park",
    "shortname": "Fota Wildlife Park"
  },
  {
    "id": "pending_org_::94362da619aacdb93179a02f43814c58",
    "name": "KOHINOOR LIMITED",
    "shortname": "KOHINOOR LIMITED"
  },
  {
    "id": "openorgs____::4d6061bed3bd437b34528e4dcbe222ed",
    "name": "Irish Research eLibrary",
    "shortname": "IReL"
  },
  {
    "id": "pending_org_::9917f6c1ee5e8972f0c7082e325dfa82",
    "name": "YOUNG SCIENTIST AND TECHNOLOGY EXHIBITION COMPANY LIMITED BY GUARANTEE",
    "shortname": "YOUNG SCIENTIST AND TECHNOLOGY EXHIBITION COMPANY LIMITED BY GUARANTEE"
  },
  {
    "id": "openorgs____::31fe50057132151b3a58cef328672972",
    "name": "Swim Ireland",
    "shortname": "Swim Ireland"
  },
  {
    "id": "pending_org_::2ae3bf17cbeb72e0f38f1637bd75b4b3",
    "name": "SONARSIM LTD",
    "shortname": "SSIM"
  },
  {
    "id": "openorgs____::71646b5015612a955c37b8fd917ffd85",
    "name": "Irish Heart Foundation",
    "shortname": "Irish Heart Foundation"
  },
  {
    "id": "openorgs____::0ca97772dc227a2079048ea4e28bdc31",
    "name": "Pilot Photonics (Ireland)",
    "shortname": "Pilot Photonics (Ireland)"
  },
  {
    "id": "openorgs____::5aa5469bd77cd4c56ca9fa1e19978a58",
    "name": "Kildare Education Centre",
    "shortname": "Kildare Education Centre"
  },
  {
    "id": "openorgs____::5ff052e5b1bb86599d11a074a059cfd0",
    "name": "City of Dublin Skin and Cancer Hospital Charity",
    "shortname": "CDSCHC"
  },
  {
    "id": "openorgs____::c90f6cf6877525113ffbb6a89b0f304d",
    "name": "Hewlett Packard Enterprise (Ireland)",
    "shortname": "HPE"
  },
  {
    "id": "pending_org_::301b9026bdbad27c49cc7ed35bb233d5",
    "name": "Houses of the Oireachtas",
    "shortname": "Houses of the Oireachtas"
  },
  {
    "id": "pending_org_::6d98c70342a14155daf06433961aa57e",
    "name": "THE IRISH TIMES DESIGNATED ACTIVITYCOMPANY",
    "shortname": "THE IRISH TIMES DESIGNATED ACTIVITYCOMPANY"
  },
  {
    "id": "pending_org_::63ffecd0a728123ddd76e8c810e66712",
    "name": "PORTABLE MEDICAL TECHNOLOGY LIMITED",
    "shortname": "PORTABLE MEDICAL TECHNOLOGY LIMITED"
  },
  {
    "id": "openorgs____::39ef7575a7a568751231084528c6282d",
    "name": "Collaborative Centre for Applied Nanotechnology",
    "shortname": "CCAN"
  },
  {
    "id": "pending_org_::944a3fb1648eaaa482d8b046a508e4ca",
    "name": "ERINN INNOVATION LIMITED",
    "shortname": "ERINN INNOVATION"
  },
  {
    "id": "pending_org_::9f189766a1958c95fd64e1f60e7634ca",
    "name": "NTERA LIMITED",
    "shortname": "NTERA LIMITED"
  },
  {
    "id": "pending_org_::501002a3725e66ab0bdda155c5d1646e",
    "name": "SUBLIMITY THERAPEUTICS LIMITED",
    "shortname": "SUBLIMITY"
  },
  {
    "id": "pending_org_::c108aadfa339ad07347f279499f1313d",
    "name": "INNOVATIVE POLYMER COMPOUNDS LIMITED",
    "shortname": "IPC"
  },
  {
    "id": "openorgs____::b6a303beac4845f6847e45d717edd20a",
    "name": "University College Cork (Department of Computer Science)",
    "shortname": "University College Cork (Department of Computer Science)"
  },
  {
    "id": "openorgs____::ab7cf45199c99c4464b8c210c8f77135",
    "name": "Mental Health Commission",
    "shortname": "Mental Health Commission"
  },
  {
    "id": "pending_org_::b2e641d527ccf39d80446fb1937f7cae",
    "name": "EPI-LIGHT LIMITED",
    "shortname": "EPI-LIGHT LIMITED"
  },
  {
    "id": "openorgs____::350d4dafc83e157b955d1bf04f28ceba",
    "name": "Mater Misericordiae University Hospital",
    "shortname": "MMUH"
  },
  {
    "id": "openorgs____::55195a7d24c9fcc2e9e2cf777760de14",
    "name": "Q4 Public Relations (Ireland)",
    "shortname": "Q4PR"
  },
  {
    "id": "openorgs____::c177610f156083cc0c35323a4336c263",
    "name": "IBM (Ireland)",
    "shortname": "IBM (Ireland)"
  },
  {
    "id": "pending_org_::3fd91cbdf84110a022d52de2466c8e23",
    "name": "PILOT PHOTONICS LTD",
    "shortname": "PILOT"
  },
  {
    "id": "pending_org_::133048533ab82f3cbf033f9b8f21c9b8",
    "name": "St. Mary's College",
    "shortname": "St. Mary's College"
  },
  {
    "id": "pending_org_::6a3699c1309c56d1999b1d32bc427a75",
    "name": "Insight SFI Research Centre for Data Analytics",
    "shortname": "Insight SFI Research Centre for Data Analytics"
  },
  {
    "id": "pending_org_::44d2542e6063c7a1b37a90804ebc1599",
    "name": "WIRE-LITE SENSORS LTD",
    "shortname": "ENDECO TECHNOLOGIES"
  },
  {
    "id": "openorgs____::07d5ee916078fec66a21052330c9cdff",
    "name": "Bard (Ireland)",
    "shortname": "Bard (Ireland)"
  },
  {
    "id": "openorgs____::6293a03189d507537f2305c80edb20a7",
    "name": "Mercy University Hospital",
    "shortname": "Mercy University Hospital"
  },
  {
    "id": "pending_org_::105c72957a0b6a3aff0a0ffadb7bec6c",
    "name": "Advanced Materials and Bioengineering Research",
    "shortname": "Advanced Materials and Bioengineering Research"
  },
  {
    "id": "pending_org_::bbba90c6e6e659275c1d191bbb6e95d0",
    "name": "CYBERCOLLOIDS LIMITED",
    "shortname": "CC"
  },
  {
    "id": "openorgs____::0b287041f371cc8eb7d953049697c816",
    "name": "PublicPolicy.ie (Ireland)",
    "shortname": "PublicPolicy.ie (Ireland)"
  },
  {
    "id": "pending_org_::3f32eab542ff91e618d21a879c0b5b4f",
    "name": "RCSI University of Medicine and Health Science (RCSI)",
    "shortname": "RCSI University of Medicine and Health Science (RCSI)"
  },
  {
    "id": "openorgs____::da5de343d5be18b2141abb4658f19387",
    "name": "Irish Universities Association",
    "shortname": "CHIU"
  },
  {
    "id": "openorgs____::152eb57d9962480c3c49d33b91e919f6",
    "name": "Cystinosis Ireland",
    "shortname": "Cystinosis Ireland"
  },
  {
    "id": "pending_org_::e7c91459c46f58293bb5f8e49fae8e5d",
    "name": "GAS NETWORKS IRELAND",
    "shortname": "GAS NETWORKS IRELAND"
  },
  {
    "id": "openorgs____::932f6832581c76a49eb090109c080dc7",
    "name": "Centre for Translation and Textual Studies SALIS Dublin City University (DCU)",
    "shortname": "Centre for Translation and Textual Studies SALIS Dublin City University (DCU)"
  },
  {
    "id": "pending_org_::d6021fcbf3c54e031677d6f195998840",
    "name": "F6S NETWORK IRELAND LIMITED",
    "shortname": "F6S IE"
  },
  {
    "id": "openorgs____::03cbc37a4204c5f895f2eb1b3192ebea",
    "name": "Project Arts Centre",
    "shortname": "Project Arts Centre"
  },
  {
    "id": "openorgs____::050f50b35c198bcfe1e49601cbb2855f",
    "name": "Portasol (Ireland)",
    "shortname": "Portasol (Ireland)"
  },
  {
    "id": "openorgs____::3f46d3e80feaf4c15375955e64183c89",
    "name": "I-Form Advanced Manufacturing Research Centre",
    "shortname": "I-Form Advanced Manufacturing Research Centre"
  },
  {
    "id": "openorgs____::73919918d57905149083150730e857de",
    "name": "Boyne Research Institute",
    "shortname": "Boyne Research Institute"
  },
  {
    "id": "pending_org_::6b9eb3ac8585dfdadfd90b5e5a25115b",
    "name": "CERVOS LIMITED",
    "shortname": "CERVOS LIMITED"
  },
  {
    "id": "pending_org_::656eef56f223062cd736aeddc0848889",
    "name": "CYNTELIX CORPORATION LIMITED",
    "shortname": "CYNTELIX"
  },
  {
    "id": "pending_org_::0556a495962b4ab81de89c11c5b02a3a",
    "name": "MICANANOTECH LIMITED",
    "shortname": "MICANANOTECH LIMITED"
  },
  {
    "id": "pending_org_::6e29903b45b343d5f16b305837853457",
    "name": "INTEL RESEARCH AND INNOVATION IRELAND LIMITED",
    "shortname": "INTEL IRELAND"
  },
  {
    "id": "pending_org_::f8dadb2a9d7b418caeb12a7557162cd5",
    "name": "Kinia",
    "shortname": "Kinia"
  },
  {
    "id": "openorgs____::7298d6d80e63528ee655111527452a75",
    "name": "Heartbeat Trust",
    "shortname": "Heartbeat Trust"
  },
  {
    "id": "pending_org_::e4d021f52a2f876ce829683e59af2e15",
    "name": "IRISH OBSERVER NETWORK LIMITED",
    "shortname": "MARINE NATURAL RESOURCE GOVERNANCE MNRG"
  },
  {
    "id": "pending_org_::c94d06ff10f95b2f176de90cccc3f584",
    "name": "CIT INFINITE DESIGNATED ACTIVITY COMPANY",
    "shortname": "CIT INFINITE DESIGNATED ACTIVITY COMPANY"
  },
  {
    "id": "pending_org_::594ed5f1fc2458508b55a25c94cafd5f",
    "name": "BIOPROBE DIAGNOSTICS LIMITED",
    "shortname": "BIOPROBE DIAGNOSTICS LIMITED"
  },
  {
    "id": "openorgs____::ea01c1f01b29d8cab65c88a1a3474c33",
    "name": "UCD School of Mathematical Sciences University College Dublin",
    "shortname": "UCD School of Mathematical Sciences University College Dublin"
  },
  {
    "id": "openorgs____::bc7cac3a79a733eaea2c6d49d3229424",
    "name": "Foras Áiseanna Saothair",
    "shortname": "FÁS"
  },
  {
    "id": "openorgs____::b7af949a66bd5fd897f29c88f9886c25",
    "name": "Espion (Ireland)",
    "shortname": "Espion (Ireland)"
  },
  {
    "id": "openorgs____::0b2181f721296baf19edaf4a078251df",
    "name": "Irish Home Energy Rating Energy Services (Ireland)",
    "shortname": "Irish Home Energy Rating Energy Services (Ireland)"
  },
  {
    "id": "openorgs____::7b679614602b4435364f2595652a11ba",
    "name": "Irish Museum of Modern Art",
    "shortname": "IMMA"
  },
  {
    "id": "openorgs____::4367206b7004f7de7f1a438c8ee0b7ce",
    "name": "Irish Institute of Clinical Neuroscience",
    "shortname": "IICN"
  },
  {
    "id": "pending_org_::09274acc558a91deb16e3ad7a4de5ceb",
    "name": "CRITICAL PATH INSTITUTE, LIMITED",
    "shortname": "CRITICAL PATH INSTITUTE, LIMITED"
  },
  {
    "id": "pending_org_::fd7d76e0274a0dab3c8d9a7ff20a90c5",
    "name": "E-SEARCH LIMITED",
    "shortname": "NEWSWEAVER"
  },
  {
    "id": "orgreg______::6b4061090beb2fc7d38561d2f8d49fd6",
    "name": "St. Vincent's University Hospital",
    "shortname": ""
  },
  {
    "id": "pending_org_::ac40922a7449790176d6b6a0a42d157f",
    "name": "TIPPERARY ENERGY AGENCY LIMITED",
    "shortname": "TEA"
  },
  {
    "id": "openorgs____::b05244ca3652d640546b84c514a059c5",
    "name": "Centre for Ageing Research and Development in Ireland",
    "shortname": "CARDI"
  },
  {
    "id": "openorgs____::8c282f87fb799d0760d5e455447f5da5",
    "name": "Centre For Irish and European Security",
    "shortname": "CIES"
  },
  {
    "id": "openorgs____::6a386142a26d9e0aa51dd367c9ec9138",
    "name": "Broadcom (Ireland)",
    "shortname": "Broadcom (Ireland)"
  },
  {
    "id": "openorgs____::5246501b8acd12bfc6ac5f6f023741f6",
    "name": "Avara Pharmaceutical Services (Ireland)",
    "shortname": "Avara Pharmaceutical Services (Ireland)"
  },
  {
    "id": "openorgs____::79c82344cef5c26e3dbff2a5ca68c662",
    "name": "Creme Global (Ireland)",
    "shortname": "Creme Global (Ireland)"
  },
  {
    "id": "pending_org_::c18b6909c9fb885b0d941d29ecf2a808",
    "name": "CELLIX LIMITED",
    "shortname": "Cellix"
  },
  {
    "id": "pending_org_::9db569db3394976a2584d33593e6b405",
    "name": "IDENTIFYHER LTD",
    "shortname": "IDENTIFYHER LTD"
  },
  {
    "id": "openorgs____::b4b40800cb10ec2569cb9aa68fb7aaa9",
    "name": "Amnesty International",
    "shortname": "AI"
  },
  {
    "id": "pending_org_::8874aaa0db03acc0c9e2443328212cc8",
    "name": "Clew Bay Marine Forum Ltd.",
    "shortname": "Clew Bay Marine Foru"
  },
  {
    "id": "pending_org_::ae403f38df62755280563a8a15b35961",
    "name": "X-CELEPRINT LIMITED",
    "shortname": "X-CELEPRINT LIMITED"
  },
  {
    "id": "openorgs____::ce2d033145a18e43db6022ba6630530d",
    "name": "Dublin City Council",
    "shortname": "Dublin City Council"
  },
  {
    "id": "pending_org_::8800db259571f74652c606ccdc95de98",
    "name": "THE NATIONAL DIGITAL RESEARCH LTD.",
    "shortname": "THE NATIONAL DIGITAL RESEARCH LTD."
  },
  {
    "id": "openorgs____::f25521fd290b665d4c2d794deeae931b",
    "name": "Letterkenny University Hospital",
    "shortname": "LUH"
  },
  {
    "id": "openorgs____::f6c9fc661c12d7a4e1b3d9a930ad1a11",
    "name": "Caredoc",
    "shortname": "Caredoc"
  },
  {
    "id": "openorgs____::ca4327a8ace278c68a24a552599558bf",
    "name": "ServusNet Informatics (Ireland)",
    "shortname": "ServusNet Informatics (Ireland)"
  },
  {
    "id": "openorgs____::1927c8441a3bb5609fa393ccabd9fccb",
    "name": "The Alzheimer Society of Ireland",
    "shortname": "The Alzheimer Society of Ireland"
  },
  {
    "id": "openorgs____::5862427afee3df6a34961156101c2218",
    "name": "Analog Devices (Ireland)",
    "shortname": "Analog Devices (Ireland)"
  },
  {
    "id": "openorgs____::3c473e3d1009222f185c45e2c647e91f",
    "name": "Social Justice Ireland",
    "shortname": "Social Justice Ireland"
  },
  {
    "id": "pending_org_::b2b63a4fd5025c7dfb77a5f1758d9739",
    "name": "ABBOTT RAPID DX INTERNATIONAL LIMITED",
    "shortname": "ABBOTT RAPID DX INTERNATIONAL LIMITED"
  },
  {
    "id": "pending_org_::58bbd590f4fb75538bdbf219b23828f4",
    "name": "Church of Ireland Theological Institute",
    "shortname": "Church of Ireland Theological Institute"
  },
  {
    "id": "pending_org_::7ee842ef5ebf84834f0cac91eb928305",
    "name": "CARTRON POINT SHELLFISH LTD",
    "shortname": "CPS"
  },
  {
    "id": "openorgs____::ffe61143a934bba97358f5c21f22dded",
    "name": "Department of Education and Skills",
    "shortname": "Department of Education and Skills"
  },
  {
    "id": "pending_org_::be74d6777dfd3f7c1865e8dd1426892d",
    "name": "PFIZER IRELAND PHARMACEUTICALS",
    "shortname": "PFIZER"
  },
  {
    "id": "pending_org_::9b894b91f49c196e929ca4401f125feb",
    "name": "RAPPEL ENTERPRISES LIMITED - ARKLOW MARINE SERVICES",
    "shortname": "RAL - AMS"
  },
  {
    "id": "pending_org_::be856a01add76754c1033843cf9e1d63",
    "name": "Apierian Limited",
    "shortname": "Apierian Ltd"
  },
  {
    "id": "pending_org_::01a95670ccc489015b38829548faa7db",
    "name": "NSILICO LIFE SCIENCE LIMITED",
    "shortname": "NSILICO LIFE SCIENCE"
  },
  {
    "id": "pending_org_::7f0b58ff7908b37ff5f677ea16941eb6",
    "name": "TEAGASC",
    "shortname": "TEAGASC"
  },
  {
    "id": "pending_org_::1336bcf3d7833bdd21cdb622b59118cc",
    "name": "Alkermes",
    "shortname": "Alkermes"
  },
  {
    "id": "openorgs____::3b56c8cc6a38dcda08439813600a8f11",
    "name": "Orbsen Therapeutics (Ireland)",
    "shortname": "Orbsen Therapeutics (Ireland)"
  },
  {
    "id": "pending_org_::5785b4c7cf3eb2850a737338184e8605",
    "name": "CUSTOMISED FOOD INDUSTRY TRAINING LTD",
    "shortname": "HALBERT RESEARCH"
  },
  {
    "id": "openorgs____::4cdd97e8e21255f7cc4783c79b14ff72",
    "name": "University Hospital Kerry",
    "shortname": "University Hospital Kerry"
  },
  {
    "id": "openorgs____::d11f981828c485cd23d93f7f24f24db1",
    "name": "Technological University Dublin",
    "shortname": "Technological University Dublin"
  },
  {
    "id": "pending_org_::125358094d216f6316c6b09fdae697b8",
    "name": "ROTHA",
    "shortname": "ROTHA"
  },
  {
    "id": "pending_org_::6ac38e1aa1b5504b42fe9625c41acef1",
    "name": "National Parks and Wildlife Service",
    "shortname": "National Parks and Wildlife Service"
  },
  {
    "id": "openorgs____::500dcbeb5795c50707313c6b38114633",
    "name": "ÉireComposites (Ireland)",
    "shortname": "ÉireComposites (Ireland)"
  },
  {
    "id": "openorgs____::ed7a37a17673c6606be375120330e3c4",
    "name": "Leo Pharma (Ireland)",
    "shortname": "Leo Pharma (Ireland)"
  },
  {
    "id": "openorgs____::070413e0c8f311389aaebab5c489955e",
    "name": "St. Columcille's Hospital",
    "shortname": "St. Columcille's Hospital"
  },
  {
    "id": "openorgs____::6207961f78c6486240e689616fd9831b",
    "name": "Trials Methodology Research Network",
    "shortname": "TMRN"
  },
  {
    "id": "pending_org_::227cf2acd725f272bef9ab032fa61860",
    "name": "BOSTON SCIENTIFIC IRELAND LIMITED",
    "shortname": "BOSTON SCIENTIFIC IRELAND LIMITED"
  },
  {
    "id": "openorgs____::c0bbfea2b563cfc6b72ba47bf214e643",
    "name": "Irish Research Council",
    "shortname": "IRC"
  },
  {
    "id": "openorgs____::107ee153bb02e5626426d23f522229a5",
    "name": "Dublin Business School",
    "shortname": "DBS"
  },
  {
    "id": "pending_org_::656f8c9bcbf5e11ea2bbb8e9364379c5",
    "name": "Institut NeuroMyoGène- Unité de Pysiopathologie et Génétique du Neurone et du Muscle",
    "shortname": "Institut NeuroMyoGène- Unité de Pysiopathologie et Génétique du Neurone et du Muscle"
  },
  {
    "id": "pending_org_::0fe3d7a91ebfae3bcb764d561d0d682b",
    "name": "IRISH EQUINE FOUNDATION LIMITED",
    "shortname": "IRISH EQUINE CENTRE"
  },
  {
    "id": "pending_org_::3264c1a880bb1823660eacf6e07be20d",
    "name": "MARINO INSTITUTE OF EDUCATION",
    "shortname": "MARINO INSTITUTE OF EDUCATION"
  },
  {
    "id": "pending_org_::f6a980770386f8208adfd776aa7eaf4d",
    "name": "Dublinia",
    "shortname": "Dublinia"
  },
  {
    "id": "openorgs____::ef1fa482055d4a504c6939add057e72f",
    "name": "Algae Health (Ireland)",
    "shortname": "Algae Health (Ireland)"
  },
  {
    "id": "pending_org_::8244240ab2c580a8c656e23fd0a53be8",
    "name": "UDARAS NA GAELTACHTA",
    "shortname": "Údarás na Gaeltachta"
  },
  {
    "id": "openorgs____::0c3b70bc23db7d091bf93dc21cafda06",
    "name": "Irish Management Institute",
    "shortname": "IMI"
  },
  {
    "id": "openorgs____::b205e8a99c5ed673bc15539aff7da987",
    "name": "Cancer Trials Ireland",
    "shortname": "ICORG"
  },
  {
    "id": "openorgs____::24875ad180bd19f9032fe76b545b945a",
    "name": "Discovery Programme",
    "shortname": "Discovery Programme"
  },
  {
    "id": "pending_org_::ab2a831240e409bd7bc4e550221edce3",
    "name": "BIOPLASTECH LTD",
    "shortname": "BIOPLASTECH LTD"
  },
  {
    "id": "openorgs____::375c0d4f1612074d50a81a4e52f01640",
    "name": "Mitsui & Co (Ireland)",
    "shortname": "Mitsui & Co (Ireland)"
  },
  {
    "id": "pending_org_::9ab64c897ed3cd96417d8bc4b2853276",
    "name": "ESB NETWORKS LTD",
    "shortname": "ESB Networks"
  },
  {
    "id": "pending_org_::c4bed6807546abf68f6064a7d4590ca9",
    "name": "Shire (Ireland)",
    "shortname": "Shire (Ireland)"
  },
  {
    "id": "openorgs____::53e13f390bfbc45d6333231e7f894b5a",
    "name": "Accenture (Ireland)",
    "shortname": "Accenture (Ireland)"
  },
  {
    "id": "pending_org_::09f4f28aa8e8707c0e066af1b07fa29b",
    "name": "Allergan",
    "shortname": "Allergan"
  },
  {
    "id": "pending_org_::89a8699555de0df3ebd065131439cbd2",
    "name": "WORLD VISION OF IRELAND LBG",
    "shortname": "WORLD VISION OF IRELAND"
  },
  {
    "id": "pending_org_::b98d1952330288a2c2c034007791cef0",
    "name": "XEOLAS PHARMACEUTICALS LIMITED",
    "shortname": "XEOLAS PHARMACEUTICALS LIMITED"
  },
  {
    "id": "openorgs____::c67cceccf8a23105c3fdd61b163fc89c",
    "name": "Lero",
    "shortname": "Lero"
  },
  {
    "id": "openorgs____::8beb942f1c3a08e8b9902ec88953a6c5",
    "name": "School of Physics Trinity College Dublin",
    "shortname": "School of Physics Trinity College Dublin"
  },
  {
    "id": "openorgs____::b30f6807c0c0cac0b4e9bf346dde7d96",
    "name": "Arts Council",
    "shortname": "Arts Council"
  },
  {
    "id": "openorgs____::e6b60cc7ff1e0349879d5cd1a5317e66",
    "name": "Norcontel",
    "shortname": "Norcontel"
  },
  {
    "id": "pending_org_::940bda0081a610d9871678f0aa59c97a",
    "name": "RESTORED HEARING LIMITED",
    "shortname": "RESTORED HEARING LTD"
  },
  {
    "id": "pending_org_::dd621d8159b5e352a39734caaf0283cc",
    "name": "Department of Religions and Theology Confederal School of Religions, Peace Studie Trinity College Dublin",
    "shortname": "Department of Religions and Theology Confederal School of Religions, Peace Studie Trinity College Dublin"
  },
  {
    "id": "pending_org_::8aa931724cfe6ce5577b56e05056396e",
    "name": "SSE AIRTRICITY LTD",
    "shortname": "SSE Airtricity Ltd"
  },
  {
    "id": "openorgs____::b615abcada1218dcea8bc5aa0738d4ca",
    "name": "National Suicide Research Foundation",
    "shortname": "NSRF"
  },
  {
    "id": "pending_org_::2a8325836bafc58b6a9af21f53e66ddb",
    "name": "BECTON DICKINSON RESEARCH CENTRE IRELAND LIMITED",
    "shortname": "BECTON DICKINSON RESEARCH CENTRE IRELAND LIMITED"
  },
  {
    "id": "pending_org_::8abdde333d71647fda31ad4074ef4116",
    "name": "Technological Higher Education Association",
    "shortname": "THEA"
  },
  {
    "id": "openorgs____::c4714e2f17a49d317c1fe0a3196f494a",
    "name": "Techworks Marine (Ireland)",
    "shortname": "Techworks Marine (Ireland)"
  },
  {
    "id": "openorgs____::779a8a32265c1db7ea6d941ac041e3cd",
    "name": "University of Galway",
    "shortname": "UG"
  },
  {
    "id": "pending_org_::f93a62049e16d240ab6381b81e6f6fba",
    "name": "Eureka",
    "shortname": "Eureka"
  },
  {
    "id": "openorgs____::71502eee613293bc69066fe24fbe3920",
    "name": "Cellix (Ireland)",
    "shortname": "Cellix (Ireland)"
  },
  {
    "id": "openorgs____::2fff8fd9766b8072c6d28d5d7c18c09a",
    "name": "Manor Farm (Ireland)",
    "shortname": "Manor Farm (Ireland)"
  },
  {
    "id": "pending_org_::00de9f3a15f2c6203d045db59dc50281",
    "name": "Compass Informatics",
    "shortname": "Compass Informatics"
  },
  {
    "id": "openorgs____::fb01f870be9ac9a0415409618ed0b2a4",
    "name": "International Federation of Television Archives",
    "shortname": "IFTA"
  },
  {
    "id": "openorgs____::ad7d948bf2d481a778881914475c2665",
    "name": "Heritage Council",
    "shortname": "Heritage Council"
  },
  {
    "id": "openorgs____::b0538f7f6b8a023a0b8b0dfe5bbf781b",
    "name": "Work Research Centre",
    "shortname": "WRC"
  },
  {
    "id": "openorgs____::6c3067147462e5c352255eb70c2b44b3",
    "name": "Health Research Board",
    "shortname": "HRB"
  },
  {
    "id": "openorgs____::8daab91bb7a216dea890e2404254cca6",
    "name": "RediCare (Ireland)",
    "shortname": "RediCare (Ireland)"
  },
  {
    "id": "pending_org_::451458958ae9329fa556baf6bab4ee0d",
    "name": "ECOFIX LIMITED",
    "shortname": "ECOFIX LIMITED"
  },
  {
    "id": "pending_org_::0b89b8865b98a89c3d1e945c5219ce40",
    "name": "St. John's Senior School",
    "shortname": "St. John's Senior School"
  },
  {
    "id": "pending_org_::f86cbec89c2510b9c351b85000a67775",
    "name": "ULTRA HIGH VACUUM SOLUTIONS LIMITED",
    "shortname": "UHVS LTD N"
  },
  {
    "id": "openorgs____::f4f03d36d5c31f2e34160f6569035c1d",
    "name": "Johnson & Johnson (Ireland)",
    "shortname": "Johnson & Johnson (Ireland)"
  },
  {
    "id": "openorgs____::e6f5813c8536f9c4c7e709c5a8496f90",
    "name": "Cell Stress Discoveries",
    "shortname": "CSD"
  },
  {
    "id": "pending_org_::d23a34c7a9ed6e32cabdd468ede7d6c6",
    "name": "Baboro",
    "shortname": "Baboro"
  },
  {
    "id": "pending_org_::ffc03640696d33664474e9c4acd469ef",
    "name": "ERVIA",
    "shortname": "ERVIA"
  },
  {
    "id": "openorgs____::68d58239e27440b68d81eae9cff9b289",
    "name": "Wexford General Hospital",
    "shortname": "Wexford General Hospital"
  },
  {
    "id": "openorgs____::106245b481a682fed3bec452934beda2",
    "name": "Irish Neonatal Health Alliance",
    "shortname": "Irish Neonatal Health Alliance"
  },
  {
    "id": "pending_org_::6c53c8579ff4c0bb1c6e7e652e487b3a",
    "name": "HORIZON NUA INNOVATION",
    "shortname": "HORIZON NUA INNOVATION"
  },
  {
    "id": "openorgs____::4a2595290c7a7afe8b09ec2380a43367",
    "name": "Roche (Ireland)",
    "shortname": "Roche (Ireland)"
  },
  {
    "id": "pending_org_::01b5875af05fd9a0a8d3f727fc4fc052",
    "name": "LIMERICK CITY AND COUNTY COUNCIL",
    "shortname": "LIMERICK CITY AND COUNTY COUNCIL"
  },
  {
    "id": "pending_org_::a8ccf9f372ec05eb233624004eddd22f",
    "name": "National Centre for Pharmacoeconomics",
    "shortname": "NCPE"
  },
  {
    "id": "openorgs____::3e40414568c890406b30b5878f864e2a",
    "name": "Mergon (Ireland)",
    "shortname": "Mergon (Ireland)"
  },
  {
    "id": "pending_org_::deeb81ba419ca435bfe2c5951e4ce7b9",
    "name": "Atlantic Diamond Ltd.",
    "shortname": "ATL"
  },
  {
    "id": "pending_org_::8d3280b5f9e404c3f080dd7183bd7d46",
    "name": "SHARED VISION TECHNOLOGY LIMITED",
    "shortname": "SHARED VISION TECHNOLOGY LIMITED"
  },
  {
    "id": "openorgs____::a9649af75430595f8ae092ce90116187",
    "name": "Foróige",
    "shortname": "Foróige"
  },
  {
    "id": "openorgs____::fc7afbbed077dcabc324d982793b3cf6",
    "name": "Fatima Groups United",
    "shortname": "FGU"
  },
  {
    "id": "pending_org_::600ec98bf2cdbd2fffc5fb1000bd59a1",
    "name": "Carr Communications Limited",
    "shortname": "Carr Comm"
  },
  {
    "id": "pending_org_::3825768d564caeb4b3dfc1eb54878fe2",
    "name": "NEWSWHIP MEDIA LIMITED",
    "shortname": "NEWSWHIP MEDIA LIMITED"
  },
  {
    "id": "openorgs____::3865f276eea4920d116a874908257029",
    "name": "Merrion Fertility Clinic",
    "shortname": "MFC"
  },
  {
    "id": "pending_org_::9cd6080e52cfaa12487dc97388d962fd",
    "name": "TRUE COMMUNICATION TECHNOLOGIES LIMITED",
    "shortname": "VRAI"
  },
  {
    "id": "openorgs____::4a9690104c9d6573725292cf68a73642",
    "name": "Baboro",
    "shortname": "Baboro"
  },
  {
    "id": "pending_org_::1d2a326b48793aee80c59d037cdeebf1",
    "name": "IBM IRELAND PRODUCT DISTRIBUTION LIMITED",
    "shortname": "IBM IRELAND PDL"
  },
  {
    "id": "openorgs____::5398e46a88a2dfb3f27ca11ff0ac45ce",
    "name": "National College of Ireland",
    "shortname": "NCI"
  },
  {
    "id": "openorgs____::88a2a2b7d4cbec490e47d8c3925143e6",
    "name": "King's Inns",
    "shortname": "HSKI"
  },
  {
    "id": "pending_org_::17cb856e033c3955f856371ea5d2e43d",
    "name": "AKARA ROBOTICS LIMITED",
    "shortname": "AKARA ROBOTICS LIMITED"
  },
  {
    "id": "openorgs____::638d60517a0d8735dd2050c3dc07cd1e",
    "name": "Nenagh Hospital",
    "shortname": "Nenagh Hospital"
  },
  {
    "id": "pending_org_::e29bbf4556d219a5f99ca41cc3dcca92",
    "name": "Jazz Pharmaceuticals",
    "shortname": "Jazz Pharmaceuticals"
  },
  {
    "id": "pending_org_::c1a25aed0a61192edecd1017c1b7f79f",
    "name": "MENTOR GRAPHICS (IRELAND) LTD",
    "shortname": "MENTOR"
  },
  {
    "id": "openorgs____::f9326d4529972edec691a8233d8e0375",
    "name": "Department of the Taoiseach",
    "shortname": "Department of the Taoiseach"
  },
  {
    "id": "pending_org_::81e3ad412838046a23645f272bf4f369",
    "name": "SEA AND SHORE SAFETY SERVICES LTD",
    "shortname": "SEA AND SH"
  },
  {
    "id": "openorgs____::ab5e0c6299f293469ed69106b690e867",
    "name": "BHP Laboratories",
    "shortname": "BHP"
  },
  {
    "id": "pending_org_::bcff718b480b7d75f820003a803a5ffb",
    "name": "PharmaFlow Ltd",
    "shortname": "PharmaFlow Ltd"
  },
  {
    "id": "openorgs____::11138380d09ea137ad946735dda5eb99",
    "name": "SolarPrint (Ireland)",
    "shortname": "SolarPrint (Ireland)"
  },
  {
    "id": "openorgs____::8552f560e025c89bd3b324151976e385",
    "name": "Asthma Society of Ireland",
    "shortname": "Asthma Society of Ireland"
  },
  {
    "id": "pending_org_::20dce040c9bb1121a2273c4749cb1158",
    "name": "GLEN",
    "shortname": "GLEN"
  },
  {
    "id": "pending_org_::2c0c5d75b77a8c67299d27405e4ff685",
    "name": "CREME SOFTWARE LTD",
    "shortname": "CREME SOFTWARE LTD"
  },
  {
    "id": "openorgs____::9425ff0556d0f5953b45709b6a0cf346",
    "name": "Olympic Council of Ireland",
    "shortname": "OCI"
  },
  {
    "id": "pending_org_::56e36ed6d54ca3a794cc1ad91dc81188",
    "name": "DEPUY IRELAND UNLIMITED COMPANY",
    "shortname": "DePuy Ireland"
  },
  {
    "id": "openorgs____::c11a8945db7aadd586dde21ca1479d6a",
    "name": "University College Dublin",
    "shortname": "UCD"
  },
  {
    "id": "pending_org_::4355ec4e1902f1451d2c1e7260b02b34",
    "name": "FLEMING MEDICAL LIMITED",
    "shortname": "same"
  },
  {
    "id": "pending_org_::016d6c239fcb109facb82a3e1c1274a2",
    "name": "INTERNATIONAL DEVELOPMENT IRELAND LIMITED",
    "shortname": "INTERNATIONAL DEVELOPMENT IRELAND LIMITED"
  },
  {
    "id": "openorgs____::f884200a61329c2818edf5466eaa3a9e",
    "name": "ENBIO (Ireland)",
    "shortname": "ENBIO (Ireland)"
  },
  {
    "id": "pending_org_::d8f321c29754ed925063b5642f578687",
    "name": "NATURE'S BEST LIMITED",
    "shortname": "NBL"
  },
  {
    "id": "pending_org_::d451db77ac8ec3445466e3acd2a66f34",
    "name": "TERRA GEOSERV LIMITED",
    "shortname": "TERRA GEOSERV LTD"
  },
  {
    "id": "pending_org_::891c0096c5d25c467b1f0477c254a5b4",
    "name": "European Campus Card Association",
    "shortname": "European Campus Card Association"
  },
  {
    "id": "openorgs____::c429feacb84a02c47e677ae615d671ec",
    "name": "CodeX Discovery (Ireland)",
    "shortname": "CodeX Discovery (Ireland)"
  },
  {
    "id": "pending_org_::9d4fcbb87c5ffcd144e7af5b21374021",
    "name": "COSMOS EDUCATION LIMITED LBG",
    "shortname": "BLACKROCK CASTLE OBSERVATORY"
  },
  {
    "id": "pending_org_::e258aca07ef089752d9e20bcb15ee2e8",
    "name": "VIRTUAL REALITY IRELAND MEDIA LTD",
    "shortname": "VIRTUAL REALITY IRELAND MEDIA LTD"
  },
  {
    "id": "openorgs____::d588a0c4320072b04145136aa73aea35",
    "name": "Inclusion Ireland",
    "shortname": "Inclusion Ireland"
  },
  {
    "id": "openorgs____::0d781b5941ba190b687f181245ef636a",
    "name": "Microsoft (Ireland)",
    "shortname": "Microsoft (Ireland)"
  },
  {
    "id": "pending_org_::794c7c256e70cc759988d95e48918d75",
    "name": "THEA",
    "shortname": "THEA"
  },
  {
    "id": "openorgs____::2f313d6350a73e03df03f1d28d1cca28",
    "name": "Western Development Commission",
    "shortname": "WDC"
  },
  {
    "id": "openorgs____::087977c2c088a6b56731663295c73a04",
    "name": "Charitable Infirmary",
    "shortname": "Charitable Infirmary"
  },
  {
    "id": "openorgs____::7a3ebb814b345a2988962b329e3c5249",
    "name": "Leopardstown Park Hospital",
    "shortname": "Leopardstown Park Hospital"
  },
  {
    "id": "openorgs____::8d3d661d3c7b803839156fa0cf6082bb",
    "name": "MCO (Ireland)",
    "shortname": "MCO (Ireland)"
  },
  {
    "id": "pending_org_::3ce470bec9abd70c2f20f22728460ff2",
    "name": "Youth RISE (Resources, Information, Support and Education)",
    "shortname": "Youth RISE (Resources, Information, Support and Education)"
  },
  {
    "id": "pending_org_::578ef3748e5b98c4787856a632e4c66b",
    "name": "MERRION PHARMACEUTICALS IRELAND LTD",
    "shortname": "MPIL"
  },
  {
    "id": "pending_org_::35b42753921521a1519961e9afc2e092",
    "name": "Rannís",
    "shortname": "Rannís"
  },
  {
    "id": "pending_org_::01d4ee2937bc5cb55d9dc11b503ea76e",
    "name": "RENALYTIX AI LIMITED",
    "shortname": "RENALYTIX"
  },
  {
    "id": "pending_org_::f68ef8988afe0a5f444e9638c7b3f32b",
    "name": "CLANWILLIAM HEALTH LIMITED",
    "shortname": "CLANWILLIAM"
  },
  {
    "id": "pending_org_::e91333b5e27ee4e8004840a45bb0cc44",
    "name": "OVAGEN LIMITED",
    "shortname": "OVAGEN LIMITED"
  },
  {
    "id": "openorgs____::dacfe68b73710d3bc388429e947b596e",
    "name": "National Museum of Ireland",
    "shortname": "NMI"
  },
  {
    "id": "pending_org_::8eadb92bca70124f52f9b14a4efb64da",
    "name": "INNOPHARMA LABS LIMITED",
    "shortname": "INNOPHARMA"
  },
  {
    "id": "openorgs____::bc372318234c60db1bf616d06229eff4",
    "name": "Meath Foundation",
    "shortname": "Meath Foundation"
  },
  {
    "id": "openorgs____::e31c7b74171e1986f2636bcf6a9eab68",
    "name": "Whipsmart Media (Ireland)",
    "shortname": "WSM"
  },
  {
    "id": "pending_org_::45fae9b1e98ddcd080056aeebaf7ee0a",
    "name": "ECHALLIANCE COMPANY LIMITED BY GUARANTEE",
    "shortname": "ECHALLIANCE COMPANY LIMITED BY GUARANTEE"
  },
  {
    "id": "pending_org_::a4f8a79de3942bfc1636a988d5576903",
    "name": "LIGHTWAVE OPTICS LTD (IRISH PRECISION OPTICS)",
    "shortname": "IPO"
  },
  {
    "id": "openorgs____::6e52f2cb80285ec78c7938f942190ca6",
    "name": "Seagate (Ireland)",
    "shortname": "Seagate (Ireland)"
  },
  {
    "id": "pending_org_::110f03a7257f2b7018c571099d006b7a",
    "name": "SLIGO COUNTY COUNCIL",
    "shortname": "SLIGO COUNTY COUNCIL"
  },
  {
    "id": "pending_org_::4534478006c78a148cd2ffeb05471e25",
    "name": "RED HAT LIMITED",
    "shortname": "RHAT"
  },
  {
    "id": "pending_org_::af1d5eee9a9a1592d61a4bfaa2ea4268",
    "name": "M & M QUALTECH LIMITED",
    "shortname": "M & M QUALTECH LIMITED"
  },
  {
    "id": "openorgs____::e03bc3944fe8e7e328714aae9077bad6",
    "name": "Bon Secours Hospital Dublin",
    "shortname": "Bon Secours Hospital Dublin"
  },
  {
    "id": "pending_org_::546733a10372d52523bea5344e5eb4fd",
    "name": "Irish Manufacturing Research (IMR)",
    "shortname": "Irish Manufacturing Research (IMR)"
  },
  {
    "id": "pending_org_::f38095a41e561e07ca6db3c9aeaa780d",
    "name": "NOKIA IRELAND LIMITED",
    "shortname": "Nokia Ireland Limited"
  },
  {
    "id": "pending_org_::bf0de263fd9575aca3f026a50d2074d6",
    "name": "FRAMEWORK COMPUTER CONSULTANTS LIMITED",
    "shortname": "FRAMEWORK COMPUTER CONSULTANTS LIMITED"
  },
  {
    "id": "openorgs____::f27f3c9496922dece130649c60586e4d",
    "name": "Local Government Management Agency",
    "shortname": "LGMA"
  },
  {
    "id": "openorgs____::33455d2c39978c3cf4ebcf9ced6c06b4",
    "name": "Cork City Council",
    "shortname": "Cork City Council"
  },
  {
    "id": "openorgs____::9d0da28e5a228752bd32d0c070719a62",
    "name": "Vhi Healthcare",
    "shortname": "Vhi Healthcare"
  },
  {
    "id": "pending_org_::e6ae1516c5ff11dbf69421c93bf0ca60",
    "name": "SMART WIRE GRID EUROPE LIMITED",
    "shortname": "Smart Wires Europe"
  },
  {
    "id": "pending_org_::3ccbec8d68412899375cd6a4b6908098",
    "name": "NATIONAL CATTLE BREEDING CENTRE LIMITED",
    "shortname": "NCBC"
  },
  {
    "id": "pending_org_::c9f436dc5f00f51cd094253ac9c1e4b6",
    "name": "BLACKSTAIRS LITHIUM LTD",
    "shortname": "BLACKSTAIRS LITHIUM LTD"
  },
  {
    "id": "pending_org_::16da23ae515bebe4ec170018d8dc67d2",
    "name": "SYNOPSYS INTERNATIONAL LIMITED",
    "shortname": "SYNOPSYS"
  },
  {
    "id": "openorgs____::447327d561548833cc9a4ef26c7a95db",
    "name": "St. Dympna's Hospital",
    "shortname": "St. Dympna's Hospital"
  },
  {
    "id": "pending_org_::4cd761f48261be70f8dd641db7109b6a",
    "name": "ORBSEN THERAPEUTICS LIMITED",
    "shortname": "ORBSEN THERAPEUTICS LIMITED"
  },
  {
    "id": "pending_org_::562aa3f0e479ad13ec7ee6004c297c3d",
    "name": "ADVANCED TECHNOLOGIES FORWARDING (ATF) COMPANY LIMITED",
    "shortname": "ADVANCED TECHNOLOGIES FORWARDING (ATF) COMPANY LIMITED"
  },
  {
    "id": "openorgs____::402f9f564f75870a6060525478f18232",
    "name": "Portiuncula Hospital",
    "shortname": "Portiuncula Hospital"
  },
  {
    "id": "openorgs____::c4cb8ce5e6b834d182b70c0f331f5d44",
    "name": "St. Davnet's Hospital",
    "shortname": "St. Davnet's Hospital"
  },
  {
    "id": "openorgs____::74b171118453ec2ead8ae552f3c62a33",
    "name": "The Office of Public Works",
    "shortname": "OPW"
  },
  {
    "id": "openorgs____::e061fac719d106c4277a2199df990a91",
    "name": "SR Technics (Ireland)",
    "shortname": "SR Technics (Ireland)"
  },
  {
    "id": "pending_org_::a718f4398ab68e3fca0d783e64a745fb",
    "name": "CROSPON LIMITED",
    "shortname": "CROSPON LTD"
  },
  {
    "id": "pending_org_::ef6b6b746186749393ab2a908793534c",
    "name": "UCC ACADEMY DESIGNATED ACTIVITY COMPANY",
    "shortname": "UCC ACADEMY DAC"
  },
  {
    "id": "openorgs____::82ab999355f01e7deeef74d0c06d3bdf",
    "name": "TE Laboratories (Ireland)",
    "shortname": "TE Laboratories (Ireland)"
  },
  {
    "id": "pending_org_::4091b418776089baae9f3d712b859967",
    "name": "NOWCASTING INTERNATIONAL LIMITED",
    "shortname": "NOWCASTING INTERNATIONAL LIMITED"
  },
  {
    "id": "pending_org_::494d0c3d91592802249fe79dd4b0f0c3",
    "name": "APC Microbiome Institute",
    "shortname": "APC Microbiome Institute"
  },
  {
    "id": "openorgs____::e7a28cb4a89428c8949cb3af8ee11380",
    "name": "ActionAid Ireland",
    "shortname": "ActionAid Ireland"
  },
  {
    "id": "openorgs____::1386167b3c658093f5c671f2ef9593b4",
    "name": "Aptiv (Ireland)",
    "shortname": "Aptiv (Ireland)"
  },
  {
    "id": "openorgs____::15fb2c0b223ca34ec651f34838d2daf9",
    "name": "AquaTT (Ireland)",
    "shortname": "AquaTT (Ireland)"
  },
  {
    "id": "openorgs____::b2123c7733b23616db04287fd467a12f",
    "name": "Enable Ireland",
    "shortname": "Enable Ireland"
  },
  {
    "id": "openorgs____::79e6791dfb6ac724581f186481ca091a",
    "name": "NortonLifeLock (Ireland)",
    "shortname": "NortonLifeLock (Ireland)"
  },
  {
    "id": "pending_org_::3a43681435df0316e65a9a7e92490228",
    "name": "KITE MEDICAL LIMITED",
    "shortname": "KITE MEDICAL LIMITED"
  },
  {
    "id": "pending_org_::26ab893406432b4556e20af3b0389421",
    "name": "Discover STM Publishing Ltd",
    "shortname": "Discover STM Publishing Ltd"
  },
  {
    "id": "openorgs____::ed2d8d61c0863f2dd52ce7d3c4da3667",
    "name": "Amarin (Ireland)",
    "shortname": "Amarin (Ireland)"
  },
  {
    "id": "pending_org_::e3f08b187de22ad7fa91823949281e48",
    "name": "Coláiste Náisiúnta na hÉireann",
    "shortname": "NCI"
  },
  {
    "id": "openorgs____::999ad60f5a659005fcad916920944741",
    "name": "Irish College of General Practitioners",
    "shortname": "Irish College of General Practitioners"
  },
  {
    "id": "pending_org_::f2afebbc24f74979b85671abd3ad39ad",
    "name": "IRISH CLINICAL ONCOLOGY RESEARCH GROUP LIMITED LBG",
    "shortname": "ICORG"
  },
  {
    "id": "pending_org_::baed459b6ab4e251f8bf4e6107b4161f",
    "name": "BORD OSPIDEIL NAOIMH SHEAMUIS",
    "shortname": "BORD OSPIDEIL NAOIMH SHEAMUIS"
  },
  {
    "id": "pending_org_::6cb772ca0ad06479f3f0d78fdf1810b7",
    "name": "Saint Josephs",
    "shortname": "Saint Josephs"
  },
  {
    "id": "pending_org_::fc19cdafd76e93510855d63f82f6d1d4",
    "name": "ATTUROS LIMITED",
    "shortname": "Atturos"
  },
  {
    "id": "openorgs____::d2cd26fc6ebe043f790fa6b5c8aa0b2f",
    "name": "National University of Ireland, Maynooth",
    "shortname": "NUIM"
  },
  {
    "id": "openorgs____::70360bc93ee8ff24f9c46699b98585c6",
    "name": "National Instruments (Ireland)",
    "shortname": "National Instruments (Ireland)"
  },
  {
    "id": "pending_org_::28c0f72d1322da1d347115bded751d49",
    "name": "HOME INSTEAD FRANCHISING LTD",
    "shortname": "Home Instead Senior Care"
  },
  {
    "id": "openorgs____::306243a733f87c98cfcbe182b2a3792e",
    "name": "Tusla - Child and Family Agency",
    "shortname": "Tusla - Child and Family Agency"
  },
  {
    "id": "pending_org_::f53578f5c2b6de73c4794a6952c6d58f",
    "name": "GLASPORT BIO LIMITED",
    "shortname": "GLASPORT BIO LIMITED"
  },
  {
    "id": "pending_org_::2b3f2fc01e55f9dfe992631ecfd52e0b",
    "name": "AISR",
    "shortname": "AISR"
  },
  {
    "id": "openorgs____::a65314a8f9798766b27fbab65c1b5d9d",
    "name": "Novartis (Ireland)",
    "shortname": "Novartis (Ireland)"
  },
  {
    "id": "openorgs____::826b05599f2677dc8b22f45f655f7b15",
    "name": "Respond Housing Association",
    "shortname": "Respond Housing Association"
  },
  {
    "id": "pending_org_::22447a5111b3df678a228bd37e2666b5",
    "name": "EBLANA PHOTONICS LIMITED",
    "shortname": "EBLANA"
  },
  {
    "id": "openorgs____::0414fdcd37654e7000b3379eabbf8b09",
    "name": "Irish Heritage Trust",
    "shortname": "Irish Heritage Trust"
  },
  {
    "id": "openorgs____::5e21aab857ef3d4269e9c1aa5580ae84",
    "name": "Marigot (Ireland)",
    "shortname": "Marigot (Ireland)"
  },
  {
    "id": "openorgs____::65115e1ec3ae04e5bb4c5d167ea75485",
    "name": "Voysis (Ireland)",
    "shortname": "Voysis (Ireland)"
  },
  {
    "id": "openorgs____::a373dc83797f15433ead322933cc672c",
    "name": "Nuritas (Ireland)",
    "shortname": "Nuritas (Ireland)"
  },
  {
    "id": "pending_org_::4853e4c07f1ff51049791b1686a6a455",
    "name": "CUSTOM COMPOST PRIVATE UNLIMITED COMPANY",
    "shortname": "CUSTOM COMPOST"
  },
  {
    "id": "openorgs____::30557c5bb1ef6d194a2cc7bb4e85b02d",
    "name": "Teleflex (Ireland)",
    "shortname": "Teleflex (Ireland)"
  },
  {
    "id": "pending_org_::0506af25ce7f9e79a74b3e14bd205776",
    "name": "BENTLEY SYSTEMS INTERNATIONAL LIMITED",
    "shortname": "Bentley Systems"
  },
  {
    "id": "pending_org_::9eb54caaf970068a40aaafc4237354aa",
    "name": "NVP ENERGY LIMITED",
    "shortname": "NVP ENERGY"
  },
  {
    "id": "pending_org_::941366c3937d916bc64e00b00abeb88e",
    "name": "G & M STEEL FABRICATORS LIMITED",
    "shortname": "G & M STEEL FABRICATORS LIMITED"
  },
  {
    "id": "openorgs____::ec845342c2a1edeaba6020607e94986e",
    "name": "All Hallows College",
    "shortname": "All Hallows College"
  },
  {
    "id": "openorgs____::3f0bd8660050ee5d12a4227150f7e396",
    "name": "Ingersoll Rand (Ireland)",
    "shortname": "Ingersoll Rand (Ireland)"
  },
  {
    "id": "pending_org_::4b9a345e725709fd725cad004e87fa35",
    "name": "Havok",
    "shortname": "Havok"
  },
  {
    "id": "pending_org_::462eaec977e218f9b1312c8596f54b81",
    "name": "ERVIA",
    "shortname": "ERVIA"
  },
  {
    "id": "pending_org_::cb3f77ae888210b59ad6ad98c65d2c82",
    "name": "ROUGHAN & O'DONOVAN LIMITED",
    "shortname": "ROD"
  },
  {
    "id": "pending_org_::64732ddc93bca2a58a9c931f89747a0b",
    "name": "Nogra Pharma (Ireland)",
    "shortname": "Nogra Pharma (Ireland)"
  },
  {
    "id": "openorgs____::ffd92e078a06bd5cd8437672e4894d06",
    "name": "Element Six (Ireland)",
    "shortname": "Element Six (Ireland)"
  },
  {
    "id": "openorgs____::7bce12979f1a84a1215b37607e9da949",
    "name": "Centre of Synthesis and Chemical Biology School of Chemistry Trinity College Dublin",
    "shortname": "Centre of Synthesis and Chemical Biology School of Chemistry Trinity College Dublin"
  },
  {
    "id": "openorgs____::24a5f76cfde1e45c4ed6fbe2f1a32899",
    "name": "An Garda Síochána",
    "shortname": "An Garda Síochána"
  },
  {
    "id": "pending_org_::d662970281a58a82c915863d7b9650c1",
    "name": "Horizon Pharma",
    "shortname": "Horizon Pharma"
  },
  {
    "id": "openorgs____::3cb19734da640f9193b20291268c9a0e",
    "name": "Marine Institute",
    "shortname": "Marine Institute"
  },
  {
    "id": "openorgs____::dbfec8c4d850003386341fabf2ce10f9",
    "name": "Bodywhys",
    "shortname": "Bodywhys"
  },
  {
    "id": "pending_org_::1637dba433e4540f2789ab9832fa2ff6",
    "name": "AURIGEN MEDICAL LIMITED",
    "shortname": "AURIGEN MEDICAL LIMITED"
  },
  {
    "id": "pending_org_::638297f55924d0ad8af879081ad6fcf6",
    "name": "VISUM LIMITED",
    "shortname": "VISUM"
  },
  {
    "id": "pending_org_::de1f73f258bcb0a548c98a1d2654bcb8",
    "name": "ADVANCED THREE D TECHNOLOGY LIMITED",
    "shortname": "3D TECHNOLOGY MEDSCAN3D"
  },
  {
    "id": "pending_org_::ec5287bab64082429fd636c11f53ec28",
    "name": "FORFAS",
    "shortname": "Forfas"
  },
  {
    "id": "openorgs____::3dafd1c5d9f6a66650ee39ba86616e3e",
    "name": "Multiple Sclerosis Society of Ireland",
    "shortname": "Multiple Sclerosis Society of Ireland"
  },
  {
    "id": "pending_org_::86f5764a233fd95cd76f954793e748d9",
    "name": "THE DUBLIN RAPE CRISIS CENTRE LIMITED",
    "shortname": "DUBLIN RAPE CRISIS CENTRE"
  },
  {
    "id": "openorgs____::1192fd50a632f8ce0d7b6241123ea77b",
    "name": "Genomics Medicine (Ireland)",
    "shortname": "GMI"
  },
  {
    "id": "pending_org_::f80b39c7d2f8f8026a14a4c1009c8001",
    "name": "XILINX IRELAND UNLIMITED COMPANY",
    "shortname": "XILINX IRELAND UNLIMITED COMPANY"
  },
  {
    "id": "openorgs____::66ec076d88f35d6b7bf55fd6344ceb04",
    "name": "MicroGen Biotech (Ireland)",
    "shortname": "MicroGen Biotech (Ireland)"
  },
  {
    "id": "openorgs____::48b59e105d9d55dda9dbaf65f1bfe72d",
    "name": "Enterprise Ireland",
    "shortname": "Enterprise Ireland"
  },
  {
    "id": "openorgs____::b5c68d97bbc3b4ca8ee91420693042ab",
    "name": "Fighting Blindness",
    "shortname": "Fighting Blindness"
  },
  {
    "id": "pending_org_::7a4bb7e2a8d77fa629b300cbee685919",
    "name": "Alcon (Ireland)",
    "shortname": "Alcon (Ireland)"
  },
  {
    "id": "openorgs____::9143d784bc3b1a85992aeb68f6cd6b6b",
    "name": "KLA (Ireland)",
    "shortname": "KLA (Ireland)"
  },
  {
    "id": "pending_org_::874f2bbcc1ee0d6dd6cfd8c61d90fd5f",
    "name": "AVECTAS LIMITED",
    "shortname": "AVECTAS LIMITED"
  },
  {
    "id": "openorgs____::f7b1f3f21a32f20fc51aeabf4077f503",
    "name": "Arthritis Ireland",
    "shortname": "Arthritis Ireland"
  },
  {
    "id": "pending_org_::8f83a460a0d993f184c81d8a1488c24e",
    "name": "Holfeld Plastics Limited",
    "shortname": "Holfeld Plastics Limited"
  },
  {
    "id": "openorgs____::2e2879bffc6e3cb50472b022b2585959",
    "name": "National Digital Research Centre",
    "shortname": "NDRC"
  },
  {
    "id": "pending_org_::7eac928a2962057e4105706b776ea057",
    "name": "PROXY BIOMEDICAL LIMITED",
    "shortname": "PROXY BIOMEDICAL LIMITED"
  },
  {
    "id": "pending_org_::b88f56d8ae0034daa9c18290818c012a",
    "name": "IRISH PLATFORM FOR PATIENTS' ORGANISATIONS SCIENCE AND INDUSTRY LIMITED BY GUARANTEE",
    "shortname": "IPPOSI"
  },
  {
    "id": "pending_org_::f78fa07583f3eb84d57e5467cc997ff6",
    "name": "REDZINC SERVICES LIMITED",
    "shortname": "REDZINC"
  },
  {
    "id": "pending_org_::3fa86e20d19984ca15ca9fbef022834c",
    "name": "ENVIROTECH INNOVATIVE PRODUCTS LTD",
    "shortname": "ENVIROTECH INNOVATIVE PRODUCTS LTD"
  },
  {
    "id": "pending_org_::e85eb49b03eab159754f546211965b49",
    "name": "TOPCHEM PHARMACEUTICALS LIMITED",
    "shortname": "TOPCHEM PHARMACEUTICALS LIMITED"
  },
  {
    "id": "pending_org_::2b469c75c5f9e1cd210d313c9885de97",
    "name": "BIOSTOR IRELAND LIMITED",
    "shortname": "BIOSTOR"
  },
  {
    "id": "pending_org_::8f24d847c134c6d9d7c82dd6d3675307",
    "name": "THE DISCOVERY PROGRAMME : CENTRE FOR ARCHAEOLOGY AND INNOVATION IRELAND",
    "shortname": "THE DISCOVERY PROGRAMME : CENTRE FOR ARCHAEOLOGY AND INNOVATION IRELAND"
  },
  {
    "id": "pending_org_::839061567ada77bc4602c5eb8102b8e4",
    "name": "GALWAY CITY COUNCIL",
    "shortname": "GALWAY CITY COUNCIL"
  },
  {
    "id": "pending_org_::cd12663d1ac2c5fea4d9a7cdd2bc6058",
    "name": "AN COMHAIRLE OIDHREACHTA-THE HERITAGE COUNCIL HC",
    "shortname": "HC"
  },
  {
    "id": "pending_org_::4803584bc8e1f8f80d3d19335d5b6dec",
    "name": "COLUMBA GLOBAL SYSTEMS LIMITED",
    "shortname": "COLUMBA"
  },
  {
    "id": "orgreg______::873bc59cddc9b9a0aa1163e89371d3e3",
    "name": "Health Protection Surveillance Centre",
    "shortname": "HPSC"
  },
  {
    "id": "openorgs____::0771c1a08b59b30e72cdf3f852f9f3d4",
    "name": "Sulzer (Ireland)",
    "shortname": "Sulzer (Ireland)"
  },
  {
    "id": "pending_org_::32763ec64e4a63fff55927749ca9f70a",
    "name": "CELLULAC LIMITED",
    "shortname": "SBL"
  },
  {
    "id": "pending_org_::5091dce172c413db32785dc3147e01e1",
    "name": "NATIONAL COUNCIL FOR CURRICULUM AND ASSESSMENT",
    "shortname": "NATIONAL COUNCIL FOR CURRICULUM AND ASSESSMENT"
  },
  {
    "id": "openorgs____::053fb6ce7fdc555f4873590510344fa2",
    "name": "Central Bank of Ireland",
    "shortname": "Central Bank of Ireland"
  },
  {
    "id": "openorgs____::e0235b9109f54e3e27f028ae1c7732a6",
    "name": "Digital Repository of Ireland",
    "shortname": "DRI"
  },
  {
    "id": "pending_org_::53d5a5f9e25dbe70d907f8ba89a7012e",
    "name": "SMART M POWER COMPANY LIMITED",
    "shortname": "SMART M POWER COMPANY LIMITED"
  },
  {
    "id": "pending_org_::0517822b8ff2fbd721d57e89549787f8",
    "name": "The Label Company",
    "shortname": "The Label Company"
  },
  {
    "id": "openorgs____::f31a9a0b45a1c9b300360edbc7dd3912",
    "name": "DeafHear",
    "shortname": "DeafHear"
  },
  {
    "id": "openorgs____::624552a5d8c26aab4e87553282e17b67",
    "name": "Eaton (Ireland)",
    "shortname": "Eaton (Ireland)"
  },
  {
    "id": "openorgs____::8450ffce7ea523a2033f5d96c6ff87f6",
    "name": "Jazz Pharmaceuticals (Ireland)",
    "shortname": "Jazz Pharmaceuticals (Ireland)"
  },
  {
    "id": "pending_org_::37f4a2db0671b6b4e495decc914b9c65",
    "name": "FEIRMEOIRI AONTUITHE NA H-EIREANN IONTAOBIATHE CUIDEACHTA FAOI THEORAINN RATHAIOCHTA",
    "shortname": "THE IRISH FARMERS ASSOCIATION"
  },
  {
    "id": "openorgs____::1d465b9abf9f41503afbf8fdae17f0b2",
    "name": "Our Lady's Hospital",
    "shortname": "Our Lady's Hospital"
  },
  {
    "id": "openorgs____::82078ad5d6f59c8c4447b0ee5561a06f",
    "name": "Tile Films (Ireland)",
    "shortname": "Tile Films (Ireland)"
  },
  {
    "id": "openorgs____::f9c6be353685c0341c494a785540e71e",
    "name": "Royal Dublin Society",
    "shortname": "RDS"
  },
  {
    "id": "ror_________::5e1a83fb3f66a77830e22fa86781a22f",
    "name": "Amnesty International Ireland",
    "shortname": "AI"
  },
  {
    "id": "pending_org_::b3510f0fe46d84fd7630e478569ccddf",
    "name": "O'MALLEY FISHERIES",
    "shortname": "O'MALLEY FISHERIES"
  },
  {
    "id": "pending_org_::97b5b637cb256dc5c41d9c35486dac73",
    "name": "DEVENISH RESEARCH DEVELOPMENT AND INNOVATION LIMITED",
    "shortname": "DEVENISH RESEARCH DEVELOPMENT AND INNOVATION"
  },
  {
    "id": "openorgs____::e7abca31981118cb3f5eb12e81a1519b",
    "name": "European Foundation for the Improvement of Living and Working Conditions",
    "shortname": "European Foundation for the Improvement of Living and Working Conditions"
  },
  {
    "id": "pending_org_::be33f9413a7319da80cba2bbbdcfb1e7",
    "name": "SUITE5 DATA INTELLIGENCE SOLUTIONS LIMITED",
    "shortname": "SUITE5 DATA INTELLIGENCE SOLUTIONS LIMITED"
  },
  {
    "id": "openorgs____::02be5a6023ab45b731414cafe6ebe76c",
    "name": "Institute of Public Health",
    "shortname": "IPH"
  },
  {
    "id": "pending_org_::872f67d68eef1c97e45be08eef5573a3",
    "name": "PILZ IRELAND INDUSTRIAL AUTOMATION DISTRIBUTION",
    "shortname": "PILZ IRELAND INDUSTRIAL AUTOMATION DISTRIBUTION"
  },
  {
    "id": "pending_org_::21980bf2ca3e134607db587b65b1b6aa",
    "name": "COOK IRELAND LIMITED",
    "shortname": "COOK"
  },
  {
    "id": "openorgs____::f94e25d0343d3c2487f9c2890aef0983",
    "name": "Era-Maptec (Ireland)",
    "shortname": "Era-Maptec (Ireland)"
  },
  {
    "id": "openorgs____::d1c59232033d3516e44ac909e05df765",
    "name": "Children's Leukaemia Research Project",
    "shortname": "CLRP"
  },
  {
    "id": "pending_org_::7d59f73a6b2a8c4d4f7e5f52cd73ca31",
    "name": "MULTIMEDIA COMPUTER SYSTEM LTD",
    "shortname": "MicroPro"
  },
  {
    "id": "openorgs____::7a25dc08596c5f28c29a68be4c7bfec4",
    "name": "Irish Hospice Foundation",
    "shortname": "IHF"
  },
  {
    "id": "openorgs____::5c7244d329a230838038062858b1b513",
    "name": "Central Statistics Office",
    "shortname": "CSO"
  },
  {
    "id": "openorgs____::4c5ad0ab1f469ddd84dd19f0cc80b796",
    "name": "Data Fusion International (Ireland)",
    "shortname": "DFI"
  },
  {
    "id": "openorgs____::f25c99a115a2693e671ee99a6906df4e",
    "name": "The Ark",
    "shortname": "The Ark"
  },
  {
    "id": "pending_org_::ffc9ec395784087250afd7bca3b1eca8",
    "name": "Synthesis and Solid State Pharmaceutical Centre",
    "shortname": "Synthesis and Solid State Pharmaceutical Centre"
  },
  {
    "id": "pending_org_::b1acb2269d370c93a7771f0f37e0be3f",
    "name": "ST jOHNS NS",
    "shortname": "ST jOHNS NS"
  },
  {
    "id": "pending_org_::2f462f144c6e0ca2d4630c2f400a74bb",
    "name": "DecaWave Limited",
    "shortname": "DecaWave"
  },
  {
    "id": "pending_org_::16e10c707218114504f2277dca2ee5b9",
    "name": "European Health Futures Forum",
    "shortname": "European Health Futures Forum"
  },
  {
    "id": "pending_org_::fa1210af4a84522b83735a0f7e6cc462",
    "name": "IRISH BIOECONOMY FOUNDATION",
    "shortname": "IRISH BIOECONOMY FOUNDATION"
  },
  {
    "id": "openorgs____::3802b84d6e51078fcc26cccfe4d5ece7",
    "name": "Inland Fisheries Ireland",
    "shortname": "Inland Fisheries Ireland"
  },
  {
    "id": "pending_org_::432eb9d95d47e9f3fe78856523525434",
    "name": "Eli Lilly S.A. - Irish Branch",
    "shortname": "Eli Lilly S.A. - Irish Branch"
  },
  {
    "id": "openorgs____::f52ec5d11558643690c9d57caae662e5",
    "name": "Origin Enterprises (Ireland)",
    "shortname": "Origin Enterprises (Ireland)"
  },
  {
    "id": "pending_org_::262f809d9e8dfed8de53f968f6bc0a0c",
    "name": "COMMUNICRAFT LIMITED",
    "shortname": "Communicraft Ltd"
  },
  {
    "id": "pending_org_::4260d5e53ca7061e83df84d9409b1e52",
    "name": "Compuscript Ltd",
    "shortname": "Compuscript Ltd"
  },
  {
    "id": "openorgs____::ffd05a505e7ea108e060aeee46820631",
    "name": "Texas Instruments (Ireland)",
    "shortname": "TI"
  },
  {
    "id": "openorgs____::8be3895d2189516a75b35080050b64ae",
    "name": "Kelada Pharmachem (Ireland)",
    "shortname": "Kelada Pharmachem (Ireland)"
  },
  {
    "id": "pending_org_::b5c46135882516633fc65defc2ba4692",
    "name": "Ireland Environmental Protection Agency, Environmental Research Centre",
    "shortname": "EPA"
  },
  {
    "id": "openorgs____::c6ff212f9d07f65925953a1700edb883",
    "name": "Royal Victoria Eye and Ear Hospital",
    "shortname": "RVEEH"
  },
  {
    "id": "openorgs____::68918f079f82b0fb62b46121e20ccd1b",
    "name": "Castolin Eutectic (Ireland)",
    "shortname": "Castolin Eutectic (Ireland)"
  },
  {
    "id": "openorgs____::369470f3cd77ff374d43c412e40243f8",
    "name": "Front Line Defenders",
    "shortname": "Front Line Defenders"
  },
  {
    "id": "pending_org_::a6b50744045b7b4f032bdb8a6568bfc7",
    "name": "PLASMA LEAP TECHNOLOGIES LIMITED",
    "shortname": "PlasmaLeap Technologies"
  },
  {
    "id": "openorgs____::a4b46d194b1fab89cc2217c5a4688acd",
    "name": "International Information Management Corporation",
    "shortname": "IIMC"
  },
  {
    "id": "pending_org_::0a84c0dcf543f3de433b13e958b03ea2",
    "name": "SlidePath",
    "shortname": "SlidePath"
  },
  {
    "id": "openorgs____::4c4fbc190048c821f4983242697b992f",
    "name": "Food Safety Authority of Ireland",
    "shortname": "FSAI"
  },
  {
    "id": "openorgs____::08175bd00292b1462d21e754d2b7fa08",
    "name": "Department of Defence",
    "shortname": "DoD"
  },
  {
    "id": "pending_org_::7a8690474d0a177430b16282bd7a4c03",
    "name": "COMPOSITE RECYCLING LIMITED",
    "shortname": "COMPOSITE RECYCLING LTD"
  },
  {
    "id": "openorgs____::525e92ca09b967d00960c6a6043d0335",
    "name": "St. Vincent's University Hospital",
    "shortname": "St. Vincent's University Hospital"
  },
  {
    "id": "openorgs____::01cf5f23db93bbf05b4d37019193ce99",
    "name": "Coillte (Ireland)",
    "shortname": "Coillte (Ireland)"
  },
  {
    "id": "pending_org_::3cf47443dec765cef151f9a3eea89f86",
    "name": "CONNECT THE DOTS EVENTS LIMITED",
    "shortname": "CONNECT THE DOTS EVENTS LIMITED"
  },
  {
    "id": "pending_org_::19134cb9f5754546015f6431d6f126ad",
    "name": "Interactions Limited",
    "shortname": "INTR"
  },
  {
    "id": "openorgs____::29d9e0c276a8534bf51b28ff890ce013",
    "name": "Vornia Biomaterials (Ireland)",
    "shortname": "Vornia Biomaterials (Ireland)"
  },
  {
    "id": "pending_org_::fd30b7efb621a610036cd154a89f846e",
    "name": "Daysha Consulting limited",
    "shortname": "Daysha"
  },
  {
    "id": "openorgs____::4a174411b717832bd892466a142aa9f6",
    "name": "Government of Ireland",
    "shortname": "Government of Ireland"
  },
  {
    "id": "pending_org_::3b6c526bc8fd6786e982e113e77a070d",
    "name": "THE IRISH ANAPHYLAXIS CAMPAIGN LIMITED BY GUARANTEE",
    "shortname": "ANAPHYLAXI"
  },
  {
    "id": "openorgs____::990d73d5d37ce3deb1cd5289910dd001",
    "name": "Pieta House",
    "shortname": "Pieta House"
  },
  {
    "id": "openorgs____::8d3ddb142ee485bd8d1640bc627716a9",
    "name": "Eblana Photonics (Ireland)",
    "shortname": "Eblana Photonics (Ireland)"
  },
  {
    "id": "openorgs____::b6f0c101d37812b5c43d046a62ca54d7",
    "name": "Eolas (Ireland)",
    "shortname": "Eolas (Ireland)"
  },
  {
    "id": "openorgs____::6f00131c6d2328793bf1bcff9b38b463",
    "name": "Reprodoc (Ireland)",
    "shortname": "Reprodoc (Ireland)"
  },
  {
    "id": "pending_org_::8e0b0ef0e5798b4b11f52951b6ec751d",
    "name": "EIRECOMPOSITES TEORANTA",
    "shortname": "EC"
  },
  {
    "id": "pending_org_::fcbe8a061e1c8638a6d0d08e9a8efce8",
    "name": "ATXA THERAPEUTICS LTD",
    "shortname": "ATXA THERAPEUTICS LTD"
  },
  {
    "id": "openorgs____::21d9c935ca905a5a939666ae3440280d",
    "name": "Crospon (Ireland)",
    "shortname": "Crospon (Ireland)"
  },
  {
    "id": "pending_org_::11e02c4e556a37a057b10561592a568b",
    "name": "VOYAGERIP INTERNATIONAL SERVICES LIMITED",
    "shortname": "VOYAGERIP INTERNATIONAL SERVICES LIMITED"
  },
  {
    "id": "pending_org_::ca860900b299e8615e31b2c974e60cf6",
    "name": "PMD DEVICE SOLUTIONS LIMITED",
    "shortname": "PMD Device Solutions Limited"
  },
  {
    "id": "openorgs____::3d8cab7843d75bce1193381d2f60aa89",
    "name": "Concern Worldwide",
    "shortname": "Concern Worldwide"
  },
  {
    "id": "openorgs____::b847a720edacaa06a6da87fb08c9ae9f",
    "name": "National Microelectronics Applications Centre (Ireland)",
    "shortname": "National Microelectronics Applications Centre (Ireland)"
  },
  {
    "id": "pending_org_::dcc73dafe4097197c8ee51fa011118c0",
    "name": "ENERGY ACTION LIMITED",
    "shortname": "ENERGY ACTION LBG"
  },
  {
    "id": "pending_org_::3a7fc73776fc1b2336d8a8d1ef7b2cbc",
    "name": "C.C.I.C.C. LIMITED",
    "shortname": "CARR"
  },
  {
    "id": "pending_org_::4dec384c3bb991a6295bec57555467b3",
    "name": "R & R MECHANICAL LIMITED",
    "shortname": "R & R"
  },
  {
    "id": "pending_org_::0d006919080e0342be4cf887e2efe5e6",
    "name": "BORD GAIS ENERGY LIMITED",
    "shortname": "BORD GAIS ENERGY LIMITED"
  },
  {
    "id": "pending_org_::b7e839ae892c5b844ecb43feb4fb5076",
    "name": "BYRNE ROISIN",
    "shortname": "BYRNE ROISIN"
  },
  {
    "id": "pending_org_::44ab826964f35dfcdc4d0b90a8d518b1",
    "name": "BSI CYBERSECURITY AND INFORMATION RESILIENCE (IRELAND) LIMITED",
    "shortname": "BSI Cybersecurity and Information Resilience"
  },
  {
    "id": "openorgs____::8233ba1251a5a47babe3162c3c803ab5",
    "name": "Cork Constraint Computation Centre University College Cork",
    "shortname": "Cork Constraint Computation Centre University College Cork"
  },
  {
    "id": "openorgs____::bb943a098b1548bb34f98a7dc97c1e51",
    "name": "Hibernia College",
    "shortname": "HIB"
  },
  {
    "id": "openorgs____::04ea2c9698c421025fbd392268c3a11d",
    "name": "Galway Education Centre",
    "shortname": "GEC"
  },
  {
    "id": "pending_org_::4241436be20e3c74a297904eba79bfe2",
    "name": "TAKEDA IRELAND LIMITED",
    "shortname": "TAKEDA"
  },
  {
    "id": "openorgs____::6cd837b553db5edfe5dbee8414f562ff",
    "name": "Irish Horseracing Regulatory Board (Ireland)",
    "shortname": "IHRB"
  },
  {
    "id": "pending_org_::a8c3e39cf812bf879bb0a04c99f8005e",
    "name": "CLW ENVIRONMENTAL PLANNERS LTD",
    "shortname": "CLW"
  },
  {
    "id": "openorgs____::c92e455a151ab11216e7e58aabdd8c32",
    "name": "Daithi O’Murchu Marine Research Station",
    "shortname": "DOMMRS"
  },
  {
    "id": "pending_org_::a5d8a6904a92924ce4171f5bf5c426d1",
    "name": "VYSERA BIOMEDICAL LIMITED",
    "shortname": "VYSERA BIOMEDICAL LIMITED"
  },
  {
    "id": "pending_org_::83b5f6c43af401d707a542c2b0143f09",
    "name": "Nokia (Ireland)",
    "shortname": "Nokia (Ireland)"
  },
  {
    "id": "openorgs____::b3f70da744d1500ed50a432cdb4d94e3",
    "name": "Kinesis Health Technologies (Ireland)",
    "shortname": "Kinesis Health Technologies (Ireland)"
  },
  {
    "id": "openorgs____::e08c73571f96ff98251f06cdc4012efc",
    "name": "EirGrid (Ireland)",
    "shortname": "EirGrid (Ireland)"
  },
  {
    "id": "pending_org_::a7cda4b405e2ea20229c9dd71d1dab1c",
    "name": "BRIDGECASTLE INFORMATION MANAGEMENTLTD",
    "shortname": "Castlebridge"
  },
  {
    "id": "pending_org_::e3dfaffd922e9c520e8d3b8e976c2ed4",
    "name": "St Patrick's Mental Health Services",
    "shortname": "St Patrick's Mental Health Services"
  },
  {
    "id": "pending_org_::ced13f748f461f303b900b3549173857",
    "name": "SKYTEK LIMITED",
    "shortname": "SKYTEK"
  },
  {
    "id": "pending_org_::8334ea9b673d9313d6ecda0105973066",
    "name": "UBOTICA TECHNOLOGIES LIMITED",
    "shortname": "UBOTICA TECHNOLOGIES LIMITED"
  },
  {
    "id": "openorgs____::33ff71c1ae64c0853afcf483ad546baa",
    "name": "Xperi (Ireland)",
    "shortname": "Xperi (Ireland)"
  },
  {
    "id": "pending_org_::8d35d439e4fdc240484baf1ff95603bc",
    "name": "Spectrum Telecom Installations Ltd",
    "shortname": "Spectrum"
  },
  {
    "id": "pending_org_::a3faad3007098c6433fdd7300492ecf7",
    "name": "NORTH WESTERN WATERS REGIONAL ADVISORY COUNCIL LIMITED BY GUARANTEE",
    "shortname": "NWWRAC"
  },
  {
    "id": "pending_org_::7870c1a93d632098c05ffdaffcf2b15c",
    "name": "Glen Dimplex Group",
    "shortname": "Glen Dimplex Group"
  },
  {
    "id": "openorgs____::530ffd666ef53305829ef330e92003b3",
    "name": "Equilume (Ireland)",
    "shortname": "Equilume (Ireland)"
  },
  {
    "id": "pending_org_::137f135e2163ab067205b916fc3dde42",
    "name": "DERILINX LIMITED",
    "shortname": "DERILINX LIMITED"
  },
  {
    "id": "pending_org_::78881747758dd98046acefc350addce1",
    "name": "GOOGLE IRELAND LIMITED",
    "shortname": "GOOGLE"
  },
  {
    "id": "pending_org_::a0ef3a279024068d293cec0618745543",
    "name": "NOEL LAWLER GREEN ENERGY SOLUTIONS LIMITED",
    "shortname": "Lawler Sustainability"
  },
  {
    "id": "pending_org_::ade3ce7e4926c7d597c82b4da1fd9d7b",
    "name": "CENTRE FOR IRISH AND EUROPEAN SECURITY LIMITED",
    "shortname": "CIES"
  },
  {
    "id": "openorgs____::78172de20f56c17549e76d7354306db5",
    "name": "Progress (Ireland)",
    "shortname": "Progress (Ireland)"
  },
  {
    "id": "pending_org_::2477e63ea4082d530e14d5d25fb753a8",
    "name": "EUROPEAN HEALTH MANAGEMENT ASSOCIATION LIMITED",
    "shortname": "EHMA"
  },
  {
    "id": "pending_org_::90b449f4a4ce0481ae4dc975e8bbe2c5",
    "name": "MPSTOR Limited",
    "shortname": "MPSTOR Limited"
  },
  {
    "id": "openorgs____::b658593f032098cc538757e95bc41f38",
    "name": "Naval Group (Ireland)",
    "shortname": "Naval Group (Ireland)"
  },
  {
    "id": "pending_org_::4669b272477a3d62908bcfcb9e8b8c29",
    "name": "CORTECHSCONNECT LIMITED",
    "shortname": "Cortechs"
  },
  {
    "id": "pending_org_::0a2758dd7c5827da487e3c08766bc994",
    "name": "SOLEARTH LIMITED",
    "shortname": "SOLEARTH ARCHITECTURE"
  },
  {
    "id": "openorgs____::1c2775807880278c3abd9c7e4a763c66",
    "name": "Monaghan Biosciences (Ireland)",
    "shortname": "Monaghan Biosciences (Ireland)"
  },
  {
    "id": "openorgs____::7555dd09a926ec703b13e0441578a6e4",
    "name": "Glencree",
    "shortname": "Glencree"
  },
  {
    "id": "openorgs____::7af134a1dc64a98fc0ce68843aa290e4",
    "name": "Irish Archaeology Field School",
    "shortname": "IAFS"
  },
  {
    "id": "pending_org_::dfce5c539d3920c87a7f6b90f04e9957",
    "name": "EM PATH WAYS COMPANY LIMITED BY GUARANTEE",
    "shortname": "EM|PATH"
  },
  {
    "id": "pending_org_::db35647145324adb259b8e549d3f3e59",
    "name": "CANCER TRIALS IRELAND COMPANY LIMITED BY GUARANTEE",
    "shortname": "CANCER TRIALS IRELAND"
  },
  {
    "id": "pending_org_::4f694aec56fb64c38a6010236bbbeadb",
    "name": "ST PATRICK'S COLLEGE DRUMCONDRA",
    "shortname": "St Patrick's College"
  },
  {
    "id": "pending_org_::af2cdadc3b29da3bfec9679136faea3c",
    "name": "TSUNAMI TRAINING LIMITED",
    "shortname": "Tsunami"
  },
  {
    "id": "pending_org_::0a345be78fe2d21f4bbfba84859c89ce",
    "name": "REAMDA LIMITED",
    "shortname": "Reamda Ltd."
  },
  {
    "id": "openorgs____::7082accf6d60734e637b3a23cf665348",
    "name": "Horse Sport Ireland",
    "shortname": "HSI"
  },
  {
    "id": "openorgs____::e7098efb64c559210328e5024ab0b06e",
    "name": "Endo (Ireland)",
    "shortname": "Endo (Ireland)"
  },
  {
    "id": "openorgs____::7d9e494b3e0a0f410d3485ddd23780bc",
    "name": "IMS Maxims (Ireland)",
    "shortname": "IMS Maxims (Ireland)"
  },
  {
    "id": "openorgs____::9b3fd02daf7da98893cd93dfbc557b71",
    "name": "Health Service Executive",
    "shortname": "Health Service Executive"
  },
  {
    "id": "pending_org_::d53901fb408d13d150a29f7cce841865",
    "name": "Royal Society of Chemistry",
    "shortname": "Royal Society of Chemistry"
  },
  {
    "id": "openorgs____::66090355caca999b16e7f885f3598a75",
    "name": "Dairygold Co-Operative Society (Ireland)",
    "shortname": "Dairygold Co-Operative Society (Ireland)"
  },
  {
    "id": "pending_org_::3d2850d3d18dfedb1504f182b0f2b3d1",
    "name": "SLAINTE BEOGA TEORANTA",
    "shortname": "Slainte Beoga Teoranta"
  },
  {
    "id": "pending_org_::faf60146b7a3f95d30d4dcbcc4c3efb4",
    "name": "ACCUPLEX DIAGNOSTICS LTD",
    "shortname": "Accuplex Diagnostics"
  },
  {
    "id": "pending_org_::a89f47641dc84cd07855034354e6aede",
    "name": "ROCKFIELD MEDICAL DEVICES LTD",
    "shortname": "ROCKFIELD MEDICAL DEVICES LTD"
  },
  {
    "id": "pending_org_::e920675d69a6113a2c05e0fdb286ad4e",
    "name": "Irish Research Council for Science, Engineering and Technology",
    "shortname": "Irish Research Council for Science, Engineering and Technology"
  },
  {
    "id": "pending_org_::f177024592b142c2a688a7f62df59f57",
    "name": "SCORPION NETWORKS LTD",
    "shortname": "SBN"
  },
  {
    "id": "ukri________::0f8f70eaa19092ec520d72faf435cc7a",
    "name": "Daithi O'Murchu Marine Research Station",
    "shortname": ""
  },
  {
    "id": "openorgs____::fb0986d8d59f2333780b074608ac575d",
    "name": "Sigmoid Pharma (Ireland)",
    "shortname": "Sigmoid Pharma (Ireland)"
  },
  {
    "id": "pending_org_::57a9c81b71e118789c864df4ab0c4baa",
    "name": "ALR Innovations Limited",
    "shortname": "Votechnik"
  },
  {
    "id": "pending_org_::3604c8492d8f7265dfe9af0f02ca874e",
    "name": "BANTRY MARINE RESEARCH STATION LIMITED",
    "shortname": "BMRS"
  },
  {
    "id": "openorgs____::8135a167dff719821ce3e502f69e0cd7",
    "name": "Aquila Bioscience (Ireland)",
    "shortname": "Aquila Bioscience (Ireland)"
  },
  {
    "id": "openorgs____::2fb59ae1676dfd378ea9175b3f76a2e9",
    "name": "Opsona Therapeutics (Ireland)",
    "shortname": "Opsona Therapeutics (Ireland)"
  },
  {
    "id": "openorgs____::bd91f4ef6bb783cfe169c5127c07f4d0",
    "name": "Lundin Mining (Ireland)",
    "shortname": "Lundin Mining (Ireland)"
  },
  {
    "id": "openorgs____::6bf7d8f6488113b95cebed110c5e62f2",
    "name": "Royal Irish Academy",
    "shortname": "RIA"
  },
  {
    "id": "pending_org_::63a370577369ede680c05cb6f6f33f0c",
    "name": "STRYKER EUROPEAN OPERATIONS LIMITED",
    "shortname": "SEOL"
  },
  {
    "id": "pending_org_::412392e17c7c3ce7a44efb5b9f4c1629",
    "name": "BOTANICAL, ENVIRONMENTAL & CONSERVATION CONSULTANTS LIMITED",
    "shortname": "BEC"
  },
  {
    "id": "openorgs____::5cd186b27d5a4085877c5a2d1e434748",
    "name": "University College Cork, School of Biochemistry and Cell Biology",
    "shortname": "University College Cork, School of Biochemistry and Cell Biology"
  },
  {
    "id": "pending_org_::face964607b81bb8d86e40328a4bcd04",
    "name": "HELIXWORKS TECHNOLOGIES LIMITED",
    "shortname": "HELIXWORKS TECHNOLOGIES LIMITED"
  },
  {
    "id": "pending_org_::72c4e125a1fd54b77140d8b0c96141e7",
    "name": "SUSTAINABLE INNOVATION TECHNOLOGY SERVICES LIMITED",
    "shortname": "SUSTAINABLE INNOVATION TECHNOLOGY SERVICES LIMITED"
  },
  {
    "id": "pending_org_::d2b406003be0bdf1ae0b9f3f508767f6",
    "name": "SAS Institute (Ireland)",
    "shortname": "SAS Institute (Ireland)"
  },
  {
    "id": "openorgs____::02e887fce7ec90b1aabab8ecbd54f1bc",
    "name": "Men’s Development Network",
    "shortname": "MDN"
  },
  {
    "id": "openorgs____::0577b2b918f3cd80746a08381e9ccf0c",
    "name": "Skytek (Ireland)",
    "shortname": "Skytek (Ireland)"
  },
  {
    "id": "pending_org_::2e92e57a376e319d5927f2d1de47793d",
    "name": "EIRGRID PLC",
    "shortname": "EIRGRID PLC"
  },
  {
    "id": "pending_org_::fa63a8e9a191cf184fb2381db0ed6e91",
    "name": "EmpowerTheUser Ltd",
    "shortname": "ETU"
  },
  {
    "id": "openorgs____::02061377244729ad8bf20a78e776d711",
    "name": "Irish Rugby Football Union",
    "shortname": "IRFU"
  },
  {
    "id": "openorgs____::955e601013973254feb42c640f9dc57c",
    "name": "Saolta University Health Care Group",
    "shortname": "Saolta University Health Care Group"
  },
  {
    "id": "pending_org_::6ab56e858c1c62d73e5a3443314aa115",
    "name": "TEMPERATURE LIMITED",
    "shortname": "SIRUS AIRCON"
  },
  {
    "id": "openorgs____::bc0d42a04ceed31d082fdbdb2b494ce1",
    "name": "International Energy Research Centre",
    "shortname": "IERC"
  },
  {
    "id": "pending_org_::696f1d177bd81e4f8f5260f178bdec78",
    "name": "IRISH COUNTRY MEATS (SHEEPMEAT) UNLIMITED COMPANY",
    "shortname": "IRISH COUNTRY MEATS (SHEEPMEAT) UNLIMITED COMPANY"
  },
  {
    "id": "pending_org_::21128cb8cf5f288d3e8f44081a4f1771",
    "name": "THE EUROPEAN COALITION FOR PEOPLE LIVING WITH OBESITY COMPANY LIMITED BY GUARANTEE",
    "shortname": "THE EUROPEAN COALITION FOR PEOPLE LIVING WITH OBESITY COMPANY LIMITED BY GUARANTEE"
  },
  {
    "id": "openorgs____::fb6ad9ba47846fa5ba1b19dcfe42f6a6",
    "name": "National University of Ireland",
    "shortname": "NUI"
  },
  {
    "id": "openorgs____::fff00de980d7863e86d8259f675fe2d2",
    "name": "Optrace (Ireland)",
    "shortname": "Optrace (Ireland)"
  },
  {
    "id": "openorgs____::04e80d113d10ef10c048043b109f9ea0",
    "name": "Hydro International (Ireland)",
    "shortname": "Hydro International (Ireland)"
  },
  {
    "id": "openorgs____::b104e508ede896df2fa521c95b692ced",
    "name": "Radiological Protection Institute of Ireland",
    "shortname": "RPII"
  },
  {
    "id": "pending_org_::bb773c9e2a475fda040f1d8353fc69f2",
    "name": "ATTRIDGE ELEANOR",
    "shortname": "THE FEDERATION OF IRISH BEEKEEPERSASSOCIATIONS"
  },
  {
    "id": "pending_org_::13fdd10db4cd3fcb9154dbdaf095b059",
    "name": "SPARK WORKS LIMITED",
    "shortname": "SPARK WORKS LIMITED"
  },
  {
    "id": "pending_org_::74100d47834126055832ed46ebd47e99",
    "name": "SILVERCLOUD HEALTH LIMITED",
    "shortname": "SILVERCLOUD HEALTH LIMITED"
  },
  {
    "id": "pending_org_::6a2f9d654839ef240d0ba6ce12c9154b",
    "name": "CITY ANALYSTS LTD",
    "shortname": "CITY ANALYSTS LTD"
  },
  {
    "id": "pending_org_::3393f3e52828774ca03ac2bd512eddf9",
    "name": "DOLMEN DESIGN AND INNOVATION LIMITED",
    "shortname": "Dolmen Design and Innovation"
  },
  {
    "id": "openorgs____::e3fc472b3c75b57c3d55203313c2da19",
    "name": "Cork’s Technology Network",
    "shortname": "CEIA"
  },
  {
    "id": "openorgs____::84885b8b6d48f4d1aea97393b65d0b5b",
    "name": "Cork University Hospital",
    "shortname": "CUH"
  },
  {
    "id": "pending_org_::82108c31eeb2f43199dba91446bcc68c",
    "name": "EMEDIA INTERACTIVE LIMITED",
    "shortname": "EMEDIA INTERACTIVE LIMITED"
  },
  {
    "id": "pending_org_::fa3b09bcaa43b5c766e52d143e5175ea",
    "name": "OPEN APPLICATIONS CONSULTING LIMITED",
    "shortname": "OpenApp"
  },
  {
    "id": "pending_org_::894e307f34dd972ab8884ead1a8303c4",
    "name": "EBLANA PHOTONICS LIMITED",
    "shortname": "EBLANA"
  },
  {
    "id": "openorgs____::db864322ea1d68411d977a4711822327",
    "name": "Met Éireann",
    "shortname": "Met Éireann"
  },
  {
    "id": "openorgs____::6e7800ba8b43c50c69a48f0025963416",
    "name": "Adient (Ireland)",
    "shortname": "Adient (Ireland)"
  },
  {
    "id": "openorgs____::8f48090441038ae1ca4f35380568582c",
    "name": "Irish Rail",
    "shortname": "Irish Rail"
  },
  {
    "id": "pending_org_::41423b776b3d2c16b7ecffcc134710b5",
    "name": "Kerry Group plc",
    "shortname": "Kerry Group plc"
  },
  {
    "id": "pending_org_::ca5ef1ed40e80d0c7ad9aee903fd94e3",
    "name": "JUNO COMPOSITES LTD",
    "shortname": "JUNO COMPOSITES LTD"
  },
  {
    "id": "openorgs____::ae08c39aa5043d9df34284ed50a9caba",
    "name": "Irish Pharmacy Union",
    "shortname": "IPU"
  },
  {
    "id": "pending_org_::dc697065c435eeca4245bbe0718320f7",
    "name": "KINESENSE LIMITED",
    "shortname": "KINESENSE LIMITED"
  },
  {
    "id": "openorgs____::631709042728ef6bc8a0b47d27bd1675",
    "name": "Henkel (Ireland)",
    "shortname": "Henkel (Ireland)"
  },
  {
    "id": "pending_org_::de6367738b5eed7cbda07fb5cbf8034c",
    "name": "IRISH WATER",
    "shortname": "UISCE EIREANN"
  },
  {
    "id": "pending_org_::d3c4e44d6aceb720eb79df08de5b7a80",
    "name": "CLEAN COMMUNICATIONS LIMITED",
    "shortname": "CLEAN COMMUNICATIONS LIMITED"
  },
  {
    "id": "openorgs____::bb2efbfb6ee458079439dac31fbe7b00",
    "name": "National Cancer Registry",
    "shortname": "National Cancer Registry"
  },
  {
    "id": "openorgs____::e4c2b7f3ae9af23b9c507c0e1c65efeb",
    "name": "National Maternity Hospital",
    "shortname": "National Maternity Hospital"
  },
  {
    "id": "openorgs____::81317c69d1aad1574f1a39f145ffd335",
    "name": "ATLANTIC TECHNOLOGICAL UNIVERSITY",
    "shortname": "GMIT"
  },
  {
    "id": "openorgs____::eba967e0223f4f7d43f3452b8994415e",
    "name": "Kilcreene Orthopaedic Hospital",
    "shortname": "Kilcreene Orthopaedic Hospital"
  },
  {
    "id": "openorgs____::a3c84ff3a017da870d3071071a50f30a",
    "name": "BioMarin (Ireland)",
    "shortname": "BioMarin (Ireland)"
  },
  {
    "id": "pending_org_::b39b21d83d11a554ecc939f1fa5358e7",
    "name": "HEALTH PRODUCTS REGULATORY AUTHORITY",
    "shortname": "HPRA"
  },
  {
    "id": "openorgs____::0006264d445728873e3f0e40a5cff911",
    "name": "Southern Regional Assembly",
    "shortname": "MWRA"
  },
  {
    "id": "re3data_____::5fa45aed58fc38dc5254cdf70f5781d0",
    "name": "National Forum for the Enhancement of Teaching and Learning in Higher Education",
    "shortname": "NDLR"
  },
  {
    "id": "pending_org_::fc9f548d912c30b0090ab9c2ed2fd1da",
    "name": "INLECOM COMMERCIAL PATHWAYS COMPANYLIMITED BY GUARANTEE",
    "shortname": "INLECOM COMMERCIAL PATHWAYS COMPANYLIMITED BY GUARANTEE"
  },
  {
    "id": "pending_org_::5c15b47ef9a8185d6c39c9be32932280",
    "name": "EARAGAIL EISC TEORANTA",
    "shortname": "EFL"
  },
  {
    "id": "pending_org_::66b1eae1cf53e1669191432742b5ee78",
    "name": "EVERCAM LIMITED",
    "shortname": "EVERCAM IO"
  },
  {
    "id": "pending_org_::c0240f66321db6f466d0bbe7a058cd37",
    "name": "NEURAVI LIMITED",
    "shortname": "NEURAVI LIMITED"
  },
  {
    "id": "pending_org_::acc8d9a35ce5e8b3c9ee5ea69dd49d8a",
    "name": "Irish Institute of Training and Development",
    "shortname": "Irish Institute of Training and Development"
  },
  {
    "id": "pending_org_::24d2a91dc277b12a3b4740cddde03a3e",
    "name": "OCEAN ENERGY LIMITED",
    "shortname": "OCEAN ENERGY LIMITED"
  },
  {
    "id": "pending_org_::263b08e0d23396f00eeb022f255bfea9",
    "name": "KEYWATER FISHERIES LIMITED",
    "shortname": "KEYWATER FISHERIES LIMITED"
  },
  {
    "id": "openorgs____::4e8b316796b2311d1aed7055bf27ba0b",
    "name": "Croí",
    "shortname": "Croí"
  },
  {
    "id": "pending_org_::4a08b811570bd756d9b1dcca9415d91a",
    "name": "Alimentary Glycoscience Research Cluster",
    "shortname": "Alimentary Glycoscience Research Cluster"
  },
  {
    "id": "openorgs____::e185531dba8d4dd0d01ab16c5b38618c",
    "name": "Intel (Ireland)",
    "shortname": "Intel (Ireland)"
  },
  {
    "id": "openorgs____::4515735111509499dc08e1d3db7b136d",
    "name": "Dell (Ireland)",
    "shortname": "Dell (Ireland)"
  },
  {
    "id": "openorgs____::5d03ff8bd07f6d0c59707da730f3a6a9",
    "name": "Rape Crisis Network Ireland",
    "shortname": "RCNI"
  },
  {
    "id": "pending_org_::d369243908f59e500c483d6d7f7ac678",
    "name": "SIRIUSXT LIMITED",
    "shortname": "SIRIUSXT LIMITED"
  },
  {
    "id": "openorgs____::346dab7f59fb541e1c2845a3bedce69a",
    "name": "Galway Clinic",
    "shortname": "Galway Clinic"
  },
  {
    "id": "openorgs____::e2f994aa5760bcaff01794707ba64128",
    "name": "BH Consulting (Ireland)",
    "shortname": "BH Consulting (Ireland)"
  },
  {
    "id": "pending_org_::712e77f0f5d3aadd045b8dd8f832e834",
    "name": "BEAL ORGANIC CHEESE LIMITED",
    "shortname": "BEAL ORGANIC CHEESE LIMITED"
  },
  {
    "id": "openorgs____::89f7dc76f5e1f61d26789cc36afc4882",
    "name": "Tunstall (Ireland)",
    "shortname": "Tunstall (Ireland)"
  },
  {
    "id": "openorgs____::6943e7d5df558a6698d6b87b24089a3b",
    "name": "Irish Refugee Council",
    "shortname": "IRC"
  },
  {
    "id": "pending_org_::55d680980684864d84356e4c12b15c9c",
    "name": "BIOMETRIC TECHNOLOGY SOLUTIONS LIMITED",
    "shortname": "BTS"
  },
  {
    "id": "openorgs____::fa8ffedeecbfb67982af0d7ac12975dc",
    "name": "BioAtlantis (Ireland)",
    "shortname": "BioAtlantis (Ireland)"
  },
  {
    "id": "openorgs____::ad6d95176ed06a76895b033e0ef8327b",
    "name": "Boardmatch",
    "shortname": "Boardmatch"
  },
  {
    "id": "pending_org_::a9fdaaa9e239908d883946dab16c9445",
    "name": "BLAFAR LIMITED",
    "shortname": "Blafar Limited"
  },
  {
    "id": "pending_org_::39e66c89ad1725eeb24d59508d60a543",
    "name": "South Infirmary-Victoria Hospital",
    "shortname": "SIVUH"
  },
  {
    "id": "openorgs____::8cbb49d38457d538abb8ac8b60811b82",
    "name": "Irish Hydrodata (Ireland)",
    "shortname": "Irish Hydrodata (Ireland)"
  },
  {
    "id": "openorgs____::5b4c24600eb8c1aede827afbf1284469",
    "name": "Irish Jewish Museum",
    "shortname": "Irish Jewish Museum"
  },
  {
    "id": "pending_org_::7381127ef83524fa98af50deffc30fce",
    "name": "TIRLÁN LIMITED",
    "shortname": "TIRLÁN"
  },
  {
    "id": "pending_org_::e922f23d5a970d14e7116215efd96771",
    "name": "SPIRITAN ASYLUM SERVICES INITIATIVE LIMITED",
    "shortname": "SPIRASI"
  },
  {
    "id": "pending_org_::8985b616ed505208b87c6ec1c961c798",
    "name": "EKF Diagnostics Limited",
    "shortname": "EKF"
  },
  {
    "id": "pending_org_::cb269a5caaca7d6d72ecbbd379c3f32a",
    "name": "BOSTON SCIENTIFIC LIMITED",
    "shortname": "BSL"
  },
  {
    "id": "openorgs____::8329254f1bdce485b0245d65dab5a86b",
    "name": "Irish Council for Civil Liberties",
    "shortname": "ICCL"
  },
  {
    "id": "openorgs____::56878d0cb48f83d38d5a60b2a873ea51",
    "name": "EmployAbility Galway",
    "shortname": "EmployAbility Galway"
  },
  {
    "id": "openorgs____::3d6043e598b0466b2a63d16dc9551fbf",
    "name": "Irish Centre for High-End Computing",
    "shortname": "ICHEC"
  },
  {
    "id": "openorgs____::6924f486263fe0f98ba29ce905df96c9",
    "name": "Medtronic (Ireland)",
    "shortname": "Medtronic (Ireland)"
  },
  {
    "id": "pending_org_::4df594af1b83e410ba9e0b85c49c7d50",
    "name": "CTL TASTAIL TEORANTA LIMITED",
    "shortname": "CTL"
  },
  {
    "id": "openorgs____::e369706bc09c1278f7ed18d23bc250ea",
    "name": "Aqualex Multimedia Consortium (Ireland)",
    "shortname": "AMC"
  },
  {
    "id": "pending_org_::3b38f32284b64697fbe7badc9209c1cd",
    "name": "Vornia Limited",
    "shortname": "Vornia Limited"
  },
  {
    "id": "openorgs____::29c51d3d66daffb5bad1e801a66da214",
    "name": "Engineers Ireland",
    "shortname": "IEI"
  },
  {
    "id": "openorgs____::250d8efa7a7764c46a1005c65e619f46",
    "name": "Limerick County Council",
    "shortname": "Limerick County Council"
  },
  {
    "id": "openorgs____::45e58265fff964dcc032988b0172c984",
    "name": "University Maternity Hospital Limerick",
    "shortname": "UMHL"
  },
  {
    "id": "pending_org_::b1fd404d17070bc855fe98a377785392",
    "name": "AIRTEL ATN LTD.",
    "shortname": "AIRTEL"
  },
  {
    "id": "pending_org_::4faa2b9cd06462ca0612e22f47423745",
    "name": "Irish College of Paramedics",
    "shortname": "Irish College of Paramedics"
  },
  {
    "id": "pending_org_::e2eeddd9631b3d713a290ddac8b12c24",
    "name": "ENERGY CO-OPERATIVES IRELAND LIMITED",
    "shortname": "Energy Co-operatives Ireland"
  },
  {
    "id": "pending_org_::8637a617a8a6fc3cd76367391c8b0e93",
    "name": "TALAM BIOTECH LIMITED",
    "shortname": "TALAM BIOTECH LIMITED"
  },
  {
    "id": "openorgs____::07a535406fc1835a38693d3cae18f801",
    "name": "Mincon (Ireland)",
    "shortname": "Mincon (Ireland)"
  },
  {
    "id": "openorgs____::0caf0c587e72935e9a49b1b6a8b4c2e1",
    "name": "Opera Software (Ireland)",
    "shortname": "Opera Software (Ireland)"
  },
  {
    "id": "pending_org_::22c2509e5c4bf3167f916605733ea3ce",
    "name": "BEATS MEDICAL LIMITED",
    "shortname": "BEATS MEDICAL LIMITED"
  },
  {
    "id": "pending_org_::bf1f9fa0b6bbed24684625eb7c7fb263",
    "name": "ACET",
    "shortname": "ACET"
  },
  {
    "id": "openorgs____::73174078d03b09b5a7ad0626a2f4f057",
    "name": "Transpharmation (Ireland)",
    "shortname": "Transpharmation (Ireland)"
  },
  {
    "id": "openorgs____::fe08a42dd313bf78a67a554aab46128a",
    "name": "NSilico (Ireland)",
    "shortname": "NSilico (Ireland)"
  },
  {
    "id": "openorgs____::a913ff51e636d197e213ee897fc88c45",
    "name": "Oran Pre-Cast (Ireland)",
    "shortname": "Oran Pre-Cast (Ireland)"
  },
  {
    "id": "pending_org_::b4fbcd4add67b486946cee6ad8bdc7a3",
    "name": "OILEAN GLAS TEORANTA",
    "shortname": "OGT"
  },
  {
    "id": "openorgs____::5b16bd55cf3398ad80702ce3670ea0d7",
    "name": "Infant",
    "shortname": "Infant"
  },
  {
    "id": "pending_org_::e284a1047ca9f63de902a0cfe38a4d6c",
    "name": "AUBREN LIMITED",
    "shortname": "AUBREN"
  },
  {
    "id": "openorgs____::f6c39995c29fea505be4c03da7c02c96",
    "name": "Java Clinical",
    "shortname": "Java Clinical"
  },
  {
    "id": "pending_org_::788ed7de87afe26f4d42121d83e09f71",
    "name": "ABB LIMITED",
    "shortname": "ABB"
  },
  {
    "id": "pending_org_::0734c52bd9254c8f0aa4c396f8d79faf",
    "name": "IRISH MANUFACTURING RESEARCH COMPANY LIMITED BY GUARANTEE",
    "shortname": "IRISH MANUFACTURING RESEARCH"
  },
  {
    "id": "openorgs____::723445920262adde9d5e000c22141d0f",
    "name": "Kingspan (Ireland)",
    "shortname": "Kingspan (Ireland)"
  },
  {
    "id": "openorgs____::8865cdd27b999d71baf647f939359f97",
    "name": "Schlumberger (Ireland)",
    "shortname": "Schlumberger (Ireland)"
  },
  {
    "id": "pending_org_::22ea82714b74e1a8514964b5a2125569",
    "name": "IRISH WHALE AND DOLPHIN GROUP COMPANY LIMITED BY GUARANTEE",
    "shortname": "Irish Whale And Dolphin Group"
  },
  {
    "id": "openorgs____::93762679d5d7cfb1ae0215e86f0e864f",
    "name": "National College of Art & Design, Dublin",
    "shortname": "NCAD"
  },
  {
    "id": "openorgs____::3f6e28a8147501b4f0de5a65847d55e6",
    "name": "Childminding Ireland",
    "shortname": "Childminding Ireland"
  },
  {
    "id": "pending_org_::911cc31449ccc4a221b8ec19b6590ccb",
    "name": "BUSINESS POINT INTELLIGENCE SOLUTIONS LIMITED",
    "shortname": "BUSINESS POINT INTELLIGENCE SOLUTIONS LIMITED"
  },
  {
    "id": "ukri________::50f88a4a84651493467f7f91fbae22d9",
    "name": "Xilinx Ireland",
    "shortname": ""
  },
  {
    "id": "openorgs____::f1b1c565059e97fce3f9bf143ee810ee",
    "name": "St. Luke's Hospital",
    "shortname": "St. Luke's Hospital"
  },
  {
    "id": "openorgs____::4e24272b3e3e028d3740265c5b8743b5",
    "name": "FOREST ENVIRONMENTAL RESEARCH AND SERVICES LIMITED",
    "shortname": "FOREST ENVIRONMENTAL RESEARCH AND SERVICES LIMITED"
  },
  {
    "id": "pending_org_::9fdd299ea1a518963ea68e86834f72b2",
    "name": "KILLYBEGS FISHERMEN'S ORGANISATION LIMITED COOPERATIVE SOCIETY",
    "shortname": "KFO"
  },
  {
    "id": "pending_org_::c4e730f1fd9e93dda76f67bc0801d660",
    "name": "EASTERN AND MIDLAND REGIONAL ASSEMBLY",
    "shortname": "EASTERN AND MIDLAND REGIONAL ASSEMBLY"
  },
  {
    "id": "openorgs____::61ec081acd9fd0aa50f4c0ec69473bd7",
    "name": "CIRCA Group Europe (Ireland)",
    "shortname": "CIRCA Group Europe (Ireland)"
  },
  {
    "id": "openorgs____::e3ee4fa9011d244c3522ffd34383f8df",
    "name": "National Children’s Research Centre",
    "shortname": "National Children’s Research Centre"
  },
  {
    "id": "openorgs____::73b5315ac6605fbf0c0d124816bcd802",
    "name": "TobaccoFree Research Institute",
    "shortname": "TFRI"
  },
  {
    "id": "pending_org_::c7485d576a50c003901dab2ea4083295",
    "name": "IRISH CATTLE BREEDING FEDERATION SOCIETY LTD",
    "shortname": "ICBF"
  },
  {
    "id": "pending_org_::981c0bfd552ade6ec92ab660be8328f9",
    "name": "Bell Labs Ireland",
    "shortname": "Bell Labs Ireland"
  },
  {
    "id": "pending_org_::5928e2b1288c849bbdee6e414c9fce73",
    "name": "TOLERANT NETWORKS LIMITED",
    "shortname": "TOLERANT NETWORKS"
  },
  {
    "id": "openorgs____::b3e5bb2ebba3f41da368cd6d32672ae0",
    "name": "Balor Arts Centre",
    "shortname": "Balor Arts Centre"
  },
  {
    "id": "pending_org_::a13741c56829ac59217ec3be1576d62a",
    "name": "PROPHOTONIX IRL LIMITED",
    "shortname": "PROPHOTONIX IRL LIMITED"
  },
  {
    "id": "openorgs____::41d6d94e495533c7efa533834744a065",
    "name": "Nanoscale Function Group Conway Institute of Biomolecular and Biomedi University College Dublin",
    "shortname": "Nanoscale Function Group Conway Institute of Biomolecular and Biomedi University College Dublin"
  },
  {
    "id": "openorgs____::420cb79ec500d741a1a6946610f279e5",
    "name": "Irish Cancer Society",
    "shortname": "Irish Cancer Society"
  },
  {
    "id": "pending_org_::7aedcd64f4aedea0a906075b96763ab7",
    "name": "CONNECTING ARCHAEOLOGY AND ARCHITECTURE IN EUROPE",
    "shortname": "CARARE"
  },
  {
    "id": "pending_org_::24b80ed6dd612db3525b9aaec2d573b8",
    "name": "Ireland Canada University Foundation",
    "shortname": "Ireland Canada University Foundation"
  },
  {
    "id": "pending_org_::8f0d9b64080084cbd382e8d27fb53b2f",
    "name": "SIGMOID PHARMA LIMITED",
    "shortname": "SIGMOID"
  },
  {
    "id": "openorgs____::aa413bc305ad3396e955166263532eb8",
    "name": "Juniper Networks (Ireland)",
    "shortname": "Juniper Networks (Ireland)"
  },
  {
    "id": "pending_org_::70a5637299d43e40e0b2a55243f324f5",
    "name": "COMHARCHUMANN FUINNIMH OILEAIN ARANN TEORANTA",
    "shortname": "COMHARCHUMANN FUINNIMH OILEAIN ARANN TEORANTA"
  },
  {
    "id": "pending_org_::11d89e2597110e31f1df563844ef2d51",
    "name": "Kildare and Wicklow Education and Training Board",
    "shortname": "Kildare and Wicklow Education and Training Board"
  },
  {
    "id": "pending_org_::fcc8b2d89083b35082699d76387a0003",
    "name": "HUNTINGTON'S DISEASE ASSOCIATION OF IRELAND",
    "shortname": "HUNTINGTON'S DISEASE ASSOCIATION OF IRELAND"
  },
  {
    "id": "pending_org_::33063691b88e3265486bc3d1fa2ad070",
    "name": "FICONTEC IRELAND LIMITED",
    "shortname": "FICONTEC IRELAND LIMITED"
  },
  {
    "id": "pending_org_::8ea900dbb3c03f2446c9e6f30458bea8",
    "name": "LUXCEL BIOSCIENCES LTD",
    "shortname": "LUXCEL"
  },
  {
    "id": "openorgs____::7e075c8a3ec5b72753220b304dc3ad32",
    "name": "Mayo General Hospital",
    "shortname": "Mayo General Hospital"
  },
  {
    "id": "openorgs____::c6c19ac58fd2f82123d783bf36822288",
    "name": "RedZinc (Ireland)",
    "shortname": "RedZinc (Ireland)"
  },
  {
    "id": "pending_org_::59e633a8ff89357ffdac6dc958556aca",
    "name": "GOCAR CARSHARING LIMITED",
    "shortname": "GoCar"
  },
  {
    "id": "pending_org_::cbcac3ca6dfeecdf0c9c24295538ac09",
    "name": "WRIPL TECHNOLOGIES LIMITED",
    "shortname": "WRIPL TECHNOLOGIES LIMITED"
  },
  {
    "id": "pending_org_::7429121a33d1260ba22dd728ffbb0a98",
    "name": "MOL TEIC",
    "shortname": "Dingle Hub"
  },
  {
    "id": "pending_org_::428d65c4bb30a3fce5be2dae5e7ec951",
    "name": "ASHLAND SPECIALTIES IRELAND LIMITED",
    "shortname": "ASHLAND SPECIALTIES IRELAND LIMITED"
  },
  {
    "id": "openorgs____::fe0edd75c73c5a34b19da7cf665484e6",
    "name": "UCD School of Mathematics and Statistics University College Dublin",
    "shortname": "UCD School of Mathematics and Statistics University College Dublin"
  },
  {
    "id": "openorgs____::6332604e6d6357105ccd3123fe9d121e",
    "name": "Mount Carmel Hospital",
    "shortname": "Mount Carmel Hospital"
  },
  {
    "id": "openorgs____::dfe9198bda660cb4ad309bc149ed9cd1",
    "name": "OncoMark (Ireland)",
    "shortname": "OncoMark (Ireland)"
  },
  {
    "id": "openorgs____::a3dba15f8d802cfad327760d3175522c",
    "name": "Boliden Tara Mines (Ireland)",
    "shortname": "Boliden Tara Mines (Ireland)"
  },
  {
    "id": "pending_org_::6cc1bdda6fd328ca6db1061df1ce7c0d",
    "name": "CON TRAAS Ltd.",
    "shortname": "CON TRAAS Ltd."
  },
  {
    "id": "pending_org_::558f51977668d3420691fe6026d39d35",
    "name": "SPECTRUM INSTRUMENTS LTD",
    "shortname": "SPECTRUM INSTRUMENTS LTD"
  },
  {
    "id": "pending_org_::2161c9b503c279c7283279a85f41aed9",
    "name": "PROJECT ARTS CENTRE",
    "shortname": "PROJECT ARTS CENTRE"
  },
  {
    "id": "pending_org_::9fad6646670cc9f41f4f13812e81497b",
    "name": "IRISH GRASSLAND ASSOCIATION",
    "shortname": "IGA"
  },
  {
    "id": "pending_org_::a0eb16a54c13ce75e329b5cbd84b22f0",
    "name": "Coastal Zone Services Limited",
    "shortname": "CZS"
  },
  {
    "id": "pending_org_::5d5ac6f2e82827ff17f8631e9b6a546b",
    "name": "Teagasc",
    "shortname": "Teagasc"
  },
  {
    "id": "pending_org_::d85579e95ca0323cedb5b50b41c65d92",
    "name": "TRILATERAL RESEARCH LIMITED",
    "shortname": "TRI IE"
  },
  {
    "id": "pending_org_::6b76c95b7f3a7db1c249a61784d169a3",
    "name": "BEOTANICS LIMITED",
    "shortname": "BEOTANICS LIMITED"
  },
  {
    "id": "pending_org_::b1faae96f306e02d8159c9265272079f",
    "name": "SYMANTEC LIMITED",
    "shortname": "SYMANTEC"
  },
  {
    "id": "openorgs____::39a290b00dbe7b408aca7d86099d1151",
    "name": "Science Gallery International",
    "shortname": "Science Gallery International"
  },
  {
    "id": "pending_org_::1f0173139f0275833e53361893cf5429",
    "name": "Health Research Charities Ireland",
    "shortname": "Health Research Charities Ireland"
  },
  {
    "id": "openorgs____::5b611adf992af13a3baf8d2bce1fb2d5",
    "name": "Memjet (Ireland)",
    "shortname": "Memjet (Ireland)"
  },
  {
    "id": "openorgs____::8227cf08cca3c9c62cc0722a786097bc",
    "name": "Red Hat (Ireland)",
    "shortname": "Red Hat (Ireland)"
  },
  {
    "id": "openorgs____::383fd8413514bcf14c292a33e907c5b9",
    "name": "Acquired Brain Injury Ireland",
    "shortname": "Acquired Brain Injury Ireland"
  },
  {
    "id": "openorgs____::f290b9623b84a872bf54656a6b29588a",
    "name": "Rotunda Hospital",
    "shortname": "Rotunda Hospital"
  },
  {
    "id": "pending_org_::a1cecbae92a769fa528cb22941370170",
    "name": "AQUILA BIOSCIENCE LIMITED",
    "shortname": "ABL"
  },
  {
    "id": "pending_org_::f23764431eab7ee624652031faf1c507",
    "name": "PALLIARE LIMITED",
    "shortname": "Palliare"
  },
  {
    "id": "pending_org_::19b7dfd3afa0251d62684a97e5f1532b",
    "name": "IRISH OPERA TRANSFORMATIONS COMPANY LIMITED BY GUARANTEE",
    "shortname": "IRISH OPERA TRANSFORMATIONS COMPANY LIMITED BY GUARANTEE"
  },
  {
    "id": "pending_org_::63819c027beedc8a3fcbacc345ac7798",
    "name": "HOOKE BIO LTD",
    "shortname": "HOOKE BIO LTD"
  },
  {
    "id": "openorgs____::202c8b7dfd72ac63fdf7b1fc99035e5e",
    "name": "University Hospital Galway",
    "shortname": "GUH"
  },
  {
    "id": "pending_org_::7fe51e8015b89a29d85b449db11c10f4",
    "name": "RecycleNet Ireland Limited",
    "shortname": "RecycleNet Ireland Limited"
  },
  {
    "id": "openorgs____::8e8bd044cb564ed3a1720c1c151beac5",
    "name": "Entellexi (Ireland)",
    "shortname": "Entellexi (Ireland)"
  },
  {
    "id": "openorgs____::22bf7d965bb77d85458590da42d0d1fa",
    "name": "Milltown Institute of Theology and Philosophy",
    "shortname": "Milltown Institute of Theology and Philosophy"
  },
  {
    "id": "openorgs____::22daff79751dba45b03193f86e707bac",
    "name": "Fáilte Ireland",
    "shortname": "Fáilte Ireland"
  },
  {
    "id": "openorgs____::5352e8197bcc25b2b66bcc0788ae8b92",
    "name": "Technological University of the Shannon: Midlands Midwest",
    "shortname": "LIT"
  },
  {
    "id": "openorgs____::afbb3ef3c8dac802fdf40c56ca2f7121",
    "name": "Dún Laoghaire Institute of Art, Design and Technology",
    "shortname": "IADT"
  },
  {
    "id": "pending_org_::3982e9f2e1f136cf4d359cc9a4b3f8a1",
    "name": "MMI CLINICAL RESEARCH DEVELOPMENT IRELAND",
    "shortname": "MMI CLINICAL RESEARCH DEVELOPMENT IRELAND"
  },
  {
    "id": "openorgs____::323e3e42531f011dcf2cffd3c9b49c42",
    "name": "Smartbay Ireland",
    "shortname": "Smartbay Ireland"
  },
  {
    "id": "openorgs____::30c42e228972841f038009d011489fa7",
    "name": "Irish Equine Centre",
    "shortname": "IEC"
  },
  {
    "id": "openorgs____::38e95e4f932b021355a0291da3b036b1",
    "name": "IBM Research - Ireland",
    "shortname": "IBM Research - Ireland"
  },
  {
    "id": "openorgs____::63c9f64df3b297f0bde25f79eebac090",
    "name": "CCT College Dublin",
    "shortname": "CCT"
  },
  {
    "id": "pending_org_::557567697c9fb56f80637cf0121617bd",
    "name": "Global Brain Health Institute",
    "shortname": "Global Brain Health Institute"
  },
  {
    "id": "pending_org_::e2ac6aaa44f462b35ac624d40a9df6e5",
    "name": "DAWN MEATS GROUP",
    "shortname": "Dawn Meats Group"
  },
  {
    "id": "pending_org_::f79b1e6e240b7332e2bf01aae16b2967",
    "name": "TFC RESEARCH AND INNOVATION LIMITED",
    "shortname": "TFC RESEARCH AND INNOVATION LIMITED"
  },
  {
    "id": "openorgs____::9e85d9916ffed6bab1b39f00cfa90b32",
    "name": "Kerry Education and Training Board",
    "shortname": "Kerry Education and Training Board"
  },
  {
    "id": "openorgs____::a00cfe63f7d13fc203b986e6de099643",
    "name": "Temple Street Children's University Hospital",
    "shortname": "Temple Street Children's University Hospital"
  },
  {
    "id": "pending_org_::5d4d1bf04802040e3f16c5a7df0ee902",
    "name": "PERACTON LIMITED",
    "shortname": "PERACTON"
  },
  {
    "id": "pending_org_::9c49e6d50b0589b4fa788534217f8899",
    "name": "SPORTS SURGERY CLINIC LIMITED",
    "shortname": "SPORTS SURGERY CLINIC LIMITED"
  },
  {
    "id": "openorgs____::18dd4a4c55382e118fa4098633e8b42e",
    "name": "OGT Amenity (Ireland)",
    "shortname": "OGT Amenity (Ireland)"
  },
  {
    "id": "pending_org_::ec667a7e130c2981c8e9ec85aafe3148",
    "name": "CITY OF DUBLIN EDUCATION AND TRAINING BOARD",
    "shortname": "CITY OF DUBLIN EDUCATION AND TRAINING BOARD"
  },
  {
    "id": "pending_org_::75a0cd62776eb24fdf4873c97055b05b",
    "name": "OPSONA THERAPEUTICS LIMITED",
    "shortname": "OPN"
  },
  {
    "id": "openorgs____::d7d68b18bec64d0ac29b2e9cb3df7ffc",
    "name": "Irish Penal Reform Trust",
    "shortname": "IPRT"
  },
  {
    "id": "openorgs____::813635bf75d996681fb8ca72f3a91957",
    "name": "Gaelic Athletic Association",
    "shortname": "CLG"
  },
  {
    "id": "openorgs____::ce15482bc7d4de69c5850a54647e29fd",
    "name": "Danaher (Ireland)",
    "shortname": "Danaher (Ireland)"
  },
  {
    "id": "pending_org_::51eec52552fd943f9105c14be4473c91",
    "name": "ATRIAN MEDICAL LIMITED",
    "shortname": "ATRIAN MEDICAL LIMITED"
  },
  {
    "id": "pending_org_::f1c2183841a5aa041847fbb3859cf921",
    "name": "UNIVERSAL LEARNING SYSTEMS LTD",
    "shortname": "UNIVERSAL LEARNING SYSTEMS LTD"
  },
  {
    "id": "pending_org_::bbc2635719d3d814b2a9dd63a24ae8b0",
    "name": "HEALTH INFORMATION AND QUALITY AUTHORITY",
    "shortname": "HEALTH INFORMATION AND QUALITY AUTHORITY"
  },
  {
    "id": "openorgs____::21c471a7f67c06110395b8b381ce74bc",
    "name": "Tokyo Electron (Ireland)",
    "shortname": "TEL"
  },
  {
    "id": "openorgs____::b4fd14f9fbc04e9cfc43a37895dee06e",
    "name": "Molecular Medicine Ireland",
    "shortname": "MMI"
  },
  {
    "id": "pending_org_::c00dc6513c6d73b4710883defc13f9a4",
    "name": "ADVANCE SCIENCE (FORBAIRT EOLAIOCHTA TEO) LIMITED",
    "shortname": "Advance Science Ltd"
  },
  {
    "id": "pending_org_::90651cba716d5d6ec448ca23b595182c",
    "name": "EUROPEAN INSTITUTE OF WOMEN'S HEALTH LIMITED",
    "shortname": "EIWH"
  },
  {
    "id": "pending_org_::f8e3e4c8f49cd59073b649868734e5d8",
    "name": "INFINEON TECHNOLOGIES IRELAND LIMITED",
    "shortname": "INFINEON TECHNOLOGIES IRELAND LIMITED"
  },
  {
    "id": "pending_org_::1551a9683f4259ca84d48cdffc6dc86f",
    "name": "St. Luke's Institute of Cancer Research",
    "shortname": "St. Luke's Institute of Cancer Research"
  },
  {
    "id": "pending_org_::359034b5405e1d3dcbbf9e54b1f33d72",
    "name": "National Biodiversity Data Centre",
    "shortname": "National Biodiversity Data Centre"
  },
  {
    "id": "openorgs____::4aab8efc71a537550198886711cf9256",
    "name": "Combilift (Ireland)",
    "shortname": "Combilift (Ireland)"
  },
  {
    "id": "openorgs____::5d55fb216b14691cf68218daf5d78cd9",
    "name": "Munster Technological University",
    "shortname": "ITT"
  },
  {
    "id": "openorgs____::1fdb7f67fcacdb369700f79ff3b342d1",
    "name": "Takeda (Ireland)",
    "shortname": "Takeda (Ireland)"
  },
  {
    "id": "openorgs____::e0f0a9e96a048a035f8d64a6da677bb3",
    "name": "South Infirmary Victoria University Hospital",
    "shortname": "SIVUH"
  },
  {
    "id": "openorgs____::0ca30e3c1eec500b24eabec7b7b45601",
    "name": "National Forum for the Enhancement of Teaching & Learning in Higher Education",
    "shortname": "T&L"
  },
  {
    "id": "openorgs____::0e227d2a5798c224a919d6f96e294046",
    "name": "School of Chemistry and Chemical Biology University College Dublin",
    "shortname": "School of Chemistry and Chemical Biology University College Dublin"
  },
  {
    "id": "pending_org_::e626cf285fff466835eee452854daefc",
    "name": "POLY-PICO TECHNOLOGIES LTD",
    "shortname": "POLY-PICO TECHNOLOGIES LTD"
  },
  {
    "id": "openorgs____::e6ce3fb90857e4a66b1b88559dbec94a",
    "name": "Ericsson (Ireland)",
    "shortname": "Ericsson (Ireland)"
  },
  {
    "id": "pending_org_::75d42f1d085f95a7faa81eb52532d413",
    "name": "SINDICE LIMITED",
    "shortname": "SIREN"
  },
  {
    "id": "openorgs____::8ca414a89d8a00fabf40a4682b013391",
    "name": "Mitel (Ireland)",
    "shortname": "Mitel (Ireland)"
  },
  {
    "id": "openorgs____::2514a61b454cfd3dfe7fa1e2be641b93",
    "name": "Health Informatics Society of Ireland",
    "shortname": "HISI"
  },
  {
    "id": "pending_org_::a446ddac61b0107f71bc52885429ac7e",
    "name": "Global Action Plan Ltd.",
    "shortname": "Global Action Plan Ltd."
  },
  {
    "id": "pending_org_::4a09d4f77c1f977a2209b6b51ad10e3b",
    "name": "THE SEAWEED COMPANY GREEN TURTLE LIMITED",
    "shortname": "THE SEAWEED COMPANY GREEN TURTLE LIMITED"
  },
  {
    "id": "pending_org_::6ba660f40580f88e846c072943969054",
    "name": "VISTATEC LTD",
    "shortname": "VISTATEC LTD"
  },
  {
    "id": "pending_org_::6ebbfd5226eeff6e5b17aaa7b137bb82",
    "name": "Nautical Enterprise Centre Ltd.",
    "shortname": "NECL"
  },
  {
    "id": "pending_org_::4bade368e9fcc2b43e8a420e162714ac",
    "name": "ANIMAL HEALTH IRELAND INITIATIVE",
    "shortname": "ANIMAL HEALTH IRELAND INITIATIVE"
  },
  {
    "id": "pending_org_::ecba73e3b52a34b602f6fcaa94155c06",
    "name": "VRM TECHNOLOGY DESIGNATED ACTIVITY COMPANY",
    "shortname": "VRM TECHNOLOGY LIMITED"
  },
  {
    "id": "pending_org_::899ec56fdcd0d3c93f835e3d4a5b62b7",
    "name": "INTERNATIONAL CONSORTIUM OF RESEARCH STAFF ASSOCIATIONS COMPANY LIMITED BY GUARANTEE",
    "shortname": "ICoRSA"
  },
  {
    "id": "openorgs____::8ee6fcf4ab6876c85821ba32514407ae",
    "name": "Cancer Clinical Research Trust",
    "shortname": "CCRT"
  },
  {
    "id": "openorgs____::3c301657a32c27433f8d2e39d4a0827d",
    "name": "The American College",
    "shortname": "The American College"
  },
  {
    "id": "openorgs____::8c3b51d1ddc08201b9f13e9397f60754",
    "name": "Cosmo Pharmaceuticals (Ireland)",
    "shortname": "Cosmo Pharmaceuticals (Ireland)"
  },
  {
    "id": "pending_org_::2d53eed364aa64246eff199c01041f4f",
    "name": "OVAGEN GROUP LIMITED",
    "shortname": "OVAGEN GROUP LIMITED"
  },
  {
    "id": "pending_org_::f38013dd514948e4745e8a0320dc409c",
    "name": "GLEN DIMPLEX HEATING & VENTILATION IRELAND UNLIMITED COMPANY",
    "shortname": "GLEN DIMPLEX HEATING & VENTILATION IRELAND UNLIMITED COMPANY"
  },
  {
    "id": "openorgs____::1ea78e5286af1fe0ceb678e475927dba",
    "name": "European Recycling Platform (Ireland)",
    "shortname": "ERP"
  },
  {
    "id": "pending_org_::e0c14eeed274ec39358bc95b66cf2d6b",
    "name": "EPRI EUROPE DAC",
    "shortname": "EPRI EUROPE DAC"
  },
  {
    "id": "pending_org_::0afcdf67dba8c8aa74ec0576744f9672",
    "name": "KONNECTA SYSTEMS LIMITED",
    "shortname": "KONNECTA SYSTEMS LIMITED"
  },
  {
    "id": "openorgs____::a160d4be84a70d0ba30318ec93e61cbc",
    "name": "Bord na Móna (Ireland)",
    "shortname": "Bord na Móna (Ireland)"
  },
  {
    "id": "pending_org_::64d23fffcf1f8ab8ebfbd3df5f83c203",
    "name": "CARA PHARMACY LIMITED",
    "shortname": "Cara Pharmacy"
  },
  {
    "id": "openorgs____::8d649d4c432f0e9f7fb1fadffa33279b",
    "name": "Neuromod (Ireland)",
    "shortname": "Neuromod (Ireland)"
  },
  {
    "id": "openorgs____::72f064aca0cb4962a2d771367d6b1d04",
    "name": "Garda Síochána College",
    "shortname": "Garda Síochána College"
  },
  {
    "id": "pending_org_::ac4fa10dedb07b8422aa5c1718b1f375",
    "name": "Elan",
    "shortname": "Elan"
  },
  {
    "id": "pending_org_::f761608feadb8ca57064b5a7f45738ed",
    "name": "SAHkartell",
    "shortname": "SAHkartell"
  },
  {
    "id": "pending_org_::8bb00e26b9d87ea186ea85da4c70c185",
    "name": "Bristol-Myers Squibb (Ireland)",
    "shortname": "BMS"
  },
  {
    "id": "pending_org_::ae35907eb73196067563e2b5a6d3d459",
    "name": "NUI Galway, Whitaker Instiute",
    "shortname": "NUI Galway, Whitaker Instiute"
  },
  {
    "id": "pending_org_::ed462fc3741a2fb2f5d670b6455b5008",
    "name": "Saint John of God Hospital",
    "shortname": "Saint John of God Hospital"
  },
  {
    "id": "pending_org_::1bbd9d685c0d51419b6e587380c1c370",
    "name": "ENERGY MONITORING IRELAND LIMITED",
    "shortname": "ENERGY MONITORING IRELAND LIMITED"
  },
  {
    "id": "openorgs____::1905816efcc98a9d5d25761aab19670d",
    "name": "Family Carers Ireland",
    "shortname": "Family Carers Ireland"
  },
  {
    "id": "pending_org_::87a4a75f2cf98cf4c008aee9bc3c756f",
    "name": "Galway County Council",
    "shortname": "Galway County Council"
  },
  {
    "id": "pending_org_::09f719b1813932e1e23fd6e59dced972",
    "name": "ICON CLINICAL RESEARCH LIMITED",
    "shortname": "ICR"
  },
  {
    "id": "openorgs____::b4a8a1a7d8b484b1bdf0f0c6161014b2",
    "name": "Sport Ireland",
    "shortname": "Sport Ireland"
  },
  {
    "id": "pending_org_::b001edf083bea1470c5eb8ec826c3352",
    "name": "Mater Dei Institute of Education",
    "shortname": "MD"
  },
  {
    "id": "pending_org_::fab77dc93db5aba1b78797db055e6403",
    "name": "Federation of Irish Beekeepers' Associations (FIBKA) CLG",
    "shortname": "Federation of Irish Beekeepers' Associations (FIBKA) CLG"
  },
  {
    "id": "openorgs____::57eb9cf7c352244c2eacb72254afe346",
    "name": "Physical Education and Sport Sciences Department, University of Limerick",
    "shortname": "Physical Education and Sport Sciences Department, University of Limerick"
  },
  {
    "id": "pending_org_::16d58656a85657635a939e9a8dd406d8",
    "name": "CROOM PRECISION TOOLING LIMITED",
    "shortname": "CROOM PRECISION TOOLING LIMITED"
  },
  {
    "id": "openorgs____::5a0ad151c6ae23f678e289941acb5123",
    "name": "Johnson Controls (Ireland)",
    "shortname": "Johnson Controls (Ireland)"
  },
  {
    "id": "openorgs____::975e7333fa77f14d3bec9cb84318a144",
    "name": "KCI (Ireland)",
    "shortname": "KCI"
  },
  {
    "id": "pending_org_::5ab35053e29c60b372f1b1db3dd75a5c",
    "name": "WEXFORD COUNTY COUNCIL",
    "shortname": "WEXFORD COUNTY COUNCIL"
  },
  {
    "id": "openorgs____::c605116b812ad6faf6e343c557f6b6c3",
    "name": "Stryker (Ireland)",
    "shortname": "Stryker (Ireland)"
  },
  {
    "id": "openorgs____::efbea0aba96db849768a78fa18eed07d",
    "name": "InnovaWood",
    "shortname": "IW"
  },
  {
    "id": "openorgs____::66de23098bd4292407c4ce450d6f376d",
    "name": "Department of Public Expenditure and Reform",
    "shortname": "Department of Public Expenditure and Reform"
  },
  {
    "id": "openorgs____::832d276b537df7f5badfd42845d30242",
    "name": "Department of Mathematics National University of Ireland Dublin",
    "shortname": "Department of Mathematics National University of Ireland Dublin"
  },
  {
    "id": "openorgs____::b3c29cd7b91ccce2ee761e2736b5336d",
    "name": "Sir Patrick Duns Research Laboratory Central Pathology Laboratory St. James's Hospital",
    "shortname": "Sir Patrick Duns Research Laboratory Central Pathology Laboratory St. James's Hospital"
  },
  {
    "id": "openorgs____::8d464af262ce80843940aab2f84930d8",
    "name": "Monaghan County Council",
    "shortname": "Monaghan County Council"
  },
  {
    "id": "pending_org_::7a14ac140078901a269615b6604e9764",
    "name": "PINPOINT INNOVATIONS LTD",
    "shortname": "PINPOINT INNOVATIONS LTD"
  },
  {
    "id": "openorgs____::9adad9527a7ee4c113cee1017d75a504",
    "name": "Irish Society for the Prevention of Cruelty to Animals",
    "shortname": "ISPCA"
  },
  {
    "id": "pending_org_::b3146295564e26b13c60d434f0ab4d4e",
    "name": "ABU INTERNATIONAL PROJECT MANAGEMENT LIMITED",
    "shortname": "ABU INTERNATIONAL PROJECT MANAGEMENT LIMITED"
  },
  {
    "id": "pending_org_::b9d8aa2f891fe29650f03bbf27b0c426",
    "name": "4D PHARMA CORK LIMITED",
    "shortname": "4D PHARMA CORK LIMITED"
  },
  {
    "id": "pending_org_::2ca18c29f8b08c814005b77ae6e5541b",
    "name": "THE NATIONAL MICROELECTRONICS APPLICATIONS CENTRE LTD",
    "shortname": "MAC"
  },
  {
    "id": "openorgs____::977d5a61ccf9075d7011a260beb1d7c7",
    "name": "Griffith College Cork",
    "shortname": "GCC"
  },
  {
    "id": "pending_org_::f0d6a56a60328fe24fb4d6aca0c60bdb",
    "name": "MONAGHAN MUSHROOMS IRELAND",
    "shortname": "MONAGHAN MUSHROOMS IRELAND"
  },
  {
    "id": "pending_org_::4114f90e28b31f49413a6e5db6cfdb3d",
    "name": "BNY Mellon Fund Services (Ireland) Limited",
    "shortname": "BNY Mellon Fund Services (Ireland) Limited"
  },
  {
    "id": "pending_org_::e3ecb5d124aa7ba42db64d37a48e40ef",
    "name": "ACCENTURE GLOBAL SOLUTIONS LIMITED",
    "shortname": "ACCENTURE GLOBAL SOLUTIONS"
  },
  {
    "id": "openorgs____::ab8ef6e5f0729b13a0d0d48f99d93c80",
    "name": "Higher Education Authority",
    "shortname": "HEA"
  },
  {
    "id": "openorgs____::dafb688ddaf1c6c3d5e09ef61c0f0389",
    "name": "Dublin City University",
    "shortname": "DCU"
  },
  {
    "id": "pending_org_::80089ae61d34d1f15b25f70604d32351",
    "name": "Saint John of God Hospital clg.",
    "shortname": "Saint John of God Hospital clg."
  },
  {
    "id": "ror_________::469337436876d725cbdbb2c2fac14688",
    "name": "Bard (Ireland)",
    "shortname": "Bard (Ireland)"
  },
  {
    "id": "openorgs____::c55c315136e3f603d3c8f366438623e7",
    "name": "Bantry General Hospital",
    "shortname": "Bantry General Hospital"
  },
  {
    "id": "pending_org_::50342f4722d245bb3fe8dcaf5d857f50",
    "name": "OVE ARUP & PARTNERS IRELAND LIMITED",
    "shortname": "OVE ARUP & PARTNERS IRELAND LIMITED"
  },
  {
    "id": "pending_org_::23f611dbb6b478db8c65c916e1f4d752",
    "name": "TECHRETE IRELAND LIMITED",
    "shortname": "TECHRETE IRELAND LIMITED"
  },
  {
    "id": "openorgs____::b9f5c21cc213fe41d9ed23cebf447fca",
    "name": "Valeo (Ireland)",
    "shortname": "Valeo (Ireland)"
  },
  {
    "id": "openorgs____::d2b7ae77d2645779a27d6cc5422e4a3a",
    "name": "Vornia (Ireland)",
    "shortname": "Vornia (Ireland)"
  },
  {
    "id": "pending_org_::9a73fde0929920942d2f05228ceaa156",
    "name": "ENDOWAVE LIMITED",
    "shortname": "ENDOWAVE LIMITED"
  },
  {
    "id": "openorgs____::732dfe18fcd8b4fc3297f8ff5c2c6609",
    "name": "Farran Technology (Ireland)",
    "shortname": "Farran Technology (Ireland)"
  },
  {
    "id": "openorgs____::abf44eac50c244375fe1797ad442b754",
    "name": "Department of Health",
    "shortname": "Department of Health"
  },
  {
    "id": "pending_org_::18ba7304b72fe1ca5671e5eacc1016f6",
    "name": "COILLTE TEORANTA",
    "shortname": "COILLTE TEORANTA"
  },
  {
    "id": "openorgs____::cfff8953137dd245911fa0b435106f79",
    "name": "Cork County Council",
    "shortname": "Cork County Council"
  },
  {
    "id": "openorgs____::86810c8e3136a1caf298d2f7ef2c9000",
    "name": "Bernal Institute, University of Limerick",
    "shortname": "Bernal Institute, University of Limerick"
  },
  {
    "id": "pending_org_::6a8ce80a098849201709d35bcf449e2d",
    "name": "APK ARCHITECTS & ENGINEERS LIMITED",
    "shortname": "APK ARCHITECTS & ENGINEERS LIMITED"
  },
  {
    "id": "openorgs____::6542c919d2dc57c9909d540d0b2c1025",
    "name": "Airmid Healthgroup",
    "shortname": "Airmid Healthgroup"
  },
  {
    "id": "pending_org_::56f6eb66ae9cec60456d6d6faf793b37",
    "name": "EUROPEAN PROFESSIONAL DRIVERS ASSOCIATION MANAGEMENT SERVICES COMPANYLIMITED BY GUARANTEE",
    "shortname": "EPDA"
  },
  {
    "id": "pending_org_::1b526b11dcd2da8b268a697b599c2dbd",
    "name": "INVIZBOX LTD",
    "shortname": "INVIZBOX LTD"
  },
  {
    "id": "openorgs____::d58f0ae003739f7ab0afb2773ff39e95",
    "name": "Supply Network Shannon (Ireland)",
    "shortname": "Supply Network Shannon (Ireland)"
  },
  {
    "id": "pending_org_::3e79d81faf268bf7a14a5b713c3daa06",
    "name": "NOVUS DIAGNOSTICS LIMITED",
    "shortname": "NOVUS DIAGNOSTICS LIMITED"
  },
  {
    "id": "openorgs____::0d8715960a66925568aa974e8943404b",
    "name": "Janssen (Ireland)",
    "shortname": "Janssen (Ireland)"
  },
  {
    "id": "pending_org_::aa9f7cc39dc5d13c3d6f69a3e11eb87b",
    "name": "CUAN BEO ENVIRONMENTAL COMPANY LBG",
    "shortname": "CUAN BEO ENVIRONMENTAL COMPANY LBG"
  },
  {
    "id": "pending_org_::3f9b179c3b3f8308a62fb0d1c839d8be",
    "name": "RODGER HAMISH",
    "shortname": "DAVID MACLEOD"
  },
  {
    "id": "pending_org_::e18b7b610229df3fe4bff2ebda53288c",
    "name": "Kelloggs",
    "shortname": "Kelloggs"
  },
  {
    "id": "openorgs____::5b781202cf6bafd74eb95023e2e67c55",
    "name": "IBAT College",
    "shortname": "IBAT College"
  },
  {
    "id": "pending_org_::48eeefe876beb98d3d052904099476a8",
    "name": "REFLECTIVE MEASUREMENT SYSTEMS LIMITED",
    "shortname": "RMS LIMITED"
  },
  {
    "id": "openorgs____::bfae7296f1492b76045efbe1c0463343",
    "name": "European Institute of Women's Health",
    "shortname": "EIWH"
  },
  {
    "id": "pending_org_::75d47432ba526bacd714a73da1bf3dcd",
    "name": "INSTITUTE FOR METHODS INNOVATION",
    "shortname": "INSTITUTE FOR METHODS INNOVATION"
  },
  {
    "id": "openorgs____::9f2956c5aeea1156eab9997b8af7045c",
    "name": "Forbairt",
    "shortname": "FNT"
  },
  {
    "id": "openorgs____::3a2f44987d7fff35a22a2b4db33c9496",
    "name": "School of Politics & International Relations SPIRe University College Dublin",
    "shortname": "School of Politics & International Relations SPIRe University College Dublin"
  },
  {
    "id": "pending_org_::6a27f8caf500a908b555eb843d13ffcb",
    "name": "Clare County Council",
    "shortname": "Clare County Council"
  },
  {
    "id": "openorgs____::1923369bf66b347a2576bf264ed341fe",
    "name": "SensL (Ireland)",
    "shortname": "SensL (Ireland)"
  },
  {
    "id": "openorgs____::12ebab89f39320ad3fb00d75e652490f",
    "name": "OxyMem (Ireland)",
    "shortname": "OxyMem (Ireland)"
  },
  {
    "id": "openorgs____::460fe46eedb0cace88e8478df3b60be4",
    "name": "Nokia (Ireland)",
    "shortname": "Nokia (Ireland)"
  },
  {
    "id": "openorgs____::3a1df774be54caa7d551610767d423f7",
    "name": "Irish Human Rights and Equality Commission",
    "shortname": "IHREC"
  },
  {
    "id": "openorgs____::e81fc36644daa11b96b056ecf8733584",
    "name": "Ervia (Ireland)",
    "shortname": "Ervia (Ireland)"
  },
  {
    "id": "openorgs____::787e22bd56bc6102818f53cdcae7d16d",
    "name": "Pfizer (Ireland)",
    "shortname": "Pfizer (Ireland)"
  },
  {
    "id": "pending_org_::1cc46733e44dc3c9817688f918bafb5f",
    "name": "NUMA ENGINEERING SERVICES LIMITED",
    "shortname": "NUMA"
  },
  {
    "id": "pending_org_::464dfdd9b2de40cac7cf51a5efdf2a80",
    "name": "W1DAEXPERIENCE LIMITED",
    "shortname": "W1DA"
  },
  {
    "id": "pending_org_::3f2e97cfb22c37dc79d94a256e22cd66",
    "name": "IRISH GREEN BUILDING COUNCIL LIMITED BY GUARANTEE",
    "shortname": "IRISH GREEN BUILDING COUNCIL LIMITED BY GUARANTEE"
  },
  {
    "id": "pending_org_::8f0d2fb11cda21805976f74d1aa9b9c7",
    "name": "REACHOUT IRELAND LTD",
    "shortname": "ReachOut Ireland"
  },
  {
    "id": "openorgs____::0cd02f1f3793e1dc1ea9b346d68ec6d7",
    "name": "Innopharma Education",
    "shortname": "Innopharma Education"
  },
  {
    "id": "pending_org_::cba9f48a7f0510c1db6d74373e0a1096",
    "name": "Enerit",
    "shortname": "Enerit"
  },
  {
    "id": "pending_org_::47cad612a1f34167d821aea2a85b67b5",
    "name": "ENERGY REFORM LIMITED",
    "shortname": "ENERGY REFORM"
  },
  {
    "id": "openorgs____::502a172b685a60a434950272735322fb",
    "name": "Irish Blood Transfusion Service",
    "shortname": "BTSB"
  },
  {
    "id": "openorgs____::83a639e0293e25103ae47d489dcec096",
    "name": "Aer Lingus (Ireland)",
    "shortname": "Aer Lingus (Ireland)"
  },
  {
    "id": "pending_org_::1fe82d8271ff5128f868bc705376bd20",
    "name": "Department of Botany University of Dublin",
    "shortname": "Department of Botany University of Dublin"
  },
  {
    "id": "pending_org_::99c2f5e51cd31ee34af777bd533d1305",
    "name": "TIPPERARY COUNTY COUNCIL",
    "shortname": "TCC"
  },
  {
    "id": "pending_org_::de3084d91efa8cc806e66aab29bbd9e0",
    "name": "CLEVERBOOKS LIMITED",
    "shortname": "CLEVERBOOKS LIMITED"
  },
  {
    "id": "pending_org_::359e79ab8290f6b4933307c9c060edac",
    "name": "Department of Justice",
    "shortname": "Department of Justice"
  },
  {
    "id": "pending_org_::5e9e324169f6a29cb748aa1fbc5fdb85",
    "name": "Deparment of Children and Youth Affairs, Ireland",
    "shortname": "Deparment of Children and Youth Affairs, Ireland"
  },
  {
    "id": "openorgs____::946bacd0de845ddd0222401d0a6a2a2c",
    "name": "Bon Secours Hospital Tralee",
    "shortname": "Bon Secours Hospital Tralee"
  },
  {
    "id": "openorgs____::fdb5e8eaed4e7af8a55003712af4787c",
    "name": "Xilinx (Ireland)",
    "shortname": "Xilinx (Ireland)"
  },
  {
    "id": "openorgs____::53dab4c498b7c2c0ec7a31a0bb3de510",
    "name": "Cook Medical (Ireland)",
    "shortname": "Cook Medical (Ireland)"
  },
  {
    "id": "openorgs____::54631eef4ebb3e563a5fdd7cf001c0d0",
    "name": "Walton Institute for Information and Communications Systems Science",
    "shortname": "Walton Institute for Information and Communications Systems Science"
  },
  {
    "id": "openorgs____::f4c0233996b99bffbb18b1129249b9c5",
    "name": "St. Patrick's Hospital",
    "shortname": "St. Patrick's Hospital"
  },
  {
    "id": "pending_org_::ee18729e4a36a6cd3c31d28f24468664",
    "name": "CONEY ISLAND SHELLFISH LIMITED",
    "shortname": "CONEY ISLAND SHELLFISH LIMITED"
  },
  {
    "id": "pending_org_::84773de6e967a5bd5d3fb3ef06cb7094",
    "name": "University College Cork",
    "shortname": "University College Cork"
  },
  {
    "id": "openorgs____::3bf69e3d4cac8482872fdc33702f47bc",
    "name": "Louth County Council",
    "shortname": "Louth County Council"
  },
  {
    "id": "pending_org_::7328e492c8c503b888f7568b385bdb83",
    "name": "BEACON HOSPITAL SANDYFORD LIMITED",
    "shortname": "BEACON HOSPITAL"
  },
  {
    "id": "pending_org_::00026e6e95b46b68fc477ef03656a520",
    "name": "DEPUY (IRELAND) UNLIMITED",
    "shortname": "DePuy Ireland"
  },
  {
    "id": "openorgs____::1d870f4161b7c853a02febab39899b4b",
    "name": "Central Mental Hospital",
    "shortname": "Central Mental Hospital"
  },
  {
    "id": "openorgs____::ded8cb3b7b10add40106265ffc0545fc",
    "name": "Future Analytics",
    "shortname": "Future Analytics"
  },
  {
    "id": "openorgs____::43d8995ef10029ee52baf96d91407a81",
    "name": "Blackrock Castle Observatory",
    "shortname": "Blackrock Castle Observatory"
  },
  {
    "id": "openorgs____::0b437bcd77d65acdb09ac2d16d8c1057",
    "name": "Sustainable Energy Authority of Ireland",
    "shortname": "SEAI"
  },
  {
    "id": "openorgs____::72e7929c3752f9b72a06778593e53943",
    "name": "FitzGerald Nurseries (Ireland)",
    "shortname": "FitzGerald Nurseries (Ireland)"
  },
  {
    "id": "openorgs____::19c250613b2ce696e820dd3e8e1fee73",
    "name": "Alimentary Health (Ireland)",
    "shortname": "Alimentary Health (Ireland)"
  },
  {
    "id": "pending_org_::33bd233cdcdb3559f0180ff436a2076b",
    "name": "SINDICE LIMITED",
    "shortname": "Sindice"
  },
  {
    "id": "openorgs____::136e6e3085c4edbdb9e3f1a7658a949f",
    "name": "Greencore (Ireland)",
    "shortname": "Greencore (Ireland)"
  },
  {
    "id": "pending_org_::b0789373e6535fbd130ea55825763881",
    "name": "APPLIED PROCESS COMPANY LIMITED",
    "shortname": "APC LTD"
  },
  {
    "id": "openorgs____::65d93fdf47e2c73743a1dd70fc3d4e74",
    "name": "Health Research Institute, University of Limerick",
    "shortname": "Health Research Institute, University of Limerick"
  },
  {
    "id": "pending_org_::a9ed51766a00cd90504030069a6e8842",
    "name": "ABM Construction",
    "shortname": "ABM Europe/Bridge Systems"
  },
  {
    "id": "pending_org_::e69097ec40256fa4ac342791d8d8304a",
    "name": "BYRNE LOOBY PARTNERS WATER SERVICES LIMITED",
    "shortname": "BYRNE LOOBY PHMCCARTHY"
  },
  {
    "id": "pending_org_::0d05134f2328a7368c0e269493148b6d",
    "name": "BIOCONNECT LTD",
    "shortname": "LIQUID TECHNOLOGIES"
  },
  {
    "id": "openorgs____::85073bceaf838678b3b42455a3548507",
    "name": "Perrigo (Ireland)",
    "shortname": "Perrigo (Ireland)"
  },
  {
    "id": "pending_org_::8da96fa0bab040e87e69024ae9ee755e",
    "name": "KLAS LIMITED",
    "shortname": "Klas Telecom"
  },
  {
    "id": "openorgs____::ad56bfcf4cfac2070e741fe9cba0c9d7",
    "name": "Dublin Institute For Advanced Studies",
    "shortname": "DIAS"
  },
  {
    "id": "openorgs____::b60c964b7a2a9aa3f7ae9a845b1c51be",
    "name": "European Society of Cataract and Refractive Surgeons",
    "shortname": "ESCRS"
  },
  {
    "id": "pending_org_::764606199d425fdac1ecb19071bfb67b",
    "name": "ANALOG DEVICES INTERNATIONAL UNLIMITED COMPANY",
    "shortname": "Analog Devices International"
  },
  {
    "id": "openorgs____::fff763477c92f70ffe1e7e3b8bb856f0",
    "name": "College of Medicine, Nursing and Health Sciences, National University of Ireland, Galway",
    "shortname": "College of Medicine, Nursing and Health Sciences, National University of Ireland, Galway"
  },
  {
    "id": "pending_org_::279acab416ac50d17e4a21939d5c061f",
    "name": "MYLAN IRE HEALTHCARE LIMITED",
    "shortname": "MYLAN IRE HEALTHCARE LIMITED"
  },
  {
    "id": "pending_org_::c456939f4e3be86c85844a6752966f0f",
    "name": "ARTOMATIX LIMITED",
    "shortname": "ARTOMATIX LIMITED"
  },
  {
    "id": "openorgs____::7360f1590c7fffd9a7e4e696f9ca1a1a",
    "name": "HEAnet",
    "shortname": "HEAnet"
  },
  {
    "id": "pending_org_::d5c481f2744be6f3eaac27a01b03fce6",
    "name": "DP DESIGNPRO LIMITED",
    "shortname": "DESIGNPRO LTD"
  },
  {
    "id": "pending_org_::175810d85c7d237ec313d0db64a8f3c4",
    "name": "SCIENCE TECHNOLOGY RESEARCH PARTNERS LIMITED",
    "shortname": "STREP"
  },
  {
    "id": "openorgs____::87694492b525119df0f740d9220e8930",
    "name": "THE PROVOST, FELLOWS, FOUNDATION SCHOLARS & THE OTHER MEMBERS OF BOARD OF THE COLLEGE OF THE HOLY & UNDIVIDED TRINITY OF QUEEN ELIZABETH NEAR DUBLIN",
    "shortname": "TRINITY COLLEGE DUBLIN"
  },
  {
    "id": "openorgs____::d95cbd3009b28c715986d746c0060169",
    "name": "St. Luke's General Hospital",
    "shortname": "St. Luke's General Hospital"
  },
  {
    "id": "openorgs____::a980d639f43685b3a47023e4a6bcece6",
    "name": "Kimmage Development Studies Centre",
    "shortname": "DSC"
  },
  {
    "id": "pending_org_::3c0852ffd87543f307fb923ea5f3f1a1",
    "name": "IRISH TRADITIONAL MUSIC ARCHIVE/TAISCE CHEOL DUCHAIS EIREANN LBG",
    "shortname": "ITMA"
  },
  {
    "id": "pending_org_::7dafa48a813a01e0ff5ab4b20821106c",
    "name": "SOFTWARE RADIO SYSTEMS LIMITED",
    "shortname": "SRS"
  },
  {
    "id": "pending_org_::5eede0d0986717ab8e149003fc42a180",
    "name": "TEILIFIS NA GAEILGE TG4",
    "shortname": "TEILIFIS NA GAEILGE TG4"
  },
  {
    "id": "pending_org_::c7cfcaae1d223034a7a185af51372956",
    "name": "JOHNSON & JOHNSON VISION CARE IRELAND UNLIMITED COMPANY",
    "shortname": "JJVCI"
  },
  {
    "id": "pending_org_::dd4e66d57df5e651fb6976641f5cea5a",
    "name": "Filmbase",
    "shortname": "Filmbase"
  },
  {
    "id": "openorgs____::12d040c633ed6f5e006bbe84075e4de4",
    "name": "Prothena (Ireland)",
    "shortname": "Prothena (Ireland)"
  },
  {
    "id": "pending_org_::fc5485f5df27da9f54c5c50ebf7d2a4a",
    "name": "EMC INFORMATION SYSTEMS INTERNATIONAL UNLIMITED COMPANY",
    "shortname": "EISI"
  },
  {
    "id": "openorgs____::2363e810c6b7e3a970d7747267807539",
    "name": "Oryx International Growth Fund (Ireland)",
    "shortname": "Oryx International Growth Fund (Ireland)"
  },
  {
    "id": "pending_org_::213b2b127a65613bb93bd9b5dbc3fc6e",
    "name": "THE ADELAIDE & MEATH HOSPITAL DUBLIN INCORPORATING THE NATIONAL CHILDREN'S HOSPITAL",
    "shortname": "THE ADELAIDE & MEATH HOSPITAL DUBLIN INCORPORATING THE NATIONAL CHILDREN'S HOSPITAL"
  },
  {
    "id": "openorgs____::d269757d47a2cab5bda3ae578fd9305e",
    "name": "Innocoll (Ireland)",
    "shortname": "Innocoll (Ireland)"
  },
  {
    "id": "pending_org_::5519f1bc35592ddf3d5263b1d8cef6ba",
    "name": "INTEL RESEARCH AND DEVELOPMENT IRELAND LIMITED",
    "shortname": "ISL"
  },
  {
    "id": "pending_org_::0dc0ce82d44eda2a4ddc910243b3d0f1",
    "name": "RYANAIR DESIGNATED ACTIVITY COMPANY",
    "shortname": "RYANAIR DESIGNATED ACTIVITY COMPANY"
  },
  {
    "id": "pending_org_::8746d280c23d4b4465af186100fee8cd",
    "name": "Humanities in the European Research Area",
    "shortname": "Humanities in the European Research Area"
  },
  {
    "id": "pending_org_::8fe14cebd4cae853b559ddc4c99ff6dd",
    "name": "CROWDHELIX LIMITED",
    "shortname": "CROWDHELIX LIMITED"
  },
  {
    "id": "openorgs____::36fa5458d6942f10554244cb9a6d50b3",
    "name": "Parameter Space (Ireland)",
    "shortname": "Parameter Space (Ireland)"
  },
  {
    "id": "pending_org_::3675f6f4d95d914617d446bdd9c2abc1",
    "name": "HUMAN DX EU, LTD",
    "shortname": "HUMAN DX"
  },
  {
    "id": "openorgs____::9c5f40739c510bb44f10268de7326cc0",
    "name": "Wood Group Kenny",
    "shortname": "Wood Group Kenny"
  },
  {
    "id": "openorgs____::bb877895894628ad40111fe657e3c317",
    "name": "Alkermes (Ireland)",
    "shortname": "Alkermes (Ireland)"
  },
  {
    "id": "pending_org_::105b19e8e1835fcec85108d983b10505",
    "name": "Wavebob Ltd",
    "shortname": "Wavebob Ltd"
  },
  {
    "id": "openorgs____::fd743453517031cebdc2c2dd2d250139",
    "name": "GlaxoSmithKline (Ireland)",
    "shortname": "GlaxoSmithKline (Ireland)"
  },
  {
    "id": "openorgs____::a95a44ac2c54112257c80f9bdecc7cbd",
    "name": "Department of Children, Equality, Disability, Integration and Youth",
    "shortname": "DCEDIY"
  },
  {
    "id": "openorgs____::54cd984fc7d3b153ec2181f985041f02",
    "name": "South East Technological University",
    "shortname": "WIT"
  },
  {
    "id": "pending_org_::efafdcc69bf0a5535fd9328287376315",
    "name": "QUALIA ANALYTICS LIMITED",
    "shortname": "Qualia Analytics"
  },
  {
    "id": "pending_org_::8497790ae66b59c6d9c751d968b49db7",
    "name": "SOUTH DUBLIN COUNTY COUNCIL",
    "shortname": "SOUTH DUBLIN COUNTY COUNCIL"
  },
  {
    "id": "openorgs____::9adcc54bbb9e7c8101c856f09a709bc8",
    "name": "Brandon Bioscience (Ireland)",
    "shortname": "Brandon Bioscience (Ireland)"
  },
  {
    "id": "openorgs____::9baa12fe5912c84fb1aac7877a144534",
    "name": "Immigrant Council of Ireland",
    "shortname": "Immigrant Council of Ireland"
  },
  {
    "id": "openorgs____::4b57256d0272a3405aa0decfccc10d85",
    "name": "OPKO Health (Ireland)",
    "shortname": "OPKO Health (Ireland)"
  },
  {
    "id": "openorgs____::5b98430f87246d10ae32f3d78440e7b7",
    "name": "Merlin Park University Hospital",
    "shortname": "Merlin Park University Hospital"
  },
  {
    "id": "openorgs____::19bd8af89bdb4f2b397c290464ad6616",
    "name": "Research Data Alliance",
    "shortname": "RDA"
  },
  {
    "id": "openorgs____::e3704dcf3ce3044b24afc877b8b8bd98",
    "name": "Tyndall National Institute (TNI)",
    "shortname": "Tyndall National Institute (TNI)"
  },
  {
    "id": "pending_org_::83cfa016fe5377e9883187d06a954657",
    "name": "SPACE TECHNOLOGY IRELAND LIMITED",
    "shortname": "SPACE TECHNOLOGY IRELAND"
  },
  {
    "id": "pending_org_::ac159d73859f4a3878952b115ee196c8",
    "name": "Ecosphere Ltd",
    "shortname": "Ecosphere Ltd"
  },
  {
    "id": "pending_org_::c917329e38f5d9de5fbea9bdf5b2951a",
    "name": "BIOTRICITY LIMITED",
    "shortname": "BIOTRICITY"
  },
  {
    "id": "pending_org_::22e1f6e5a4bb040d24e1fd20bf6b18ff",
    "name": "BURNSIDE EUROCYL LIMITED",
    "shortname": "BURNSIDE"
  },
  {
    "id": "pending_org_::a8a3e07f408a5c02772dc8a2272e5dad",
    "name": "KERRY COUNTY COUNCIL",
    "shortname": "KERRY COUNTY COUNCIL"
  },
  {
    "id": "pending_org_::f01340c40119ce53e03265d47e9dfaa5",
    "name": "DANU SPORTS LIMITED",
    "shortname": "DANU SPORTS"
  },
  {
    "id": "openorgs____::acb786e14e67684cce34cf7708824662",
    "name": "AnaBio Technologies (Ireland)",
    "shortname": "AnaBio Technologies (Ireland)"
  },
  {
    "id": "pending_org_::1011043072edd720a8e65518201bb1c8",
    "name": "BOYNE RESEARCH INSTITUTE LIMITED",
    "shortname": "BOYNE"
  },
  {
    "id": "pending_org_::6962f10ca4322335c391d5c277627c87",
    "name": "IES R&D",
    "shortname": "IES R&D"
  },
  {
    "id": "openorgs____::0b51930edae78d3a2f9e85a9310ea0de",
    "name": "PerkinElmer (Ireland)",
    "shortname": "PerkinElmer (Ireland)"
  },
  {
    "id": "pending_org_::8dd922b7ae7263e7ba7847693319f7b9",
    "name": "ARRAN CHEMICAL COMPANY LIMITED",
    "shortname": "Arran"
  },
  {
    "id": "pending_org_::3ee60a24c09108113decabb9daf87727",
    "name": "GKINETIC ENERGY LIMITED",
    "shortname": "GKINETIC ENERGY LIMITED"
  },
  {
    "id": "pending_org_::8499212d730e12be4e15f5bf6c3553d1",
    "name": "NATIONAL GALLERY OF IRELAND",
    "shortname": "NATIONAL GALLERY OF IRELAND"
  },
  {
    "id": "openorgs____::b8b8ca674452579f3f593d9f5e557483",
    "name": "University College Cork",
    "shortname": "UCC"
  },
  {
    "id": "pending_org_::0bd1dff4cbf5c7b8c59252d7a015e3c5",
    "name": "MARINE COMPUTATION SERVICES LTD",
    "shortname": "MCS"
  },
  {
    "id": "pending_org_::e33f4b7c5721360414da5b19d22556e7",
    "name": "Total Sigma Measurements Ltd",
    "shortname": "TSM Control Systems"
  },
  {
    "id": "orgreg______::6205c5a274caad7ccd19c3839700a4f8",
    "name": "The State Laboratory",
    "shortname": ""
  },
  {
    "id": "openorgs____::f2743b65e4ef4fdf0088ec480c02539d",
    "name": "DiaSorin (Ireland)",
    "shortname": "DiaSorin (Ireland)"
  },
  {
    "id": "openorgs____::44309414e42ecc14780aecb6dc8db6a6",
    "name": "Department of Agriculture Food and the Marine",
    "shortname": "Department of Agriculture Food and the Marine"
  },
  {
    "id": "pending_org_::2ca65d8ef402aa92b446c2ced0e4dc10",
    "name": "Our Lady's Hospital School",
    "shortname": "Our Lady's Hospital School"
  },
  {
    "id": "pending_org_::15d3eaf6604d38fb40149ea84407ef2e",
    "name": "HUAWEI TECHNOLOGIES (IRELAND) CO LIMITED",
    "shortname": "HUAWEI TECHNOLOGIES (IRELAND) CO LIMITED"
  },
  {
    "id": "pending_org_::d37cec93cb47958669b3488a426334a1",
    "name": "Macra na Feirme",
    "shortname": "Macra na Feirme"
  },
  {
    "id": "pending_org_::53a6abdc9487af4d9ed6350dd4fd2775",
    "name": "RESPECT Limited",
    "shortname": "RESPECT Ltd"
  },
  {
    "id": "pending_org_::76bc4a8eb57673fbe6e8da03ab2c53f4",
    "name": "Froebel College of Education",
    "shortname": "Froebel College of Education"
  },
  {
    "id": "pending_org_::81e08c7a18280a9599c22d75603ec3c0",
    "name": "STILLWATER COMMUNICATIONS LIMITED",
    "shortname": "STILLWATER COMMUNICATIONS LIMITED"
  },
  {
    "id": "pending_org_::7368bbcdf76706db7c610017b2cb6afc",
    "name": "IRVING OIL WHITEGATE REFINERY LIMITED",
    "shortname": "IRVING OIL WHITEGATE REFINERY LIMITED"
  },
  {
    "id": "openorgs____::ac24979127f13dfff87c2db063498032",
    "name": "Ballyhoura Development (Ireland)",
    "shortname": "Ballyhoura Development (Ireland)"
  },
  {
    "id": "pending_org_::f51ef252e346c833ffc135282aabfdcf",
    "name": "St. Patrick's College (a College of Dublin City University)",
    "shortname": "St. Patrick's College (a College of Dublin City University)"
  },
  {
    "id": "pending_org_::d4a40afb489ebdcf163ad20ef056e63f",
    "name": "Marigot Ltd",
    "shortname": "Marigot Ltd"
  },
  {
    "id": "openorgs____::7686670cc66bb7cab5c1c0a40826ea3c",
    "name": "University Hospital Waterford",
    "shortname": "WRH"
  },
  {
    "id": "pending_org_::2c58098ed335aa0049c4b79c12af9636",
    "name": "THE CIRCA GROUP EUROPE LIMITED",
    "shortname": "CIRCA Group Europe"
  },
  {
    "id": "openorgs____::a79fc2956c014e7f22ea425146548c00",
    "name": "DELL (Ireland)",
    "shortname": "DELL (Ireland)"
  },
  {
    "id": "pending_org_::13f5b13c695720dbc94679da13ee74a7",
    "name": "WORDSWORTH LEARNING LIMITED",
    "shortname": "WORDSWORTH LEARNING LIMITED"
  },
  {
    "id": "pending_org_::9bffcac9175a4745cfbfa9f0c342738c",
    "name": "CHANCEL LIMITED",
    "shortname": "CHANCEL LIMITED"
  },
  {
    "id": "pending_org_::4556926666dcb302752bcb21f2f39963",
    "name": "THE REVENUE COMMISSIONERS",
    "shortname": "THE REVENUE COMMISSIONERS"
  },
  {
    "id": "pending_org_::0c0a3c7309ab84039400ec090dba533b",
    "name": "BRANDON PRODUCTS LTD",
    "shortname": "BRANDON PRODUCTS LTD"
  },
  {
    "id": "openorgs____::78f20a65d84488a614e9d6cc7a6c6bdd",
    "name": "Mary Immaculate College",
    "shortname": "Mary Immaculate College"
  },
  {
    "id": "pending_org_::082681897974daca7b408835ae75e01f",
    "name": "National Standards Authority of Ireland",
    "shortname": "NSAI"
  },
  {
    "id": "pending_org_::1305292d06a5936150bb1cd0763670ec",
    "name": "FOREST ENTERPRISES LIMITED",
    "shortname": "FOREST ENTERPRISES LIMITED"
  },
  {
    "id": "openorgs____::c8b90ae910f5111c0899028acaef3b9c",
    "name": "National Council for the Blind of Ireland",
    "shortname": "NCBI"
  },
  {
    "id": "openorgs____::609e52e323c8b2384167326a956e4dc2",
    "name": "Disability Federation of Ireland",
    "shortname": "DFI"
  },
  {
    "id": "pending_org_::59575d22f73ce0557e9d16ee8b0c8d51",
    "name": "WEXFORD WOOD PRODUCERS LTD",
    "shortname": "WWP"
  },
  {
    "id": "openorgs____::890a703e772e1cb7b50c30df1821b583",
    "name": "National Rehabilitation Hospital",
    "shortname": "NRH"
  },
  {
    "id": "pending_org_::b4169828ea0c78a5f6640d077855f60a",
    "name": "Shire",
    "shortname": "Shire"
  },
  {
    "id": "openorgs____::12acaac0f9b511092bab67eb10ea5e5d",
    "name": "Connolly Hospital Blanchardstown",
    "shortname": "Connolly Hospital Blanchardstown"
  },
  {
    "id": "pending_org_::422688f07f0193011589f1f29aef587d",
    "name": "APPLIED INTELLIGENCE ANALYTICS LIMITED",
    "shortname": "APPLIED INTELLIGENCE ANALYTICS"
  },
  {
    "id": "pending_org_::58a2eb8dbba33ba0f2a2fd67c4265ecd",
    "name": "ENERIT LIMITED",
    "shortname": "ENERIT LIMITED"
  },
  {
    "id": "pending_org_::755f8fd6b93d322b87f827cba1960f93",
    "name": "The Royal Society - Grant",
    "shortname": "The Royal Society - Grant"
  },
  {
    "id": "pending_org_::c50d37ae6511374271f3bb3296afe6ab",
    "name": "VALITACELL LTD",
    "shortname": "VALITACELL LTD"
  },
  {
    "id": "openorgs____::49b04aa97360c1ced7dbd2f1440f3710",
    "name": "Ecological Consultancy Services (Ireland)",
    "shortname": "Ecological Consultancy Services (Ireland)"
  },
  {
    "id": "pending_org_::5eb29ebc2099d8a0ff990d02be5320fe",
    "name": "MOVIDIUS LTD",
    "shortname": "MOVIDIUS LTD"
  },
  {
    "id": "pending_org_::844590d7be9b04c4ddd36a786a1ad404",
    "name": "University of Dublin Dept. of Mathematics University College",
    "shortname": "University of Dublin Dept. of Mathematics University College"
  },
  {
    "id": "openorgs____::34f487f8789e751eeaf056562cd2cc78",
    "name": "Department of the Environment, Climate and Communications",
    "shortname": "DECC"
  },
  {
    "id": "pending_org_::622828e002571100c3f77cb633c3144f",
    "name": "CDETB",
    "shortname": "CDETB"
  },
  {
    "id": "openorgs____::39a349a5ed2e66d19cb005cf5ebddb2c",
    "name": "Transport Infrastructure Ireland",
    "shortname": "TII"
  },
  {
    "id": "openorgs____::73e40271841cec4e7abddb9af4fabe58",
    "name": "Mental Health Ireland",
    "shortname": "MHI"
  },
  {
    "id": "openorgs____::89ee7f2e8fad686a2c3e534d012233d5",
    "name": "Department of Culture, Heritage and the Gaeltacht",
    "shortname": "Department of Culture, Heritage and the Gaeltacht"
  },
  {
    "id": "pending_org_::cf28ee748c24cce82c2ffd723e5e37f2",
    "name": "SURGACOLL TECHNOLOGIES LIMITED",
    "shortname": "SURGACOLL"
  },
  {
    "id": "openorgs____::874d871cfe70abaaf54e2b7eacd259b5",
    "name": "Health and Safety Authority",
    "shortname": "HSA"
  },
  {
    "id": "pending_org_::33744f62ef6535983c15882d96287fb8",
    "name": "Teagasc",
    "shortname": "Teagasc"
  },
  {
    "id": "openorgs____::c473fdb3a3a0da35b9bd0ecfc458c53c",
    "name": "Eurofins (Ireland)",
    "shortname": "Eurofins (Ireland)"
  },
  {
    "id": "pending_org_::f853e9119e6e966991a7aad30eec47ac",
    "name": "MOVIDIUS LIMITED",
    "shortname": "MOVIDIUS LIMITED"
  },
  {
    "id": "pending_org_::0ea00ce6a9909628539314369af8a356",
    "name": "VITRA IRELAND LIMITED",
    "shortname": "VITRA"
  },
  {
    "id": "openorgs____::ee67d5c9dbff684db36cfd0372a5feca",
    "name": "Brennan & Company (Ireland)",
    "shortname": "Brennan & Company (Ireland)"
  },
  {
    "id": "openorgs____::021b255ad5ffe71e3228adebf043b21a",
    "name": "National Youth Council of Ireland",
    "shortname": "NYCI"
  },
  {
    "id": "openorgs____::0276a5f2dd2502bdd91b4ed0d74317b5",
    "name": "Teva Pharmaceuticals (Ireland)",
    "shortname": "Teva Pharmaceuticals (Ireland)"
  },
  {
    "id": "openorgs____::62757678f996635add4c79e7bb42e467",
    "name": "St. James's Hospital",
    "shortname": "SJH"
  },
  {
    "id": "openorgs____::0d35b47736d1062075469e8aec58a5ba",
    "name": "ICS Skills",
    "shortname": "ICS Skills"
  },
  {
    "id": "openorgs____::6c8cfbca11bc1158f4ef607f234a3bda",
    "name": "United Technologies Research Center",
    "shortname": "UTRC"
  },
  {
    "id": "pending_org_::b4c45ef89d1737bb5699cd7bebf0dcab",
    "name": "THE EUROPEAN ASSOCIATION FOR THE STUDY OF OBESITY - IRELAND COMPANY LIMITED BY GUARANTEE",
    "shortname": "EASO"
  },
  {
    "id": "openorgs____::ae91c66eeb93d7baaf849ea5bfd10b4f",
    "name": "Impedans (Ireland)",
    "shortname": "Impedans (Ireland)"
  },
  {
    "id": "openorgs____::a239d5f72f2c5dc9a4e37ec4c3be194f",
    "name": "Our Lady of Lourdes Hospital",
    "shortname": "OLLH"
  },
  {
    "id": "openorgs____::5d5f3833969e87e9b793bd0e288e2666",
    "name": "National Adult Literacy Agency",
    "shortname": "NALA"
  },
  {
    "id": "pending_org_::f272c33c9153b0f4fae347e8109fb530",
    "name": "EQUAL 1 LABORATORIES IRELAND LIMITED",
    "shortname": "EQUAL 1 LABORATORIES IRELAND LIMITED"
  },
  {
    "id": "openorgs____::8b6a7c644b9116ed7c5e656c440bb3eb",
    "name": "Agilent Technologies (Ireland)",
    "shortname": "Agilent Technologies (Ireland)"
  },
  {
    "id": "pending_org_::74f3e4b916ba9d05530b66c1d42ef8be",
    "name": "NUTRITICS LIMITED",
    "shortname": "NUTRITICS LIMITED"
  },
  {
    "id": "openorgs____::b9b2c681040c1af4ac362e1e7454f50a",
    "name": "Clarity Centre for Sensor Web Technologies",
    "shortname": "Clarity Centre for Sensor Web Technologies"
  },
  {
    "id": "pending_org_::bb9aae83b930a838a222a83be9bf2c51",
    "name": "INTERNATIONAL MUSHROOMS LTD",
    "shortname": "INTERNATIONAL MUSHROOMS LTD"
  },
  {
    "id": "pending_org_::5723bc7111b18dd5158172149cc7cab1",
    "name": "Cellix Ltd",
    "shortname": "Cellix Ltd"
  },
  {
    "id": "ukri________::098722276799b05d57846d9f039fb2e5",
    "name": "The Office of Public Works",
    "shortname": ""
  },
  {
    "id": "openorgs____::921be8f75237b2825910e423c32568bf",
    "name": "General Paints (Ireland)",
    "shortname": "General Paints (Ireland)"
  },
  {
    "id": "pending_org_::445de1d8a229d22e64ff392e1d5942e5",
    "name": "CREDITLOGIC LIMITED",
    "shortname": "CREDITLOGIC LIMITED"
  },
  {
    "id": "pending_org_::cadd4a51571595b60df3e0d55e75370c",
    "name": "LEXISNEXIS RISK SOLUTIONS (EUROPE)LTD",
    "shortname": "LEXISNEXIS RISK SOLUTIONS (EUROPE)LTD"
  },
  {
    "id": "openorgs____::7e0bb59637dba10fcde99a057734b265",
    "name": "Learning Hub Limerick",
    "shortname": "Learning Hub Limerick"
  },
  {
    "id": "pending_org_::5d06819f5354d5de5b29ae462dcdda2b",
    "name": "FUTURE ANALYTICS CONSULTING LIMITED",
    "shortname": "FUTURE ANALYTICS CONSULTING"
  },
  {
    "id": "openorgs____::a5b60a2e7f07aa4938cae7358d01b1a2",
    "name": "University Hospital Limerick",
    "shortname": "University Hospital Limerick"
  },
  {
    "id": "openorgs____::3948755ab838d38dac250bf12d376f3e",
    "name": "Muscular Dystrophy Ireland",
    "shortname": "MDI"
  },
  {
    "id": "openorgs____::ff7953acdb9e5dc14b6168414c475d66",
    "name": "Roughan & O'Donovan Innovative Solutions",
    "shortname": "RODIS"
  },
  {
    "id": "openorgs____::3eee8ab5200b4d0f789323afea5e4c2d",
    "name": "Early Childhood Ireland",
    "shortname": "Early Childhood Ireland"
  },
  {
    "id": "pending_org_::d781890f52440708f189fb0b04bdfbc7",
    "name": "AEROGEN LTD",
    "shortname": "Aerogen"
  },
  {
    "id": "openorgs____::fa6ca26b300ec6d422deff61fccf739f",
    "name": "Bio-Marine Ingredients (Ireland)",
    "shortname": "BII"
  },
  {
    "id": "pending_org_::844d8e3c6af5befa62d89bee7fb65161",
    "name": "ORPC IRELAND LIMITED",
    "shortname": "ORPC IRELAND LIMITED"
  },
  {
    "id": "openorgs____::f28ea8138a86dfa1c9b9429422ea0ef3",
    "name": "Trinity College Library Dublin",
    "shortname": "RSS"
  },
  {
    "id": "openorgs____::586732d2d4f04fd4dd8d77701e600950",
    "name": "Atlantia Food Clinical Trials",
    "shortname": "Atlantia Food Clinical Trials"
  },
  {
    "id": "openorgs____::981d20df3fb615a6f16760b681a86e4e",
    "name": "The Soar Foundation",
    "shortname": "The Soar Foundation"
  },
  {
    "id": "openorgs____::09ed0cba7dea811cb66161d775281d18",
    "name": "Parkinson's Association of Ireland",
    "shortname": "PAI"
  },
  {
    "id": "pending_org_::805ce0a20c808e5442dbf1a8d1a718ce",
    "name": "Konnekt-able Technologies Limited",
    "shortname": "Konnekt-able Technologies"
  },
  {
    "id": "pending_org_::87bb64b8138faeb7388bee6f0be2e0b2",
    "name": "HERMITAGE PEDIGREE PIGS LTD",
    "shortname": "HPP"
  },
  {
    "id": "pending_org_::26ba2db2e9578a6b32667928cc277895",
    "name": "National Museum of Ireland - Archaeology",
    "shortname": "National Museum of Ireland - Archaeology"
  },
  {
    "id": "ror_________::7750e14d7970d82e03736675f6739558",
    "name": "Dell (Ireland)",
    "shortname": "Dell (Ireland)"
  },
  {
    "id": "openorgs____::bc3fb04508070f81bf864bb21c17168f",
    "name": "Fishing in Ireland",
    "shortname": "Fishing in Ireland"
  },
  {
    "id": "openorgs____::f7e9d102519eedee6dbd472ef95c3447",
    "name": "Midland Regional Hospital Portlaoise",
    "shortname": "Midland Regional Hospital Portlaoise"
  },
  {
    "id": "pending_org_::5d780a8adf9ce100569799c63cd995d0",
    "name": "GORTA",
    "shortname": "GORTA SELF HELP AFRICA - SHA"
  },
  {
    "id": "pending_org_::dbc7bc0c1d7635f919ca0a52fe75a0b8",
    "name": "Irish Children's Arthritis Network",
    "shortname": "iCAN"
  },
  {
    "id": "openorgs____::25d5f8b2e596850d170d31e7ba674047",
    "name": "Animal Health Ireland",
    "shortname": "AHI"
  },
  {
    "id": "pending_org_::a68f981234a781995c859c1c826f1a01",
    "name": "Geneseg",
    "shortname": "Geneseg"
  },
  {
    "id": "openorgs____::ea325368eabd14f5be39bd127d7c8d5a",
    "name": "Department of Justice and Equality",
    "shortname": "Department of Justice and Equality"
  },
  {
    "id": "pending_org_::a9dff568eaedab068dc25ea7315d0005",
    "name": "OCEANFUEL LTD",
    "shortname": "OCEANFUEL LTD"
  },
  {
    "id": "openorgs____::22c14af7928c73f46f00abe2f33a5d84",
    "name": "Department of Education",
    "shortname": "Department of Education"
  },
  {
    "id": "openorgs____::775fdbba272bebf3de17ef70a3507b44",
    "name": "Becton Dickinson (Ireland)",
    "shortname": "BD"
  },
  {
    "id": "pending_org_::6ad804f9719a4e4bf9cfd3fe698952df",
    "name": "Compuscript",
    "shortname": "Compuscript"
  },
  {
    "id": "openorgs____::0f0b3d085bea71480c78238877a60f8f",
    "name": "Dundalk Institute of Technology",
    "shortname": "DkIT"
  },
  {
    "id": "openorgs____::2a5791773fe68989b48328cd700981d6",
    "name": "HeyStaks Technologies (Ireland)",
    "shortname": "HeyStaks Technologies (Ireland)"
  },
  {
    "id": "openorgs____::5ceac21f48601b6eb2180d2b45fb80fa",
    "name": "Royal College of Physicians of Ireland",
    "shortname": "RCPI"
  },
  {
    "id": "pending_org_::c6e93e4198a1cb15776a65983b37e384",
    "name": "UDARAS EITLIOCHTA NA HEIREANN THE IRISH AVIATION AUTHORITY",
    "shortname": "IAA (COOPANS)"
  },
  {
    "id": "pending_org_::ab800b644eb93ee0296b48aa57f63ad1",
    "name": "DUNREIDY ENGINEERING LIMITED",
    "shortname": "DR"
  },
  {
    "id": "openorgs____::a346cbb87d3a77a6e20645355a776b57",
    "name": "Flamel (Ireland)",
    "shortname": "Flamel (Ireland)"
  },
  {
    "id": "openorgs____::8923f636cbe3750af7c2b126ddf7c49e",
    "name": "Intune Networks (Ireland)",
    "shortname": "Intune Networks (Ireland)"
  },
  {
    "id": "openorgs____::f62682c4e8d0d3f9d6c3a26bb2f0d1a2",
    "name": "Cappagh National Orthopaedic Hospital",
    "shortname": "Cappagh National Orthopaedic Hospital"
  },
  {
    "id": "openorgs____::88e44bd488ea25a7cdbe5c4f4d1a15a1",
    "name": "Gorta Self Help Africa",
    "shortname": "Gorta Self Help Africa"
  },
  {
    "id": "openorgs____::3bfb84fe7012ef9983dfe02104afa5d4",
    "name": "Cavan General Hospital",
    "shortname": "CGH"
  },
  {
    "id": "openorgs____::36a0d9a9afe1e130df0c019d48c39d84",
    "name": "Raidió Teilifís Éireann (Ireland)",
    "shortname": "RTÉ"
  },
  {
    "id": "openorgs____::995df17d0aa04618221953664d82e497",
    "name": "St. Finbarr's Hospital",
    "shortname": "St. Finbarr's Hospital"
  },
  {
    "id": "pending_org_::ef0c13570ff28569915a0d8ea9236791",
    "name": "OSTOFORM LIMITED",
    "shortname": "OSTOFORM"
  },
  {
    "id": "pending_org_::d02aa41d32cf8b0efb34bd2419ab3888",
    "name": "MICROSEMI IRELAND TRADING",
    "shortname": "MICROSEMI IRELAND TRADING"
  },
  {
    "id": "pending_org_::21abea6c7ebd70dacceab8eb0080d3f3",
    "name": "SONAS TECHNOLOGIES LIMITED",
    "shortname": "SONAS TECHNOLOGIES"
  },
  {
    "id": "openorgs____::bbac3546c69722157ce645a065461455",
    "name": "Pintail (Ireland)",
    "shortname": "Pintail (Ireland)"
  },
  {
    "id": "pending_org_::d7ef69d6ebd415dc5f02a541e6f91067",
    "name": "HRB Clinical Research Facility Galway",
    "shortname": "HRB Clinical Research Facility Galway"
  },
  {
    "id": "pending_org_::20fb6865551825be571334823f19cb94",
    "name": "ACCESS EARTH LIMITED",
    "shortname": "ACCESS EARTH LIMITED"
  },
  {
    "id": "pending_org_::d89d82be1125ab19d43c233031c3193e",
    "name": "Covidien",
    "shortname": "Covidien"
  },
  {
    "id": "openorgs____::d86cac710c674143174d612828f76cb6",
    "name": "Think-tank for Action on Social Change",
    "shortname": "TASC"
  },
  {
    "id": "openorgs____::d3222a3429f67366e20d125f38d7a225",
    "name": "International Society for Quality in Health Care",
    "shortname": "ISQua"
  },
  {
    "id": "openorgs____::e9f2f454f2f1d06e43110787f8546ab8",
    "name": "Rusal (Ireland)",
    "shortname": "Rusal (Ireland)"
  },
  {
    "id": "openorgs____::a0cf1c4ac485227f68de575504313295",
    "name": "National Institute for Bioprocessing Research and Training",
    "shortname": "NIBRT"
  },
  {
    "id": "openorgs____::30bebf2eb5a434d9031a721249235173",
    "name": "Áiseanna Tacaíochta",
    "shortname": "ÁT"
  },
  {
    "id": "openorgs____::6a3b485300224c78e7a835053320ca0f",
    "name": "Kingsbridge Private Hospital Sligo",
    "shortname": "Kingsbridge Private Hospital Sligo"
  },
  {
    "id": "pending_org_::3f4383f081257f7ed07969dfc2f4ae35",
    "name": "RENEWABLE ENERGY DYNAMICS TECHNOLOGY LTD",
    "shortname": "RED-T"
  },
  {
    "id": "pending_org_::bd780a527c5517bf8638afcd8bdba463",
    "name": "LABAS",
    "shortname": "LABAS"
  },
  {
    "id": "openorgs____::b88f79b7caed422fbb9061217646dbba",
    "name": "Plusvital (Ireland)",
    "shortname": "Plusvital (Ireland)"
  },
  {
    "id": "pending_org_::b88f686db881aa38e1890a943dcacbe6",
    "name": "Irish Composites Centre",
    "shortname": "Irish Composites Centre"
  },
  {
    "id": "openorgs____::9b7c7677c4ebc5af103f44771ac75f6f",
    "name": "Midland Regional Hospital Mullingar",
    "shortname": "Midland Regional Hospital Mullingar"
  },
  {
    "id": "pending_org_::f78cf1f71fecd68726d2636aa5aae088",
    "name": "Clew Bay Oyster Co-operative Society Limited",
    "shortname": "C.B.O.C."
  },
  {
    "id": "pending_org_::722bcdb2205fd9977075bd467cfff09e",
    "name": "Department of Foreign Affairs",
    "shortname": "Department of Foreign Affairs"
  },
  {
    "id": "openorgs____::f00bae2e547a968490ebb40c92bf5d45",
    "name": "Horizon Therapeutics (United Kingdom)",
    "shortname": "Horizon Therapeutics (United Kingdom)"
  },
  {
    "id": "openorgs____::0da3e6c2f6cbd26aec7c11b1174ca022",
    "name": "Irish Nutrition & Dietetic Institute",
    "shortname": "INDI"
  },
  {
    "id": "openorgs____::0d66eb61c399aab27f76d2fba8370bec",
    "name": "Google (Ireland)",
    "shortname": "Google (Ireland)"
  },
  {
    "id": "pending_org_::bd34a9ae0e67f4e2346a36abb856e9c9",
    "name": "TAPASTREET LIMITED",
    "shortname": "TAPASTREET LIMITED"
  },
  {
    "id": "pending_org_::5492cff67b59b69bae48c3897c0b2eba",
    "name": "AN POST",
    "shortname": "THE POST OFFICE"
  },
  {
    "id": "pending_org_::15e1b51dcfc77a912d7aade04abb160a",
    "name": "Open Innovation Partners Limited",
    "shortname": "Open Innovation Partners Limited"
  },
  {
    "id": "pending_org_::45d8edca1220477227ae956dacbac8f2",
    "name": "ENDECO TECHNOLOGIES LIMITED",
    "shortname": "ENDECO TECHNOLOGIES LIMITED"
  },
  {
    "id": "pending_org_::90d60b36e6190cf5cd27142ab2f19735",
    "name": "TRANSGERO LIMITED",
    "shortname": "TGO"
  },
  {
    "id": "openorgs____::078cd649c045824ce53394d8bee230eb",
    "name": "Eli Lilly (Ireland)",
    "shortname": "Eli Lilly (Ireland)"
  },
  {
    "id": "pending_org_::19e88f2681aac2a28a523ca06da9ee84",
    "name": "CONNAUGHT ELECTRONICS LIMITED",
    "shortname": "VALEO VISION SYSTEMS"
  },
  {
    "id": "pending_org_::f6ed381bf99d743b1ea95903f3c1e4a4",
    "name": "IIMC INTERNATIONAL INFORMATION MANAGEMENT CORPORATION LIMITED",
    "shortname": "IIMC"
  },
  {
    "id": "pending_org_::5abc98a958ed520835c6d8c18774f35d",
    "name": "Association for Dental Education In Europe",
    "shortname": "Association for Dental Education In Europe"
  },
  {
    "id": "pending_org_::745c88dae2e5edd6ab8c24d16c03d781",
    "name": "NUI Galway, Economics",
    "shortname": "NUI Galway, Economics"
  },
  {
    "id": "openorgs____::c6f5bb926d2d16c650a162f9fe98857b",
    "name": "Hyperion (Ireland)",
    "shortname": "Hyperion (Ireland)"
  },
  {
    "id": "pending_org_::470994b6f1c3f7884ba7e5dadd6afb8c",
    "name": "Social Care Ireland",
    "shortname": "Social Care Ireland"
  },
  {
    "id": "pending_org_::f45b582aaaff273070028b99787c9338",
    "name": "CELIGNIS LIMITED",
    "shortname": "CELIGNIS LIMITED"
  },
  {
    "id": "pending_org_::cd9a49577e7f6ae562a44dec83ea6cd4",
    "name": "LONGLINE ENVIRONMENT LIMITED",
    "shortname": "LONGLINE ENVIRONMENT"
  },
  {
    "id": "pending_org_::058c3be69202f47098eb0c0ac5a464ce",
    "name": "KIDS SPEECH LABS LIMITED",
    "shortname": "KIDS SPEECH LABS LIMITED"
  },
  {
    "id": "pending_org_::f372a54732bf2268472cbca90f69b37e",
    "name": "RTE",
    "shortname": "RTE"
  },
  {
    "id": "pending_org_::675d0c29a25cf3da9060886fed00308e",
    "name": "ZUTEC INC. (IRELAND) LIMITED",
    "shortname": "ZUTEC"
  },
  {
    "id": "pending_org_::08662392332ed12ebf0d3fe3a23c7c5f",
    "name": "Western Vascular Institute Ltd",
    "shortname": "Western Vascular Institute Ltd"
  },
  {
    "id": "pending_org_::1b80574eb794a4bc46a61414f4242cdf",
    "name": "TYCO IRELAND LIMITED",
    "shortname": "TYCO IRELAND LIMITED"
  },
  {
    "id": "pending_org_::20d2202ddc8ebe1b1d857c92cd4789e9",
    "name": "Irish Learning Technology Association",
    "shortname": "Irish Learning Technology Association"
  },
  {
    "id": "pending_org_::060de55bd07658074228ffd100376eb6",
    "name": "LIEBHERR CONTAINER CRANES LIMITED",
    "shortname": "LIEBHERR CONTAINER CRANES LIMITED"
  },
  {
    "id": "openorgs____::e1790e9219b8b2ad685f6328111a0c31",
    "name": "Isles International University",
    "shortname": "IIU"
  },
  {
    "id": "openorgs____::2dfb06bf4b274d17e07f23fe8be13023",
    "name": "Allergan (Ireland)",
    "shortname": "Allergan (Ireland)"
  },
  {
    "id": "openorgs____::30bf20074d7a96fb7d1c0bb803702aed",
    "name": "Coombe Women & Infants University Hospital",
    "shortname": "Coombe Women & Infants University Hospital"
  },
  {
    "id": "pending_org_::e8f749f1f8f8be7daae921794d388658",
    "name": "CODE PLUS LIMITED",
    "shortname": "CODE PLUS LIMITED"
  },
  {
    "id": "openorgs____::ecddf79ebc0c796a1c04db91a13ba3d9",
    "name": "Irish Life (Ireland)",
    "shortname": "Irish Life (Ireland)"
  },
  {
    "id": "openorgs____::34e1625ae7028cb1645f40737587bc01",
    "name": "eBay (Ireland)",
    "shortname": "eBay (Ireland)"
  },
  {
    "id": "pending_org_::76773094a432c2881dd935f0193c0240",
    "name": "POWER CAPITAL RENEWABLE ENERGY LTD",
    "shortname": "POWER CAPITAL RENEWABLE ENERGY LTD"
  },
  {
    "id": "openorgs____::d0a2bebff169f6b17ac9b66726b73c25",
    "name": "St Michael’s Hospital",
    "shortname": "St Michael’s Hospital"
  },
  {
    "id": "openorgs____::71b52edd9a78646cc654643898912763",
    "name": "Dublin Dental University Hospital",
    "shortname": "DDUH"
  },
  {
    "id": "openorgs____::a5ece25d92b4df7109f13f00eac145a9",
    "name": "Cork Alliance Centre",
    "shortname": "Cork Alliance Centre"
  },
  {
    "id": "pending_org_::bac49fcb410f158fe8013746c3468a42",
    "name": "DUN LAOGHAIRE RATHDOWN COUNTY COUNCIL",
    "shortname": "DLRCOCO"
  },
  {
    "id": "sfi_________::17d9f145189f0a2958126dd65bb8bffe",
    "name": "Fighting Blindness",
    "shortname": ""
  },
  {
    "id": "pending_org_::cd052ed7ce670b11525c26b754cf1075",
    "name": "COMMERCIAL MUSHROOM PRODUCERS COOPERATIVE SOCIETY LTD",
    "shortname": "CMP"
  },
  {
    "id": "openorgs____::afcaa0b546b645ede6a7a072fc1b6868",
    "name": "Convex Electrical (Ireland)",
    "shortname": "Convex Electrical (Ireland)"
  },
  {
    "id": "openorgs____::0dfcf0c625003c85bba6307de68a4901",
    "name": "Schools of Medicine and Biochemistry & Immunology Trinity College Dublin",
    "shortname": "Schools of Medicine and Biochemistry & Immunology Trinity College Dublin"
  },
  {
    "id": "pending_org_::f850435fd0d15cc50cce0407fcf0646f",
    "name": "RESOLUTE MARINE LIMITED",
    "shortname": "RML"
  },
  {
    "id": "pending_org_::c491f9b162ff4c8763b74d759a8bc6e3",
    "name": "CRUSETON LIMITED",
    "shortname": "DAC"
  },
  {
    "id": "openorgs____::532c18c147033fdab04b1d5ece1bf014",
    "name": "Irish Business and Employers Confederation",
    "shortname": "IBEC"
  },
  {
    "id": "openorgs____::eb50b6f100b680278585d68a4f619999",
    "name": "Trinity College Dublin",
    "shortname": "Trinity College Dublin, Ireland"
  },
  {
    "id": "openorgs____::423f2287e5ea569d05383d10271d779c",
    "name": "Department of Housing, Planning and Local Government",
    "shortname": "Department of Housing, Planning and Local Government"
  },
  {
    "id": "openorgs____::916b69b2ad1cb3f699f3ed41b1e53b08",
    "name": "Innovation Zed (Ireland)",
    "shortname": "Innovation Zed (Ireland)"
  },
  {
    "id": "openorgs____::f52b5d72394877007a3a1ee0740bbb61",
    "name": "ECDL Foundation",
    "shortname": "ICDL"
  },
  {
    "id": "openorgs____::ebea2ef900ab922ae769cee1893030c4",
    "name": "Cystic Fibrosis Ireland",
    "shortname": "CF"
  },
  {
    "id": "pending_org_::5a0d6fac90bd131450f6305a424ed168",
    "name": "Childhood Development Initiative CLG",
    "shortname": "Childhood Development Initiative CLG"
  },
  {
    "id": "pending_org_::f0036985248f8d459496140acb28d58f",
    "name": "Vox Power Ltd",
    "shortname": "Vox Power"
  },
  {
    "id": "openorgs____::c02f7820e47303e58b750a80fef424e6",
    "name": "South Tipperary General Hospital",
    "shortname": "STGH"
  },
  {
    "id": "pending_org_::009dae1d8ee3fee3651f2a3ba517d6e2",
    "name": "TALLAGHT WEST CHILDHOOD DEVELOPMENTINITIATIVE COMPANY LBG",
    "shortname": "CHILDHOOD DEVELOPMENT INITIATIVE"
  },
  {
    "id": "openorgs____::c4d8fc61e4b4d7f7e6361cc1b80502f9",
    "name": "Glór",
    "shortname": "Glór"
  },
  {
    "id": "pending_org_::c440928d7b6d8d328b1d4f80840e7677",
    "name": "Accountancy &amp; Business College (Ireland) Limited trading as Dublin Business School",
    "shortname": "Accountancy &amp; Business College (Ireland) Limited trading as Dublin Business School"
  },
  {
    "id": "pending_org_::0a193e526516f1c86c3fa4ed6c619520",
    "name": "Recordati (Ireland)",
    "shortname": "Recordati (Ireland)"
  },
  {
    "id": "pending_org_::1244e274b137362bdfde3b8295462555",
    "name": "SHANNON COILED SPRINGS LIMITED",
    "shortname": "SHANNON COILED SPRINGS LIMITED"
  },
  {
    "id": "openorgs____::e22e45b0539dffb38b2726ae4ec532c4",
    "name": "National Disability Authority",
    "shortname": "NDA"
  },
  {
    "id": "pending_org_::ab62a76c119a29ff8a7c7f58cd6962ce",
    "name": "National Botanic Gardens of Ireland",
    "shortname": "National Botanic Gardens of Ireland"
  },
  {
    "id": "openorgs____::804ace869a3a58577bb5286e8448d6ae",
    "name": "Health Protection Surveillance Centre",
    "shortname": "HPSC"
  },
  {
    "id": "openorgs____::1e2a547a192e9c937806576c4817781a",
    "name": "Shinawil (Ireland)",
    "shortname": "Shinawil (Ireland)"
  },
  {
    "id": "pending_org_::75f1b38c571f2a3591441677709b258d",
    "name": "CARLOW EMERGENCY DOCTORS-ON-CALL COMPANY LIMITED BY GUARANTEE",
    "shortname": "CARLOW EMERGENCY DOCTORS-ON-CALL COMPANY LIMITED BY GUARANTEE"
  },
  {
    "id": "openorgs____::7e98eabdb9273550f21df4d0a75b51dc",
    "name": "CyberColloids",
    "shortname": "CyberColloids"
  },
  {
    "id": "openorgs____::bffc88dc244dea08c265cc4f637d514e",
    "name": "All Ireland Institute for Hospice and Palliative Care",
    "shortname": "AIIHPC"
  },
  {
    "id": "pending_org_::1b2d6979cab95d5283e3147718c4af7c",
    "name": "HAUNTED PLANET STUDIOS LTD",
    "shortname": "HAUNTED PLANET STUDIOS LTD"
  },
  {
    "id": "openorgs____::c9a7e9a48e6a676c125d67c8e53affd3",
    "name": "Shannon Applied Biotechnology Centre",
    "shortname": "Shannon Applied Biotechnology Centre"
  },
  {
    "id": "pending_org_::7ca7b2f55b43a2b38370b08c02eab1bf",
    "name": "EpiSensor Ltd",
    "shortname": "EpiSensor Ltd"
  },
  {
    "id": "openorgs____::eebb1999d03faff318f9709f0bce1803",
    "name": "Science Foundation Ireland",
    "shortname": "SFI"
  },
  {
    "id": "openorgs____::f279aa30b2a21da2029498f723dd3b98",
    "name": "Electro Automation (Ireland)",
    "shortname": "Electro Automation (Ireland)"
  },
  {
    "id": "pending_org_::5cf64c9af68c9939e7ff510cdb99f4f8",
    "name": "SUPERNODE LIMITED",
    "shortname": "SUPERNODE LIMITED"
  },
  {
    "id": "openorgs____::a298aca8491ac8a4825c5f5dc3032e28",
    "name": "Pavee Point",
    "shortname": "Pavee Point"
  },
  {
    "id": "openorgs____::13ba4506b7b888b1c583e01bcdfea5ab",
    "name": "Centre for Research in Engineering Surface Technology",
    "shortname": "CREST"
  },
  {
    "id": "pending_org_::15e16b6834597217e1af907aa3ff81b8",
    "name": "TRUSTWATER LIMITED",
    "shortname": "TrustWater"
  },
  {
    "id": "corda__h2020::f111da14f4a6b86700822e4ee5979082",
    "name": "NATIONAL ROADS AUTHORITY",
    "shortname": "TRANSPORT INFRASTRUCTURE IRELAND"
  },
  {
    "id": "pending_org_::5b4fcef9cac02e97df306fb17f470216",
    "name": "Justice for Magdalenes Research",
    "shortname": "JFMR"
  },
  {
    "id": "pending_org_::cb7ad46ec17975e56386016fed91e530",
    "name": "ALTRATECH LIMITED",
    "shortname": "ALTRATECH LIMITED"
  },
  {
    "id": "openorgs____::4cac0e9acbb78f5b8325ca2756304d58",
    "name": "Local Enterprise Office Cork North & West",
    "shortname": "Local Enterprise Office Cork North & West"
  },
  {
    "id": "openorgs____::7f81611b3b44f0bdb3c7a207dfe16158",
    "name": "Gavin and Doherty Geosolutions (Ireland)",
    "shortname": "GDG"
  },
  {
    "id": "pending_org_::2b7316f2b427cafdec9952b5a4f3a82b",
    "name": "INCORAS SOLUTIONS LIMITED",
    "shortname": "INC"
  },
  {
    "id": "openorgs____::a890b9e319b7906486bc5ef5d1b5bc9b",
    "name": "CAPTEC (Ireland)",
    "shortname": "CAPTEC (Ireland)"
  },
  {
    "id": "pending_org_::a8b4edd25cee34ebf89e1fa7b2f766d0",
    "name": "KOKOMO HEALTHCARE LIMITED",
    "shortname": "KOKOMO"
  },
  {
    "id": "openorgs____::b083fcf0bc02fd0b4390de98bdb2ce35",
    "name": "Beaumont Hospital",
    "shortname": "Beaumont Hospital"
  },
  {
    "id": "pending_org_::f74a35012afa4645f35aeb3925f61b67",
    "name": "Comhar Teoranta",
    "shortname": "Comhar Teoranta"
  },
  {
    "id": "pending_org_::d76c91e86cfd733b91a6d1817a559c60",
    "name": "BANAGHER PRECAST CONCRETE LIMITED",
    "shortname": "BANAGHER PRECAST CONCRETE LIMITED"
  },
  {
    "id": "pending_org_::a73ee9e1565c13696184ac1474529ccc",
    "name": "SPACE ENGAGERS COMPANY LIMITED BY GUARANTEE",
    "shortname": "SPACE ENGAGERS"
  },
  {
    "id": "openorgs____::4acec728556ae07b0533bd11f4d2fc83",
    "name": "Junior Achievement",
    "shortname": "JA"
  },
  {
    "id": "pending_org_::36ad4f0ee8d8568b6902e9752b1801ed",
    "name": "NEMYSIS LIMITED",
    "shortname": "NEMYSIS"
  },
  {
    "id": "openorgs____::3af90f75c9a16a78ce5a735c9238d12d",
    "name": "BioClin (Ireland)",
    "shortname": "BioClin (Ireland)"
  },
  {
    "id": "pending_org_::3cba6e66cb57db89b7de7f8456eee370",
    "name": "ANIMATED LANGUAGE LEARNING LIMITED",
    "shortname": "Animated Language Learning"
  },
  {
    "id": "pending_org_::841cc510e1ded9fa8ff6b34369987cff",
    "name": "WILD ATLANTIC SHELLFISH LIMITED",
    "shortname": "WILD ATLANTIC SHELLFISH LIMITED"
  },
  {
    "id": "pending_org_::b84fbd0d1ced3457ae9450b4571e600c",
    "name": "BREPCO BIOPHARMA LTD",
    "shortname": "BrePco Bioharma"
  },
  {
    "id": "pending_org_::412a97a7623c174bc38f99d062918bbe",
    "name": "EUROPEAN INSTITUTE OF WOMEN'S HEALTH COMPANY LIMITED BY GUARANTEE",
    "shortname": "EIWH"
  },
  {
    "id": "pending_org_::71e2f7196aabbe100a29920d28223496",
    "name": "Our Lady's Hospice Ltd",
    "shortname": "Our Lady's Hospice Ltd"
  },
  {
    "id": "openorgs____::18dced1d79c34955f79e0739c87dbd5d",
    "name": "Bausch Health (Ireland)",
    "shortname": "Bausch Health (Ireland)"
  },
  {
    "id": "pending_org_::759bfb0a14bc05702e5330f8125d543d",
    "name": "THE BERE ISLAND PROJECTS GROUP COMPANY LIMITED BY GUAANTEE",
    "shortname": "BERE ISLAND PROJECTS GROUP"
  },
  {
    "id": "openorgs____::35af7a7323cf97acaaafbad9d357975c",
    "name": "Renishaw (Ireland)",
    "shortname": "Renishaw (Ireland)"
  },
  {
    "id": "pending_org_::75cba6de9f1fca83c65d37d861259537",
    "name": "EACHTRA ARCHAEOLOGICAL PROJECTS LIMITED",
    "shortname": "EACHTRA ARCHAEOLOGICAL PROJECTS LIMITED"
  },
  {
    "id": "pending_org_::9bb740c46e5a96309c92f5c079fa9c55",
    "name": "VISION BUILT STRUCTURES LTD",
    "shortname": "VISION BUILT STRUCTURES LTD"
  },
  {
    "id": "pending_org_::6edbae8bd87b35ee2bef7cd091362b86",
    "name": "ENBIO LIMITED",
    "shortname": "ENBIO LIMITED"
  },
  {
    "id": "pending_org_::c60b695fc9ae9d46e8615c6066ab1b84",
    "name": "CausewayCS",
    "shortname": "CausewayCS"
  },
  {
    "id": "pending_org_::e460bfe91b126cc0db947a7793017a5f",
    "name": "NT-MDT SERVICE & LOGISTICS LTD",
    "shortname": "NT MDT S&L"
  },
  {
    "id": "pending_org_::907042efd6cbba8bf770cfbcd376259c",
    "name": "CHILDREN'S HEALTH IRELAND",
    "shortname": "CHILDREN'S HEALTH IRELAND"
  },
  {
    "id": "pending_org_::70bbebafefaea02145d695faf59ce51d",
    "name": "PLC INGREDIENTS LIMITED",
    "shortname": "PLC INGREDIENTS LIMITED"
  },
  {
    "id": "pending_org_::ba5f84a6c7dee212f239588bb8cff2be",
    "name": "ChildVision",
    "shortname": "ChildVision"
  },
  {
    "id": "openorgs____::e139b14355bc85e4062d00e67d048fcd",
    "name": "Biosensia (Ireland)",
    "shortname": "Biosensia (Ireland)"
  },
  {
    "id": "openorgs____::aab69c8b3fd1d48939b51635611438cc",
    "name": "Nexus Research",
    "shortname": "Nexus Research"
  },
  {
    "id": "openorgs____::e0c964d0c58928036f5bef76be284b53",
    "name": "VistaMed (Ireland)",
    "shortname": "VistaMed (Ireland)"
  },
  {
    "id": "pending_org_::81c9d63dd94643b2076ad0848a37c4f3",
    "name": "OIL SERVICES 4 U LIMITED",
    "shortname": "CO"
  },
  {
    "id": "openorgs____::f9470d63861577b2229e9e8132e8c98f",
    "name": "GOAL",
    "shortname": "GOAL"
  },
  {
    "id": "pending_org_::f6dc50dc043cfdc87d4e22aa400ee6cd",
    "name": "The Irish Association for Applied Linguistics",
    "shortname": "The Irish Association for Applied Linguistics"
  },
  {
    "id": "pending_org_::c0aca8d4b56c76070c01d99ba98d7413",
    "name": "DECARE DENTAL IRELAND LIMITED",
    "shortname": "DECARE DENTAL IRELAND LIMITED"
  },
  {
    "id": "openorgs____::ce0ab3e6bf60e6df680a4a1986c40364",
    "name": "Vitalograph (Ireland)",
    "shortname": "Vitalograph (Ireland)"
  },
  {
    "id": "openorgs____::4bb26425755b969ed1cee4e57a9d215d",
    "name": "Irish Sea Fisheries Board",
    "shortname": "Irish Sea Fisheries Board"
  },
  {
    "id": "pending_org_::11993a88fb4cb8bf58d92a0418338067",
    "name": "IRISH MUSEUM OF MODERN ART COMPANY",
    "shortname": "IMMA"
  },
  {
    "id": "pending_org_::0135ea6d1db36fc08cccf5c9a079d1df",
    "name": "INNOVATION AND MANAGEMENT CENTRE LIMITED",
    "shortname": "WestBIC"
  },
  {
    "id": "openorgs____::7d7b8146c0d71670c08dd00cee4ba528",
    "name": "Reagecon (Ireland)",
    "shortname": "Reagecon (Ireland)"
  },
  {
    "id": "openorgs____::88651d14e24311d7dbcb40998868ed21",
    "name": "National Economic and Social Council",
    "shortname": "NESC"
  },
  {
    "id": "pending_org_::14646bb0f3508ba99c26ba8345d10330",
    "name": "BIOPIXS LIMITED",
    "shortname": "BIOPIXS"
  },
  {
    "id": "openorgs____::70b2f485c1ad86f917609fc306b31bd2",
    "name": "SolanoTech (Ireland)",
    "shortname": "SolanoTech (Ireland)"
  },
  {
    "id": "pending_org_::19a1bba02be9b28d8089056e8a8d6161",
    "name": "Pfizer Healthcare Ireland",
    "shortname": "Pfizer Healthcare Ireland"
  },
  {
    "id": "pending_org_::4363d3bf004e955248c7453e0870038d",
    "name": "Marino School",
    "shortname": "Marino School"
  },
  {
    "id": "pending_org_::9af4af97acda3570f008f0809f231206",
    "name": "FOTONATION LIMITED",
    "shortname": "FOTONATION"
  },
  {
    "id": "openorgs____::8d379689c57036cc1e25465e61c065a1",
    "name": "Department of Foreign Affairs and Trade",
    "shortname": "Department of Foreign Affairs and Trade"
  },
  {
    "id": "pending_org_::5288310f90b0cb60224bf4a48a7ab2ff",
    "name": "Polypico",
    "shortname": "Polypico"
  },
  {
    "id": "openorgs____::b1a2dc3a00244913fcf72a5ecc9e03b2",
    "name": "Mayo County Council",
    "shortname": "Mayo County Council"
  },
  {
    "id": "pending_org_::1b305dfc69892d8ad026e2c72f9f7ff5",
    "name": "Irish Diagnostic Laboratory Services Limited",
    "shortname": "Irish Diagnostic Laboratory Services Limited"
  },
  {
    "id": "openorgs____::ae4c2c18137ddf89e58dca06a26542df",
    "name": "Mallow General Hospital",
    "shortname": "MGH"
  },
  {
    "id": "pending_org_::240571203e64d21a731b79f4744708a8",
    "name": "SEQOME LIMITED",
    "shortname": "SEQOME LIMITED"
  },
  {
    "id": "openorgs____::7eda8d8128005a4afee3fc146ba7d260",
    "name": "Ipsen (Ireland)",
    "shortname": "Ipsen (Ireland)"
  },
  {
    "id": "openorgs____::98fdbaa82d75300aa4ceef5148c844d4",
    "name": "Trinity College Dublin Department of Geology",
    "shortname": "Trinity College Dublin Department of Geology"
  },
  {
    "id": "ukri________::6c674dc7de4283515ce9ef2063f530a2",
    "name": "TSM Control Systems",
    "shortname": ""
  },
  {
    "id": "sfi_________::5c390a55237d37114fd224ce7ac3ee5d",
    "name": "The Institution of Engineers of Ireland",
    "shortname": ""
  },
  {
    "id": "openorgs____::fd14a4b6009bfed3685d9955ac7bb52e",
    "name": "State Laboratory",
    "shortname": "State Laboratory"
  },
  {
    "id": "openorgs____::43d8b0155e33ea8e9d465e05cd47b649",
    "name": "Our Lady's Children's Hospital",
    "shortname": "Our Lady's Children's Hospital"
  },
  {
    "id": "pending_org_::3655652d2e83446e9b91fad4ea7a2b9c",
    "name": "SEED TECHNOLOGY LIMITED",
    "shortname": "Seedtech"
  },
  {
    "id": "openorgs____::7a172148349b6d0ca3aa119e5160bbf7",
    "name": "Inter-Euro Technology",
    "shortname": "IET"
  },
  {
    "id": "pending_org_::0e08debe357bef65e4565b75c56432f8",
    "name": "ORIGIN ENTERPRISES PUBLIC LIMITED COMPANY",
    "shortname": "ORIGIN"
  },
  {
    "id": "pending_org_::8b2fdbac035cb437c4f7af97a5caa5be",
    "name": "Carl Diver Advanced Manufacturing Consulting Limited",
    "shortname": "CDAMC"
  },
  {
    "id": "openorgs____::b53a395ecda7a63815952697e7480457",
    "name": "DS Biopharma (Ireland)",
    "shortname": "DS Biopharma (Ireland)"
  },
  {
    "id": "openorgs____::fc8b3f2ee9b3e670ada7686d06660a3c",
    "name": "Bon Secours Hospital Galway",
    "shortname": "Bon Secours Hospital Galway"
  },
  {
    "id": "openorgs____::1a761172cf5ef0477b131773fc93dbe6",
    "name": "Abbott (Ireland)",
    "shortname": "Abbott (Ireland)"
  },
  {
    "id": "pending_org_::27679b24fa35a2019fd543bd0fc53aec",
    "name": "Alcatel Lucent Ireland Limited",
    "shortname": "Alcatel Lucent Ireland Limited"
  },
  {
    "id": "openorgs____::f4f00c6f57dd9717b493c1d492ad7320",
    "name": "Geological Survey of Ireland",
    "shortname": "Geological Survey of Ireland"
  },
  {
    "id": "openorgs____::7c471da97687baadd63d912264917022",
    "name": "University of Limerick",
    "shortname": "UL"
  },
  {
    "id": "openorgs____::19c4eeb1d9063b603b71879ca36d8f3a",
    "name": "Brandtone (Ireland)",
    "shortname": "Brandtone (Ireland)"
  },
  {
    "id": "openorgs____::bc239f2dd16acfb64605afd9b44de259",
    "name": "Alltech (Ireland)",
    "shortname": "Alltech (Ireland)"
  },
  {
    "id": "openorgs____::b1e620a1ba1e17d17e6efcc7509fee10",
    "name": "St Nicholas Trust",
    "shortname": "St Nicholas Trust"
  },
  {
    "id": "pending_org_::12fdd06274de1d3d49c5d1b1f7e2e047",
    "name": "THINKSMART TECHNOLOGIES LTD",
    "shortname": "THINKSMART TECHNOLOGIES LTD"
  },
  {
    "id": "openorgs____::1ac90ccd97e342b459228f81aa4ec5bc",
    "name": "National Platform of Self Advocates",
    "shortname": "National Platform of Self Advocates"
  },
  {
    "id": "pending_org_::ec4a3383a5858e1fbd21e5a059fb168f",
    "name": "Davis College",
    "shortname": "Davis College"
  },
  {
    "id": "pending_org_::af64beb399fbbe613a098e506653e663",
    "name": "SENSL TECHNOLOGIES LIMITED",
    "shortname": "SENSL"
  },
  {
    "id": "pending_org_::0bb0942a13406d684035cb0713b93054",
    "name": "Breakthrough Cancer Research",
    "shortname": "Breakthrough Cancer Research"
  },
  {
    "id": "openorgs____::dcb0ed8f63a396ed095ff4f51f2a8c2a",
    "name": "Irish Congress of Trade Unions",
    "shortname": "ICTU"
  },
  {
    "id": "pending_org_::55da57d512b6fddc52c7d5d030b88b9f",
    "name": "TRULY IRISH COUNTRY FOOD LIMITED",
    "shortname": "TRULY IRISH COUNTRY FOOD LIMITED"
  },
  {
    "id": "openorgs____::dd037fe6dca09ec294d67f12421b7232",
    "name": "Dermot Foley Landscape Architects (Ireland)",
    "shortname": "DFLA"
  },
  {
    "id": "pending_org_::824e7a75cc93418c3f0ca6a7afb2a590",
    "name": "St. Paul's NS",
    "shortname": "St. Paul's NS"
  },
  {
    "id": "pending_org_::d171b925a62936b3bc954634da30b625",
    "name": "Radisens Diagnostics Limited",
    "shortname": "Radisens Diagnostics Limited"
  },
  {
    "id": "pending_org_::1c1bcec4070d322a6e2375649b25edcc",
    "name": "ESB INNOVATION ROI LIMITED",
    "shortname": "ESB INNOVATION ROI LIMITED"
  },
  {
    "id": "openorgs____::7bdeb2749573637e26a5855dc554c603",
    "name": "Newpark Music Centre",
    "shortname": "Newpark Music Centre"
  },
  {
    "id": "openorgs____::cae17463e1b9d33248486e67046f55b0",
    "name": "Diageo (Ireland)",
    "shortname": "Diageo (Ireland)"
  },
  {
    "id": "openorgs____::e49d5f36c009681d2ee337b3ff216afb",
    "name": "College of Engineering and Informatics, National University of Ireland, Galway",
    "shortname": "College of Engineering and Informatics, National University of Ireland, Galway"
  },
  {
    "id": "openorgs____::e3ab5b3ba053246210dc2b6f3f76510c",
    "name": "Molloy Environmental Systems (Ireland)",
    "shortname": "Molloy Environmental Systems (Ireland)"
  },
  {
    "id": "openorgs____::f97a0d8823c38dc6b3562bb32736f49e",
    "name": "Men Overcoming Violence",
    "shortname": "MOVE"
  },
  {
    "id": "pending_org_::370e485bd26be3d1651aa9f06e285ef2",
    "name": "Numerics Warehouse Ltd",
    "shortname": "Numerics Warehouse Ltd"
  },
  {
    "id": "openorgs____::2c1871f3d54d1a92769c38b1bd195d76",
    "name": "Teagasc - The Irish Agriculture and Food Development Authority",
    "shortname": "Teagasc - The Irish Agriculture and Food Development Authority"
  },
  {
    "id": "openorgs____::1740def10300ddab542af0bf1c07dcb2",
    "name": "Economic and Social Research Institute",
    "shortname": "ESRI"
  },
  {
    "id": "pending_org_::059138a1eb220f2f12c4f4a5b8aff90f",
    "name": "ICON",
    "shortname": "ICON"
  },
  {
    "id": "pending_org_::c1f3a81d68f32e15cf55adb76f527a8b",
    "name": "VIVASURE MEDICAL LIMITED",
    "shortname": "Vivasure Medical"
  },
  {
    "id": "pending_org_::fc677fbcd32a4b7592113b4f624d68ba",
    "name": "SOCIETY FOR THE MANAGEMENT OF ELECTRONIC BIODIVERSITY DATA LIMITED",
    "shortname": "SMEBD"
  },
  {
    "id": "pending_org_::48ee580ae38d614d040938a5f7d5867e",
    "name": "FIRECOMMS LIMITED",
    "shortname": "FIRECOMMS"
  },
  {
    "id": "pending_org_::d35027ef876481fc7e8102519a0187d8",
    "name": "DECIPHEX LIMITED",
    "shortname": "DECIPHEX LIMITED"
  },
  {
    "id": "pending_org_::1eced76df3ea3786fa883174ae286b9a",
    "name": "PROFEXCEL.NET LTD",
    "shortname": "ICEP Europe ( The Institute of Child Education and Psychology Europe)"
  },
  {
    "id": "pending_org_::6f5eeaae52234ed58bc28edfb98b6762",
    "name": "Royal Institute of the Architects of Ireland",
    "shortname": "RIAI"
  },
  {
    "id": "openorgs____::4f6dca2788d1a2c5bb9827f5557c7a3a",
    "name": "Irish Society of Chartered Physiotherapists",
    "shortname": "ISCP"
  },
  {
    "id": "openorgs____::a6e7755b398ab9da87b28b9895442800",
    "name": "Transgender Equality Network Ireland",
    "shortname": "TENI"
  },
  {
    "id": "openorgs____::b1f1b4b5ec82eb4004ffb82c818ceb48",
    "name": "Engineering Solutions International (Ireland)",
    "shortname": "Engineering Solutions International (Ireland)"
  },
  {
    "id": "pending_org_::6dd9fde2d48f40d63b2d253a51c85071",
    "name": "REMEDY BIOLOGICS LIMITED",
    "shortname": "REMEDY BIOLOGICS LIMITED"
  },
  {
    "id": "openorgs____::b5956e6a9860dcc2116525d8c8b2bc2d",
    "name": "Glanbia (Ireland)",
    "shortname": "Glanbia (Ireland)"
  },
  {
    "id": "openorgs____::54dbc867844e55a3274f490d3bdbe5ad",
    "name": "Focus Ireland",
    "shortname": "Focus Ireland"
  },
  {
    "id": "pending_org_::4b953e02433ad8c2b3fb20d09b402ae8",
    "name": "IRISH CO-OPERATIVE ORGANISATION SOCIETY LIMITED",
    "shortname": "IRISH CO-OPERATIVE ORGANISATION SOCIETY LIMITED"
  },
  {
    "id": "pending_org_::94a2bcae506f2b856eb678ac8e82975c",
    "name": "BeoCare Limited",
    "shortname": "BeoCare Limited"
  },
  {
    "id": "openorgs____::f835a1a48f5ff679424b9d21365d7495",
    "name": "St. Johns Hospital",
    "shortname": "St. Johns Hospital"
  },
  {
    "id": "openorgs____::5f98f0633128f49328b63ddcedcfba40",
    "name": "Sports Surgery Clinic",
    "shortname": "SSC"
  },
  {
    "id": "pending_org_::35bf09c2d7d330961857f3c1cc488d31",
    "name": "EMC INFORMATION SYSTEMS INTERNATIONAL",
    "shortname": "EISI"
  },
  {
    "id": "openorgs____::66ee9fc00a83b9b8dcfe4d6b7cbc9965",
    "name": "World Rugby",
    "shortname": "IRB"
  },
  {
    "id": "openorgs____::7782b696e35e6379de91ce2b432b2055",
    "name": "British Council",
    "shortname": "British Council"
  },
  {
    "id": "pending_org_::080329730a1e84f223432ea8270dd90d",
    "name": "SLR ENVIRONMENTAL CONSULTING(IRELAND)LIMITED",
    "shortname": "SLR Consulting Ireland"
  },
  {
    "id": "pending_org_::dbb5dabd438dab01869b2bb79cc6c438",
    "name": "COADY PARTNERSHIP ARCHITECTS LIMITED",
    "shortname": "COADY PARTNERSHIP ARCHITECTS LIMITED"
  },
  {
    "id": "pending_org_::8f754e125a5bb19180fd6a3c964c1edf",
    "name": "VETEX MEDICAL LIMITED",
    "shortname": "VETEX MEDICAL LIMITED"
  },
  {
    "id": "openorgs____::2ab0ed533fe44b08880289223ea9b6d1",
    "name": "Shellfish Ireland (Ireland)",
    "shortname": "Shellfish Ireland (Ireland)"
  },
  {
    "id": "openorgs____::80db98569077e4af58e4c511c340021a",
    "name": "Epilepsy Ireland",
    "shortname": "Epilepsy Ireland"
  },
  {
    "id": "pending_org_::78aafba8a19ed79da7462d22960dc678",
    "name": "ECOWELLNESS CONSULTING LIMITED",
    "shortname": "ECOWELLNESS CONSULTING LIMITED"
  },
  {
    "id": "pending_org_::cad6e6bb22679c694b8c60e634b05f2c",
    "name": "CYLON CONTROLS LIMITED",
    "shortname": "CYLON CONTROLS LIMITED"
  },
  {
    "id": "pending_org_::cb205df9295b52915b63c84fd3f68470",
    "name": "ORAN PRE-CAST LIMITED",
    "shortname": "ORAN PRE-CAST LIMITED"
  },
  {
    "id": "openorgs____::8ca13935a2702dedeca5c9c67da3f29d",
    "name": "Midland Regional Hospital at Tullamore",
    "shortname": "Midland Regional Hospital at Tullamore"
  },
  {
    "id": "pending_org_::8d5b0d639cc1c2e4a2c79c6684830d8d",
    "name": "Q4 PR LIMITED",
    "shortname": "Q4 PUBLIC RELATIONS"
  },
  {
    "id": "pending_org_::0151a3143094acb398dff7859a1483f8",
    "name": "GALWAY CLINIC DOUGHISKA LIMITED",
    "shortname": "GALWAY CLINIC DOUGHISKA"
  },
  {
    "id": "openorgs____::22760f2e3072a6fdf325adbcec9d136f",
    "name": "Royal College of Surgeons in Ireland",
    "shortname": "RCSI"
  },
  {
    "id": "pending_org_::cc0fa0a92d66dbae9d53c738773dea3f",
    "name": "DEPARTMENT OF HEALTH",
    "shortname": "DEPARTMENT OF HEALTH"
  },
  {
    "id": "openorgs____::d6416558b4a74fb123f8e26b77c2aa72",
    "name": "Xeolas Pharmaceuticals (Ireland)",
    "shortname": "Xeolas Pharmaceuticals (Ireland)"
  },
  {
    "id": "pending_org_::5a26afe9f6c7b9aca4c2fa1a15790f04",
    "name": "RANDOX TEORANTA",
    "shortname": "Randox Teoranta"
  },
  {
    "id": "pending_org_::39914b2e53732247cba4fee3eb27d8ce",
    "name": "CROSSCARE LIMITED",
    "shortname": "CROSSCARE LIMITED"
  },
  {
    "id": "pending_org_::3b81ad0f409777d5201b5fe876090977",
    "name": "Atlantic Shellfish Ltd",
    "shortname": "Atlantic Shellfish Ltd"
  },
  {
    "id": "openorgs____::2c66fe328da1d3fecb924f721ae1a8f2",
    "name": "COPE Galway",
    "shortname": "COPE Galway"
  },
  {
    "id": "pending_org_::3cc4340f93cb715f262e8b4957b519e9",
    "name": "PERCH DYNAMIC SOLUTIONS LIMITED",
    "shortname": "PERCH DYNAMIC SOLUTIONS LIMITED"
  },
  {
    "id": "pending_org_::3599debade263959d12201086c15c1b4",
    "name": "The Institution of Engineering and Technology",
    "shortname": "The Institution of Engineering and Technology"
  },
  {
    "id": "pending_org_::1f4c4096884824938e4475dbf2a7cd91",
    "name": "Marine Law and Ocean Policy Research Services Ltd",
    "shortname": "Marine Law and Ocean Policy Research Services Ltd"
  },
  {
    "id": "pending_org_::45808d4a173ae3b9611016c8df2c3295",
    "name": "AIDAN MICHAEL POWER",
    "shortname": "MICHAEL"
  },
  {
    "id": "pending_org_::908d6d7ee05fe1452a62dc8317c05027",
    "name": "CREGANNA UNLIMITED COMPANY",
    "shortname": "Creganna"
  },
  {
    "id": "openorgs____::c6c77d16cc19bf4b32c3a282068b2e5c",
    "name": "Irish Thoracic Society",
    "shortname": "ITS"
  }
]
