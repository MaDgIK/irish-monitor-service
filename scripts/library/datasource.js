var datasources = [
  {
    official_name: "DCU Online Research Access Service",
    english_name: "DORAS",
    openaire_id: "opendoar____::07e1cd7dca89a1678042477183b7ac3f",
    original_id: "opendoar____::119"
  },
  {
    official_name: "Maynooth University ePrints &amp; eTheses Archive",
    english_name: "",
    openaire_id: "opendoar____::115f89503138416a242f40fb7d7f338e",
    original_id: "opendoar____::223"
  },
  {
    official_name: "NORMA",
    english_name: "NORMA",
    openaire_id: "opendoar____::12092a75caa75e4644fd2869f0b6c45a",
    original_id: "opendoar____::3443"
  },
  {
    official_name: "STÓR",
    english_name: "",
    openaire_id: "opendoar____::1680e9fa7b4dd5d62ece800239bb53bd",
    original_id: "opendoar____::2656"
  },
  {
    official_name: "Institutional Archive of Scholarly Content",
    english_name: "IASC",
    openaire_id: "opendoar____::1f16dde7e4a071f4a7649bdd0459efcd",
    original_id: "opendoar____::10512"
  },
  {
    official_name: "Trinity's Access to Research Archive",
    english_name: "TARA",
    openaire_id: "opendoar____::210f760a89db30aa72ca258a3483cc7f",
    original_id: "opendoar____::883"
  },
  {
    official_name: "South West Open Research Deposit",
    english_name: "SWORD",
    openaire_id: "opendoar____::219ece62fae865562d4510ea501cf349",
    original_id: "opendoar____::9687"
  },
  {
    official_name: "Access to Research at National University of Ireland, Galway",
    english_name: "Access to Research at National University of Ireland, Galway",
    openaire_id: "opendoar____::2b3bf3eee2475e03885a110e9acaab61",
    original_id: "opendoar____::1513"
  },
  {
    official_name: "University of Limerick Institutional Repository",
    english_name: "ULIR",
    openaire_id: "opendoar____::2ba8698b79439589fdd2b0f7218d8b07",
    original_id: "opendoar____::1249"
  },
  {
    official_name: "Arrow@TU Dublin",
    english_name: "Arrow@TU Dublin",
    openaire_id: "opendoar____::39e4973ba3321b80f37d9b55f63ed8b8",
    original_id: "opendoar____::1248"
  },
  {
    official_name: "Barnardos Knowledge Bank",
    english_name: "Barnardos Knowledge Bank",
    openaire_id: "opendoar____::491723c615d42eb8b44650bcbe384561",
    original_id: "opendoar____::10727"
  },
  {
    official_name: "SETU Open Access Repository",
    english_name: "Formerly: Waterford Institute of Technology Repository",
    openaire_id: "opendoar____::4daa3db355ef2b0e64b472968cb70f0d",
    original_id: "opendoar____::934"
  },
  {
    official_name: "Cork Open Research Archive",
    english_name: "CORA",
    openaire_id: "opendoar____::748ba69d3e8d1af87f84fee909eef339",
    original_id: "opendoar____::1535"
  },
  {
    official_name: "DIAS Access to Institutional Repository",
    english_name: "DAIR",
    openaire_id: "opendoar____::8fb134f258b1f7865a6ab2d935a897c9",
    original_id: "opendoar____::4043"
  },
  {
    official_name: "RCSI Repository",
    english_name: "RCSI (Royal College of Surgeons in Ireland) University of Medicine & Health Sciences",
    openaire_id: "opendoar____::9701a1c165dd9420816bfec5edd6c2b1",
    original_id: "opendoar____::1401"
  },
  {
    official_name: "Research Repository UCD",
    english_name: "",
    openaire_id: "opendoar____::a89cf525e1d9f04d16ce31165e139a4b",
    original_id: "opendoar____::1061"
  },
  {
    official_name: "T-Stór",
    english_name: "T-Stór",
    openaire_id: "opendoar____::b5baa9c23ac3e015ad287b17a3d4afa3",
    original_id: "opendoar____::2559"
  },
  {
    official_name: "Maynooth EPrints and ETheses Archive",
    english_name: "Maynooth EPrints and ETheses Archive",
    openaire_id: "opendoar____::dabd8d2ce74e782c65a973ef76fd540b",
    original_id: "opendoar____::1179"
  },
  {
    official_name: "Griffith Open",
    english_name: "Griffith Open",
    openaire_id: "opendoar____::df5354693177e83e8ba089e94b7b6b55",
    original_id: "opendoar____::4044"
  },
  {
    official_name: "Mary Immaculate Research Repository and Digital Archive",
    english_name: "MIRR",
    openaire_id: "opendoar____::e0688d13958a19e087e123148555e4b4",
    original_id: "opendoar____::2655"
  },
  {
    official_name: "Marine Institute Open Access Repository (OAR)",
    english_name: "",
    openaire_id: "opendoar____::ea119a40c1592979f51819b0bd38d39d",
    original_id: "opendoar____::2197"
  },
  {
    official_name: "DBS Esource",
    english_name: "DBS Esource",
    openaire_id: "opendoar____::f3ac63c91272f19ce97c7397825cc15f",
    original_id: "opendoar____::2313"
  }
]
