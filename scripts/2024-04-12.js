function deleteIndexParameters() {
    var indicators = db.indicator.find();
    indicators.forEach(indicator => {
        indicator.indicatorPaths.forEach(path => {
            if(path.parameters) {
                delete path.parameters['index_id'];
                delete path.parameters['index_name'];
                delete path.parameters['index_shortName'];
            }
            db.indicator.save(indicator);
        });
    })
}

function convertProfilesToReferences() {
    var defaultStakeholders = db.stakeholder.find({defaultId: null});
    defaultStakeholders.forEach(stakeholder => {
        stakeholder.copy = false;
        db.stakeholder.save(stakeholder);
    })
    var stakeholders = db.stakeholder.find({ defaultId: { $exists: true, $ne: null } });
    stakeholders.forEach(stakeholder => {
        stakeholder.copy = false;
        stakeholder.topics.forEach(topicId => {
            var topic = db.topic.findOne({_id: ObjectId(topicId)});
            topic.categories.forEach(categoryId => {
                var category = db.category.findOne({_id: ObjectId(categoryId)});
                category.subCategories.forEach(subCategoryId => {
                    var subCategory = db.subCategory.findOne({_id: ObjectId(subCategoryId)});
                    subCategory.numbers.forEach(sectionId => {
                        var section = db.section.findOne({_id: ObjectId(sectionId)});
                        section.indicators.forEach(indicatorId => {
                            db.indicator.deleteOne({_id: ObjectId(indicatorId)});
                        });
                        db.section.deleteOne({_id: ObjectId(sectionId)});
                    });
                    subCategory.charts.forEach(sectionId => {
                        var section = db.section.findOne({_id: ObjectId(sectionId)});
                        section.indicators.forEach(indicatorId => {
                            db.indicator.deleteOne({_id: ObjectId(indicatorId)});
                        });
                        db.section.deleteOne({_id: ObjectId(sectionId)});
                    });
                    db.subCategory.deleteOne({_id: ObjectId(subCategoryId)});
                });
                db.category.deleteOne({_id: ObjectId(categoryId)});
            });
            db.topic.deleteOne({_id: ObjectId(topicId)});
        });
        stakeholder.topics = [];
        db.stakeholder.save(stakeholder);
    });
}

print('topic: ' + db.topic.find({ defaultId: { $exists: true, $ne: null } }).count());
print('category: ' + db.category.find({ defaultId: { $exists: true, $ne: null } }).count());
print('subCategory: ' + db.subCategory.find({ defaultId: { $exists: true, $ne: null } }).count());
print('Section: ' + db.section.find({ defaultId: { $exists: true, $ne: null } }).count());
print('Indicator: ' + db.indicator.find({ defaultId: { $exists: true, $ne: null } }).count());
convertProfilesToReferences();
deleteIndexParameters();
print('topic: ' + db.topic.find({ defaultId: { $exists: true, $ne: null } }).count());
print('category: ' + db.category.find({ defaultId: { $exists: true, $ne: null } }).count());
print('subCategory: ' + db.subCategory.find({ defaultId: { $exists: true, $ne: null } }).count());
print('Section: ' + db.section.find({ defaultId: { $exists: true, $ne: null } }).count());
print('Indicator: ' + db.indicator.find({ defaultId: { $exists: true, $ne: null } }).count());
