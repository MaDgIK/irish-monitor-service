load('library/initialize.js');

initializeRPOs(removeDuplicateIds(rpos, 'id'));
initializeRFOs(removeDuplicateIds(rfos, 'funder_id'));
initializeDatasources(removeDuplicateIds(datasources, 'openaire_id'));
